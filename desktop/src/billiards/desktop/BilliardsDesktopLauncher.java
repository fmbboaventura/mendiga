package billiards.desktop;

import billiards.BilliardsLibGDX;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class BilliardsDesktopLauncher {	
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new BilliardsLibGDX(), config);
	}
}
