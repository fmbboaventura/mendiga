package superjumper.desktop;

import superjumper.SuperJumperLibGDX;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class SuperJumperDesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 480;
		config.height = 741;
		new LwjglApplication(new SuperJumperLibGDX(), config);
	}
}
