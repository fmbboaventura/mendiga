# MEnDiGa #

The Minimal Engine for Digital Games (MEnDiGa) is a simplified framework based on game features capable of building casual games regardless of their implementation resources. It presents minimal features in a representative hierarchy of spatial and game elements, along with basic behaviors and event support related to game logic features.