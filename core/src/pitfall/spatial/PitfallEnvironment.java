package pitfall.spatial;

import pitfall.spatial.nodes.AudioNodeContainer;
import MEnDiGa.spatial.Environment;
import MEnDiGa.spatial.nodes.Node;

public class PitfallEnvironment extends Environment {
	
	public static final Node audioNodeContainer = new AudioNodeContainer("audioContainer");

	public PitfallEnvironment(String id) {
		super(id);
	}

}
