package pitfall.spatial.nodes;

import MEnDiGa.spatial.locations.Map;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode.PlayMode;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.PhysicsNode;


public class CampfireElement extends Element {

	public CampfireElement(String id) {
		super(id);
		this.setGroupId("enemy");
		Position position = new Position(id + "Position", 7, 7, 2);//.5f, 2);
		this.setCurrentLocation(position);
		
		BoxBound boundingVolume = new BoxBound();
		boundingVolume.height = 1;
		boundingVolume.width = 1;
		this.setBounds(boundingVolume);
		
		PhysicsNode physicsNode = new PhysicsNode();
		this.setPhysicsNode(physicsNode);
		
		SpriteNode graphicNode = new SpriteNode(this.getName(), "res/fire.png", 128, 128);
		//this.graphicNode.setOffset(128/2f, 128/2f);
		graphicNode.setScale(Map.worldUnitPerPixels/4f);
		graphicNode.defineAnimation("burning", new int[]{0, 1}, 0.3f, PlayMode.LOOP_NORMAL);
		graphicNode.setCurrentAnimation("burning");
		this.addGraphicNode(graphicNode);
	}
}
