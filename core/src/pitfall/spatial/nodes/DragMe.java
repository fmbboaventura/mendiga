package pitfall.spatial.nodes;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;

public class DragMe extends TouchDragNode{

	public DragMe(String id) {
		super(id, new Position("myLocation", 0, 0, 10), 64, 64);
		SpriteNode graphicNode = new SpriteNode(		
				"res/tiles.png", //Caminho do arquivo de imagem contendo os frames
				 64, 64, //Altura e largura da �rea da imagem que ser� utilizada 
				 1, // Numero de linhas
				 1 // Numero de colunas
				);
		
		this.addGraphicNode(graphicNode);
	}

}
