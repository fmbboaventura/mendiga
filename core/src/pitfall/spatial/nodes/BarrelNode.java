package pitfall.spatial.nodes;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class BarrelNode extends Node {

	public BarrelNode(String id) {
		super(id);
		SpriteNode graphicNode = new SpriteNode(
				"res/barrel.png", 
				56, 29, 
				1, 2,
				46, 48); // largura e altura do barril no jogo
		this.addGraphicNode(graphicNode);
		
		this.currentLocation = new Position(id + "Position", 300, 190, 3);
	}

}
