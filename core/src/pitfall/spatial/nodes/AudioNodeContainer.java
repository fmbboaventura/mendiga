package pitfall.spatial.nodes;

import pitfall.spatial.nodes.audio.FallSound;
import pitfall.spatial.nodes.audio.JumpSound;
import pitfall.spatial.nodes.audio.PickupSound;
import MEnDiGa.spatial.nodes.Node;

public class AudioNodeContainer extends Node {

	public AudioNodeContainer(String id) {
		super(id);
		
		this.addAudioNode(new PickupSound());
		this.addAudioNode(new JumpSound());
		this.addAudioNode(new FallSound());
	}

}
