package pitfall.spatial.nodes.audio;

import MEnDiGa.adapters.libgdx.phisics.CollisionAdapter;
import MEnDiGa.spatial.nodes.AudioNode;

public class FallSound extends AudioNode{

	public FallSound() {
		super(CollisionAdapter.CONTACT_END, "res/fall-sound.wav");
		this.setLooping(false);
	}

}
