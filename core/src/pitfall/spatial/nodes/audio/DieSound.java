package pitfall.spatial.nodes.audio;

import MEnDiGa.spatial.nodes.AudioNode;

public class DieSound extends AudioNode{

	/**
	 * @param name
	 * @param filePath
	 */
	public DieSound(String id, String filePath) {
		super(id, filePath);
	}
	
}
