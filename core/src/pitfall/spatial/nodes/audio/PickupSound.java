package pitfall.spatial.nodes.audio;

import MEnDiGa.spatial.nodes.AudioNode;

public class PickupSound extends AudioNode{

	public PickupSound() {
		super("pickUp", "res/Pickup_Coin.wav");
		this.setLooping(false);
	}

}
