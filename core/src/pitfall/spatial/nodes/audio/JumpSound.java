package pitfall.spatial.nodes.audio;

import MEnDiGa.spatial.nodes.AudioNode;

public class JumpSound extends AudioNode{

	public JumpSound() {
		super("jumpSound", "res/Jump.wav");
		this.setLooping(false);
	}

}
