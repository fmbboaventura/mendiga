package pitfall.spatial.nodes;

import java.awt.geom.Rectangle2D;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Node;

public class TouchDragNode extends Node{

	private boolean isTouched = false;
	private float width, height;
	private Rectangle2D.Float touchArea;
	public Position touchPositionRelativeToOrigin;
	
	public TouchDragNode(String id) {
		super(id);
	}
	
	public TouchDragNode(String id, Position position, float width, float height) {
		super(id);
		this.currentLocation = position;
		this.width = width;
		this.height = height;
		this.touchArea = new Rectangle2D.Float(position.getX(),
									   position.getY(),
									   this.width, 
									   this.height);
	}

	/*public void setBounds(int width, int height){
		this.width = width;
		this.height = height;
		touchArea.s
	}*/
	
	public boolean isTouched(){
		return isTouched;
	}
	
	public void setTouched(boolean touched){
		this.isTouched = touched;
	}
	
	public Rectangle2D.Float getBoundingRectangle(){
		return this.touchArea;
	}

	public float getWidth() {
		return width;
	}

	public float getHeight() {
		return height;
	}
}
