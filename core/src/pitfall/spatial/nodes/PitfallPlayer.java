package pitfall.spatial.nodes;

import MEnDiGa.spatial.locations.Map;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.SpriteNode.PlayMode;
import MEnDiGa.spatial.nodes.Life;
import MEnDiGa.spatial.nodes.PhysicsNode;
import MEnDiGa.spatial.nodes.Player;
import MEnDiGa.spatial.nodes.Score;
import MEnDiGa.spatial.nodes.Velocity;

public class PitfallPlayer extends Player {
	
	// Definindo o a chave do pulo (ainda n�o usado) e os IDs das anima��es
	public static final String BUTTON = "button";
	public static final String WALK_ANIMATION = "walk";
	public static final String CLIMB_ANIMATION = "climb";
	public static final String ON_GROUND = "canJump";
	
	// Frame numero 9 do sheet
	public static final int PITFALL_IDLE = 9;
	public static final int PITFALL_JUMP = 0;
	public static final String ON_LADDER = "onLadder";
	
	// Placeholders
	public static Position RESPAWN = new Position("respawn", 1, 9, 0); //+ 1.375f, 0);
	
	public static Position UPPER_LEFT = new Position("upper left", 1, 7, 0); //+ 1.375f, 0);
	public static Position UPPER_RIGHT = new Position("upper right", 18, 7, 0); //+ 1.375f, 0);
	
	public static Position LOWER_LEFT = new Position("lower left", 1, 2, 0);//+ 1.375f, 0);
	public static Position LOWER_RIGHT = new Position("lower right", 18, 2, 0); //+ 1.375f, 0);

	public PitfallPlayer(String name) {
		super(name);
		
		this.groupId = "player";
		
		BoxBound pitfallBounds = new BoxBound();
		
		int regionWidth = 80;
		int regionHeight = 44;
		int frameWidth = 80/5;
		int frameHeight = 44/2;
		
		/* O frame contem muito espaco vazio. Criar um bounding box com as mesmas dimensoes 
		 * do frame nao seria bom, porque as colisoes se tornariam visivelmente imprecisas. 
		 * A altura e a largura utilizadas abaixo sao equivalentes a area do "corpo" do 
		 * pitfall e est�o expressas em world units.
		 * 
		 * Para a altura do pitfall, eu converto a altura do frame de pixels para world units
		 * (ou tiles, se preferir). Mas como a imagem que contem os frames eh muito pequena, 
		 * e 1 world unit equivale a 32 pixels, a altura final do pitfall seria de 22/32. Ou 
		 * seja, ~0.7 world units (menor que 1 tile!). Multiplico por 4 para que pitfall tenha 
		 * aproximadamente 3 tiles de altura (dava pra jogar basquete).
		 * 
		 * A largura total do frame � de 16 pixels. A largura do "corpo" do pitfall ocupa 1/4
		 * da largura do frame. Por isso eu nao multiplico a largura final por 4, como fiz com
		 * a altura.
		 */
		pitfallBounds.width = Map.pixelsToWorld(frameWidth);
		pitfallBounds.height = Map.pixelsToWorld(frameHeight) * 4;
		this.setBounds(pitfallBounds);
		
		PhysicsNode pitfallPhysics = new PhysicsNode();
		this.setPhysicsNode(pitfallPhysics);
		
		//this.audioNode = new JumpSound();
		
		/* ## GraphicNode Config. ## */
		SpriteNode graphicNode = new SpriteNode(
				this.getName(),
				"res/sheet4.png", // Caminho do sprite sheet
				frameWidth, frameHeight);      // Altura e largura dos frames
		
		/* Define a regiao do sprite sheet que sera utilizada
		 * Os frames sao cortados a partir dessa regiao.
		 */
		graphicNode.setRegion(regionWidth, regionHeight);
		
		/* Define o fator usado para escalar os sprites.
		 * Map.worldUnitPerPixels � o fator de convers�o
		 * de pixels pra World units. A constante serve
		 * para aumentar o tamanho final do sprite. Ou
		 * seja, a imagem tera 4x o tamanho inicial e este
		 * ser� expresso em world units.
		 */
		graphicNode.setScale(Map.worldUnitPerPixels*4f);
		
		graphicNode.setOffset(-0.75f, 0);
		this.addGraphicNode(graphicNode);
				
		Position position = UPPER_LEFT;
		this.setCurrentLocation(position);
		
		// Usando a classe property
		this.addProperty(new Velocity());
		this.addProperty(new Flags());
		this.addProperty(new Life(3));
		this.addProperty(new Score());
		
		graphicNode.setCurrentFrame(PITFALL_IDLE);
		
		/*
		 * Defino uma animacao com um nome, um array com os indices dos 
		 * frames que serao usados na animacao e o tempo de duracao
		 * de cada frame.
		 */
		graphicNode.defineAnimation(WALK_ANIMATION, new int[]{0, 1, 2, 3, 4}, 0.07f, PlayMode.LOOP_NORMAL);
		graphicNode.defineAnimation(CLIMB_ANIMATION, new int[]{5, 6}, 0.3f, PlayMode.LOOP_NORMAL);
	}
	
	public void setCurrentCommand(String command){
		super.setCurrentCommand(command);
	}
}
