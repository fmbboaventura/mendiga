package pitfall.spatial.nodes;

import java.util.Random;

import MEnDiGa.spatial.locations.Map;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.PhysicsNode;

public class TreasureElement extends Element {

	public TreasureElement(String id) {
		super(id);
		this.setGroupId("collectible");
		Position position = new Position(id + "Position", 15, 7, 1);//.5f, 1);
		this.setCurrentLocation(position);
		
		BoxBound boundingVolume = new BoxBound();
		boundingVolume.height = 1;
		boundingVolume.width = 1;
		this.setBounds(boundingVolume);
		
		PhysicsNode physicsNode = new PhysicsNode();
		this.setPhysicsNode(physicsNode);
		
		SpriteNode graphicNode = new SpriteNode(this.getName(), "res/treasures.png", 128, 128);
		//this.graphicNode.setOffset(128/2f, 128/2f);
		graphicNode.setScale(Map.worldUnitPerPixels/4f);
		graphicNode.setCurrentFrame(new Random().nextInt(5));
		this.addGraphicNode(graphicNode);
	}
}
