package pitfall.spatial.nodes;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class BlockNode extends Node {

	public BlockNode() {
		super("blockNode");
		SpriteNode graphicNode = new SpriteNode(		
				"res/tiles.png", //Caminho do arquivo de imagem contendo os frames
				 64, 64, //Altura e largura da �rea da imagem que ser� utilizada 
				 1, // Numero de linhas
				 1 // Numero de colunas
				);
		
		Position position = (Position) new Position("blockNodePosition", 200, 192, 1);
		this.setCurrentLocation(position);
		
		graphicNode.setCurrentFrame(0);
		this.addGraphicNode(graphicNode);
	}
}

