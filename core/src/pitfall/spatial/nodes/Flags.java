package pitfall.spatial.nodes;

import MEnDiGa.spatial.nodes.Property;

public class Flags extends Property{
	public boolean onGround;
	public boolean onLadder;
	
	public Flags(){
		this.onGround = false;
		this.onLadder = false;
	}
}
