package pitfall.observers;

import pitfall.spatial.nodes.Flags;
import pitfall.spatial.nodes.PitfallPlayer;
import MEnDiGa.observers.CollisionGroupObserver;

/**
 * <p>
 * Execute {@link Behavior}s when a {@link Node} of the "player" group
 * collides with a {@link Node} of the "changeLevelSensor" group.
 * These sensors .... TODO: finish this javadoc
 * </p>
 * 
 * @author Filipe Boaventura
 *
 */
public class LadderCollisionObserver extends CollisionGroupObserver {

	private PitfallPlayer player;

	public LadderCollisionObserver(String id, PitfallPlayer p) {
		super(id, "player", "ladder");
		this.player = p;
	}

	@Override
	public boolean evaluate(Object... params) {
		if(params != null){
			String eventType = (String) params[0];
			
			if(eventType.endsWith("Begin")){
				player.getProperty(Flags.class).onLadder = true;
				player.getPhysicsNode().setGravityScale(0);
			}
			else if(eventType.endsWith("End")){
				player.getProperty(Flags.class).onLadder = false;
				player.getPhysicsNode().setGravityScale(1);
			}
		}
		return false;
	}

}
