package pitfall.observers;

import pitfall.PitfallGame;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.nodes.Player;

public class SplashTouchObserver extends Observer{
	private PitfallGame game;
	private String splashNodeId;
	private Spatial spatialToGo;
	
	public SplashTouchObserver(String id, PitfallGame game, String splashNodeId, Spatial spatialToGo) {
		super(id);
		this.game = game;
		this.splashNodeId = splashNodeId;
		this.spatialToGo = spatialToGo;
	}

	@Override
	public boolean evaluate(Object... params) {
		if (splashNodeId.equals(game.getCurrentSpatial().getId())){
			if (params != null && params[0] != null && Player.TOUCH_DOWN.equals(params[0])){	
				game.setCurrentSpatial(spatialToGo);
			}
		}
		
		return false;
	}

}
