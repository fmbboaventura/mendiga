package pitfall.observers;

import pitfall.spatial.nodes.TouchDragNode;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.locations.Position;

public class TouchObserver extends Observer {

	private TouchDragNode tdNode;
	private Behavior behaviorToPerform;
	
	public TouchObserver(String id, TouchDragNode node, Behavior behaviorToPerform) {
		super(id);
		this.tdNode = node;
		this.behaviorToPerform = behaviorToPerform;
	}

	@Override
	public boolean evaluate(Object... params) {
		
		if(params != null && params[0].equals("touchDown")){
			float[] cursorCoordinates = (float[]) params[1];
			if(!tdNode.isTouched()){
				// Avalia se as coordenadas do cursor est�o contidas no node
				if(tdNode.getBoundingRectangle().contains(cursorCoordinates[0], cursorCoordinates[1])){
					
					tdNode.setTouched(true);
					Position position = (Position)tdNode.getCurrentLocation();
					
					// Calcula a diferen�a entre o ponto de toque e a origem
					// Levando em considera��o a origem do libgdx (canto inferior esquerdo)
					tdNode.touchPositionRelativeToOrigin = new Position("touchPoint", cursorCoordinates[0] - position.getX(), cursorCoordinates[1] - position.getY(), 0);
					
					
					behaviorToPerform.execute(cursorCoordinates);
				
				}
				else{
					tdNode.setTouched(false);
				}
			}
			else if(tdNode.isTouched()){
				behaviorToPerform.execute(cursorCoordinates);
			}
		}
		else if(params != null && params[0].equals("noTouch")){
			tdNode.setTouched(false);
			tdNode.touchPositionRelativeToOrigin = null;
		}
		
		return false;
	}

}
