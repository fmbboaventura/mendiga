package pitfall.observers;

import MEnDiGa.observers.CollisionGroupObserver;


public class TreasureCollisionObserver extends CollisionGroupObserver {

	public TreasureCollisionObserver(String id) {
		super(id, "player", "collectible");
	}

	@Override
	public boolean evaluate(Object... params) {
		if(params != null){
			String eventType = (String) params[0];
			
			if(eventType.endsWith("Begin")){
				params[0] = "treasure";
				return true;
			}
		}
		return false;
	}
}
