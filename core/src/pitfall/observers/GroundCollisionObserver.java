package pitfall.observers;

import MEnDiGa.observers.CollisionGroupObserver;
import MEnDiGa.spatial.nodes.Player;
import pitfall.spatial.nodes.Flags;
import pitfall.spatial.nodes.PitfallPlayer;

public class GroundCollisionObserver extends CollisionGroupObserver {
	private PitfallPlayer player;

	public GroundCollisionObserver(String id, PitfallPlayer p ) {
		super(id, p.getGroupId(), "ground");
		player = p;
	}

	@Override
	public boolean evaluate(Object... params) {
		if(params != null){
			String param = (String) params[0];
			if(param.endsWith("Begin")){
				player.getProperty(Flags.class).onGround = true;
				player.setCurrentCommand(Player.NONE_MOVE);
			}
			else if(param.endsWith("End")){
				if(!player.getCurrentCommand().equals(PitfallPlayer.BUTTON1_PRESS)
						&& !player.getCurrentCommand().equals(PitfallPlayer.MOVE_UP)
						&& !player.getCurrentCommand().equals(PitfallPlayer.MOVE_DOWN)){
					//FIXME rever condi��o
					return true;
				}
			}
		}
		return false;
	}
}
