package pitfall.observers;

import java.util.HashMap;

import pitfall.spatial.nodes.Flags;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.nodes.Player;

// Registra os inputs e os processa de acordo com o estado atual do pitfall
public class PitfallInputObserver extends Observer {

	public HashMap<String, Boolean> keyStates;
	private Player player;

	public PitfallInputObserver(String id, Player player) {
		super(id);

		this.player = player;

		keyStates = new HashMap<String, Boolean>();
		keyStates.put(Player.MOVE_LEFT, false);
		keyStates.put(Player.MOVE_RIGHT, false);
		keyStates.put(Player.MOVE_UP, false);
		keyStates.put(Player.MOVE_DOWN, false);
		keyStates.put(Player.BUTTON1_PRESS, false);
	}

	@Override
	public boolean evaluate(Object... params) {

		if (params != null) {
			/*
			 * Processa os inputs registrados de acordo com estado atual do
			 * pitfall.
			 */

			if (params[0].equals("currentCommand"))
				params[0] = player.getCurrentCommand();
			Flags flags = player.getProperty(Flags.class);

			/*
			 * Se o pitfall est� no ch�o, verifica o estado das teclas
			 * correspondentes aos behaviors que podem ser executados neste
			 * estado.
			 */
			if (flags.onGround) {

				// Caso -> ou <-
				if (params[0].equals(Player.MOVE_LEFT)) {
					player.getPlayerBehavior(Player.MOVE_LEFT).execute(
							Player.MOVE_LEFT);
					player.setCurrentCommand(Player.MOVE_LEFT);
				} else if (params[0].equals(Player.MOVE_RIGHT)) {
					player.getPlayerBehavior(Player.MOVE_RIGHT).execute(
							Player.MOVE_RIGHT);
					player.setCurrentCommand(Player.MOVE_RIGHT);
				}

				// Pulo
				if (params[0].equals(Player.BUTTON1_PRESS)) {
					player.getPlayerBehavior(Player.BUTTON1_PRESS).execute();
					player.setCurrentCommand(Player.BUTTON1_PRESS);
					params[0] = "jumpSound";
					return true;
				}

				// Nenhum bot�o acionado
				if (params[0].equals(Player.NONE_MOVE)
						&& player.getProperty(Flags.class).onGround) {
					player.getPlayerBehavior(Player.NONE_MOVE).execute(
							Player.NONE_MOVE);
					player.setCurrentCommand(Player.NONE_MOVE);
				}
			}

			if (flags.onLadder) {
				if (keyStates.get(Player.MOVE_UP)) {
					player.getPlayerBehavior(Player.MOVE_UP).execute(
							Player.MOVE_UP);
					player.setCurrentCommand(Player.MOVE_UP);
				} else if (keyStates.get(Player.MOVE_DOWN)) {
					player.getPlayerBehavior(Player.MOVE_DOWN).execute(
							Player.MOVE_DOWN);
					player.setCurrentCommand(Player.MOVE_DOWN);
				}

				if (keyStates.get(Player.BUTTON1_PRESS)) {
					player.getPlayerBehavior(Player.BUTTON1_PRESS).execute();
					player.setCurrentCommand(Player.BUTTON1_PRESS);
				}
			}
		}
		return false;
	}

}
