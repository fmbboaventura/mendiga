package pitfall.observers;

import MEnDiGa.observers.CollisionGroupObserver;

/**
 * <p>
 * Execute {@link Behavior}s when a {@link Node} of the "player" group
 * collides with a {@link Node} of the "changeLevelSensor" group.
 * These sensors .... TODO: finish this javadoc
 * </p>
 * 
 * @author Filipe Boaventura
 *
 */
public class ChangeLevelObserver extends CollisionGroupObserver {

	public ChangeLevelObserver(String id) {
		super(id, "player", "changeLevelSensor");
	}

	@Override
	public boolean evaluate(Object... params) {
		if(params != null){
			String eventType = (String) params[0];
			
			if(eventType.endsWith("Begin")){
				return true;
			}
		}
		return false;
	}

}
