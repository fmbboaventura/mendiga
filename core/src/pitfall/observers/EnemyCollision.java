package pitfall.observers;

import MEnDiGa.observers.CollisionGroupObserver;

public class EnemyCollision extends CollisionGroupObserver {


	public EnemyCollision(String id) {
		super(id, "player", "enemy");
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public boolean evaluate(Object... params) {
		if(params != null){
			String eventType = (String) params[0];
			
			if(eventType.endsWith("Begin")){
				return true;
			}
		}
		return false;
	}
}
