package pitfall.behaviors;

import pitfall.spatial.nodes.PitfallPlayer;
import MEnDiGa.behaviors.Behavior;

// Executado quando pitfall colide com um inimigo
public class PitfallDie extends Behavior {
	private PitfallPlayer player;
	
	public PitfallDie(PitfallPlayer player) {
		this.player = player;
	}

	@Override
	public void execute(Object... params) {
		/* No loop, se um node contem um position com o name respaw,
		 * significa que  ele deve ser "teletransportado" para as
		 * coordenadas desse position.
		 */
		player.setCurrentLocation(PitfallPlayer.RESPAWN);
	}

}
