/**
 * 
 */
package pitfall.behaviors;

import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.AudioNode;
import MEnDiGa.spatial.nodes.AudioNode.State;
import MEnDiGa.spatial.nodes.Node;

/**
 * @author Filipe Boaventura
 *
 */
public class PlayAudioNode extends Behavior {

	private Node node;
	
	public PlayAudioNode(Node node){
		this.node = node;
	}
	
	@Override
	public void execute(Object... params) {
		// Executa os audio nodes correspondentes aos ids
		for(Object audioNodeId : params){
			if(audioNodeId instanceof String){
				AudioNode audio = node.getAudioNode((String)audioNodeId);
				if(audio != null){
					audio.setState(State.PLAYING);
				}
			}
		}
	}
}
