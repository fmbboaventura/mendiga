package pitfall.behaviors;

import pitfall.spatial.nodes.PitfallPlayer;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Velocity;

public class ClimbLadder extends Behavior {
	
PitfallPlayer player;
private float climbSpeed = 6;
	
	public ClimbLadder(PitfallPlayer player){
		this.player = player;
	}

	@Override
	public void execute(Object... params) {
		Velocity v = player.getProperty(Velocity.class);
		v.setX(0);

		if(((String)params[0]).equals(PitfallPlayer.MOVE_UP)){
			v.setY(climbSpeed);
		}
		else if(((String)params[0]).equals(PitfallPlayer.MOVE_DOWN)){
			v.setY(-1*climbSpeed);
		}

		((SpriteNode) player.getGraphicNode(player.getName())).setCurrentAnimation(PitfallPlayer.CLIMB_ANIMATION);
	}

}
