package pitfall.behaviors;

import pitfall.spatial.PitfallEnvironment;
import pitfall.spatial.nodes.CampfireElement;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.locations.Map;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.Player;

public class LoadLevelBehavior extends Behavior {
	private GameSession game;
	
	public LoadLevelBehavior(GameSession game) {
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		if(params != null){
			Element changeLevelSensor = null;
			Player p = null;
			
			if(((Element) params[1]).getGroupId().equals("changeLevelSensor")){
				changeLevelSensor = (Element) params[1];
				p = (Player) params[2];
			} else {
				changeLevelSensor = (Element) params[2];
				p = (Player) params[1];
			}
			
			String mapFileSource = (String) changeLevelSensor.getProperty("nextLevel");
			if(game.getSpatial(mapFileSource) == null){
				Map map = new Map("map-" + mapFileSource, mapFileSource);
				
				PitfallEnvironment next = new PitfallEnvironment(mapFileSource);
				next.addLocation(map.getId(), map);
				next.addNode(p);
				
				//FIXME STUB BEGINS HERE!!
				
				switch(mapFileSource){
				case "res/forestMap2.tmx":
					next.addNode(new CampfireElement("fire"));
					break;
				case "res/lvl2.tmx":
					break;
				case "res/lvl3.tmx":
					break;
				}
				
				//FIXME STUB ENDS HERE!!
				//TODO Criar metodo para instanciar elements a partir de map objects 
				
				game.addSpatial(next.getId(), next);
				game.setCurrentSpatial(next);
				
			}
			else{
				game.setCurrentSpatial(game.getSpatial(mapFileSource));
			}
			
			Position position = (Position) p.getCurrentLocation();
			String sensorName = (String) changeLevelSensor.getName();
			
			if(sensorName.contains("Right")){
				position.setX(1);
			}
			else if(sensorName.contains("Left")){
				position.setX(18);
			}
			p.setCurrentLocation(new Position("respawn", position.getX(), position.getY(), position.getZ()));
		}
	}

}
