package pitfall.behaviors;

import pitfall.spatial.nodes.PitfallPlayer;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Velocity;

public class Walk extends Behavior {
	PitfallPlayer player;
	float walkSpeed = 6;
	
	public Walk(PitfallPlayer player){
		this.player = player;
	}

	@Override
	public void execute(Object... params) {
		
		Velocity v = player.getProperty(Velocity.class);
		v.setY(0);
		SpriteNode gNode = (SpriteNode) player.getGraphicNode(player.getName());
		if(((String)params[0]).equals(PitfallPlayer.MOVE_RIGHT)){
			v.setX(walkSpeed);
			gNode.flip(false, false);
		}
		else if(((String)params[0]).equals(PitfallPlayer.MOVE_LEFT)){
			
			v.setX(-1*walkSpeed);
			gNode.flip(true, false);
		}		
		gNode.setCurrentAnimation(PitfallPlayer.WALK_ANIMATION);
		
	}
}
