package pitfall.behaviors;

import pitfall.spatial.nodes.PitfallPlayer;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Velocity;

public class Stop extends Behavior {
	PitfallPlayer player;
	
	public Stop(PitfallPlayer player){
		this.player = player;
	}

	@Override
	public void execute(Object... params) {
			Velocity v = player.getProperty(Velocity.class);
			
			SpriteNode n = (SpriteNode) player.getGraphicNode(player.getName());
			n.setCurrentFrame(PitfallPlayer.PITFALL_IDLE);
			//player.getPhysicsNode().setLinearVelocity(0, 0);
			//Novo m�todo
			v.zero();
			//PhysicsAdapter.getInstance().applyVelocityToElement(player.getId(), 0, 0);
	}
}