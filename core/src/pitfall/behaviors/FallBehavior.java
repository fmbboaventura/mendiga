package pitfall.behaviors;

import MEnDiGa.behaviors.Behavior;
import pitfall.spatial.nodes.Flags;
import pitfall.spatial.nodes.PitfallPlayer;

// Behavior ser� respons�vel pela queda em linha reta do pitfall
// Tamb�m tocar� o som da queda ???????
public class FallBehavior extends Behavior{
	PitfallPlayer player;
	
	public FallBehavior(PitfallPlayer player) {
		this.player = player;
	}

	@Override
	public void execute(Object... params) {
		player.getProperty(Flags.class).onGround = false;
		player.getPlayerBehavior(PitfallPlayer.NONE_MOVE).execute();
	}

}
