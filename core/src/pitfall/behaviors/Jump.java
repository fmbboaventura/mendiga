package pitfall.behaviors;

import pitfall.spatial.nodes.Flags;
import pitfall.spatial.nodes.PitfallPlayer;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Velocity;

public class Jump extends Behavior {
	PitfallPlayer player;
	float jumpSpeed = 6;
	
	public Jump(PitfallPlayer player){
		this.player = player;
	}

	@Override
	public void execute(Object... params) {
		Flags flags = player.getProperty(Flags.class);
		
		if(flags.onGround){
			SpriteNode gNode = (SpriteNode) player.getGraphicNode(player.getName());
			gNode.setCurrentAnimation(null);
			gNode.setCurrentFrame(PitfallPlayer.PITFALL_JUMP);
			
			// Novo m�todo
			flags.onGround = false;
			Velocity velocity = player.getProperty(Velocity.class);
			velocity.setY(jumpSpeed);
		}
	}
}
