package pitfall.behaviors;

import pitfall.spatial.nodes.TouchDragNode;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.locations.Position;

public class Drag extends Behavior {
	
	private TouchDragNode nodeToDrag;
	
	public Drag(TouchDragNode nodeToDrag){
		this.nodeToDrag = nodeToDrag;
	}
	
	@Override
	public void execute(Object... params) {
		Position position = (Position)nodeToDrag.getCurrentLocation();
		Position touchPoint = new Position("touchPoint", (Float) params[0], (Float) params[1], 0);
		
		// Calcula a nova posi��o do node utilizando o ponto de refer�ncia calculado anteriormente
		position.setX(touchPoint.getX() - nodeToDrag.touchPositionRelativeToOrigin.getX());
		position.setY(touchPoint.getY() - nodeToDrag.touchPositionRelativeToOrigin.getY());
		nodeToDrag.getBoundingRectangle().setRect(position.getX(), position.getY(), nodeToDrag.getWidth(), nodeToDrag.getHeight());
		
	}

}
