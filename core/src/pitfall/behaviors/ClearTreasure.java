package pitfall.behaviors;

import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import pitfall.spatial.nodes.TreasureElement;

// Remove o item da tela e seu corpo do mundo. Excutado quando pitfall pega um tesouro
public class ClearTreasure extends Behavior {
	
	private GameSession game;

	public ClearTreasure() {}
	
	public ClearTreasure(GameSession game) {
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		if(params != null){
			
			TreasureElement treasure = null;
			if(params[1] instanceof TreasureElement){
				treasure = (TreasureElement) params[1];
			}
			else treasure = (TreasureElement) params[2];
			
			game.getCurrentSpatial().removeNode(treasure);
			//TODO encontrar forma de remover o node do box2d
			//PhysicsAdapter.getInstance().destroyElementBody(treasureId);
			params[0] = "pickUp";
		}
	}

}
