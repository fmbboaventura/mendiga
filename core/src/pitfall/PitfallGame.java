package pitfall;

import pitfall.behaviors.ClearTreasure;
import pitfall.behaviors.ClimbLadder;
import pitfall.behaviors.FallBehavior;
import pitfall.behaviors.Jump;
import pitfall.behaviors.LoadLevelBehavior;
import pitfall.behaviors.PitfallDie;
import pitfall.behaviors.PlayAudioNode;
import pitfall.behaviors.Stop;
import pitfall.behaviors.Walk;
import pitfall.observers.ChangeLevelObserver;
import pitfall.observers.EnemyCollision;
import pitfall.observers.GroundCollisionObserver;
import pitfall.observers.LadderCollisionObserver;
import pitfall.observers.PitfallInputObserver;
import pitfall.observers.TreasureCollisionObserver;
import pitfall.spatial.PitfallEnvironment;
import pitfall.spatial.nodes.PitfallPlayer;
import pitfall.spatial.nodes.TreasureElement;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.Environment;
import MEnDiGa.spatial.locations.Map;
import MEnDiGa.spatial.nodes.Player;

public class PitfallGame extends GameSession {
	
	public PitfallGame(){
		// ## Spatials ##
		
		// JungleEnvironment
		Environment startUp = new PitfallEnvironment("start");
		this.addSpatial(startUp.getId(), startUp);
		
		// Configurando a unidade de medida
		Map.worldUnitPerPixels = 1/32f;
		
		// adicionando Mapa inicial
		Map map = new Map("startMap","res/forestMap32.tmx");
		startUp.addLocation(map.getId(), map);
		
		// Adiciona player inicial ao enviroment
		PitfallPlayer player = new PitfallPlayer("player1");
		startUp.addNode(player);
		
		// Adciona item no spatial
		TreasureElement treasure = new TreasureElement("treasure");
		startUp.addNode(treasure);
		
//		CampfireElement fire = new CampfireElement("fire");
//		startUp.addNode(fire.getId(), fire);
		
		// ## Behaviors ##

		Behavior walk = new Walk(player);
		player.setPlayerBehavior(Player.MOVE_RIGHT, walk);
		player.setPlayerBehavior(Player.MOVE_LEFT, walk);
		this.addBehavior("walk", walk);
		
		Behavior playAudio = new PlayAudioNode(PitfallEnvironment.audioNodeContainer);
		this.addBehavior("playAudio", playAudio);
		
		Behavior jump = new Jump(player);
		player.setPlayerBehavior(Player.MOVE_UP, jump);
		player.setPlayerBehavior(Player.BUTTON1_PRESS, jump);
		this.addBehavior("jump", jump);
		
		Behavior stop = new Stop(player);
		player.setPlayerBehavior(Player.NONE_MOVE, stop);
		this.addBehavior("stop", stop);
		
		Behavior climb  = new ClimbLadder(player);
		player.setPlayerBehavior(Player.MOVE_UP, climb);
		player.setPlayerBehavior(Player.MOVE_DOWN, climb);
		this.addBehavior("climb", climb);
		// Trata o t�rmino  do contato entre o pitfall e o ch�o
		Behavior fall = new FallBehavior(player);
		this.addBehavior("fall", fall);
		
		// Apaga um tesouro do enviroment atual
		Behavior clrTreasure = new ClearTreasure(this);
		this.addBehavior("clearTreasure", clrTreasure);
		
		Behavior die = new PitfallDie(player);
		this.addBehavior("die", die);
		
		Behavior changeLvlBehavior = new LoadLevelBehavior(this);
		this.addBehavior("changeLevel", changeLvlBehavior);
		
		// ## Observers ##
		
		PitfallInputObserver pInput = new PitfallInputObserver("pitfallInputObserver", player);
		pInput.addBehavior(playAudio);
		this.addObserver(pInput.getId(), pInput);
		
		GroundCollisionObserver groundCollision = new GroundCollisionObserver("groundCollisionObserver", player);
		groundCollision.addBehavior(fall);
		groundCollision.addBehavior(playAudio);
		this.addObserver(groundCollision.getId(), groundCollision);
		
		TreasureCollisionObserver treasureCollision = new TreasureCollisionObserver("treasureCollisionObserver");
		treasureCollision.addBehavior(clrTreasure);
		treasureCollision.addBehavior(playAudio);
		this.addObserver(treasureCollision.getId(), treasureCollision);
		
		EnemyCollision enemyCollision = new EnemyCollision("enemyCollisionObserver");
		enemyCollision.addBehavior(die);
		this.addObserver(enemyCollision.getId(), enemyCollision);
		
		ChangeLevelObserver changeLvlObserver = new ChangeLevelObserver("ChangeLevelObserver");
		changeLvlObserver.addBehavior(changeLvlBehavior);
		this.addObserver(changeLvlObserver.getId(), changeLvlObserver);
		
		LadderCollisionObserver ladderCollision = new LadderCollisionObserver("ladderCollisionObserver", player);
		this.addObserver(ladderCollision.getId(), ladderCollision);
		
		// Spatial to start
		setCurrentSpatial(startUp);
	}
}
