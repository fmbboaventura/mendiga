package pitfall;


import pitfall.spatial.PitfallEnvironment;
import MEnDiGa.adapters.libgdx.SoundAdapter;
import MEnDiGa.adapters.libgdx.GameAdapter;
import MEnDiGa.adapters.libgdx.InputAdapter;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.nodes.AudioNode;
import MEnDiGa.spatial.nodes.Node;

import com.badlogic.gdx.Gdx;

public class PitfallLibGDX extends GameAdapter {
	
	@Override
	public void createAdapter(Spatial s){
		super.createAdapter(s);
		for(AudioNode audio : PitfallEnvironment.audioNodeContainer.getAudioNodes()){
			audioAdapters.put(audio.getId(), new SoundAdapter(audio));
		}
	}
	
	@Override
	public void create() {
		viewportWidth = 20;
		viewportHeight = 14;
		gravityX = 0;
		gravityY = -10;
		this.game = new PitfallGame();
		
		// Cria os adapters para o spatial atual
		this.createAdapter(game.getCurrentSpatial());
				
		this.input = new InputAdapter(this.renders.get(game.getCurrentSpatial().getId()).getCamera());
		this.input.addObserver(this.game.getObserver("pitfallInputObserver"));
		
		// Configuro o libGDX para usar o input processor que eu implementei
		Gdx.input.setInputProcessor(input);
	}
	
	@Override
	protected void updateAudio(Node n) {
		super.updateAudio(n);
		for(AudioNode audio : PitfallEnvironment.audioNodeContainer.getAudioNodes()){
			this.audioAdapters.get(audio.getId()).update();
		}
	}

	@Override
	public void render() {
		// TODO Auto-generated method stub
		super.render();
		this.input.update();
		//this.game.getObserver("pitfallInputObserver").fire(new Object[]{"currentCommand"});
	}
}
