package billiards;

import MEnDiGa.adapters.libgdx.GameAdapter;
import MEnDiGa.adapters.libgdx.InputAdapter;

import com.badlogic.gdx.Gdx;

public class BilliardsLibGDX extends GameAdapter{

	@Override
	public void create() {
		viewportWidth = 40;//2.5f;
		viewportHeight = 30;//1.875f;
		//this.gravityY = -10;
		this.game = new BilliardsGame();
		// Cria os adapters para o spatial atual
		this.createAdapter(game.getCurrentSpatial());
		
		this.input = new InputAdapter(this.renders.get(game.getCurrentSpatial().getId()).getCamera());
		this.input.addObserver(this.game.getObserver("input"));
		
		// Configuro o libGDX para usar o input processor que eu implementei
		Gdx.input.setInputProcessor(input);
	}

	@Override
	public void render() {
		super.render();
		((BilliardsGame) this.game).update();
	}
}
