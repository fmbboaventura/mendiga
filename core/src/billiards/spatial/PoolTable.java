package billiards.spatial;

import java.util.Iterator;

import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.Node;
import MEnDiGa.spatial.nodes.PhysicsNode;
import MEnDiGa.spatial.nodes.SphereBound;
import billiards.spatial.node.ClipElement;
import billiards.spatial.node.ClipElement.Clip;
import billiards.spatial.node.CueBallElement;
import billiards.spatial.node.PocketElement;
import billiards.spatial.node.PocketElement.PocketAlignment;
import billiards.spatial.node.PoolBallElement;
import billiards.spatial.node.BackgroundNode;
import billiards.spatial.node.ScoreDisplayNode;

public class PoolTable extends Spatial{
	
	private static final String CUE_BALL = "cueBall";
	public static float pixelsToWorld = 1/32f;
	private CueBallElement cueBall;

	public PoolTable(String id) {
		super(id);
		this.addNode(new BackgroundNode("table"));
//		for (int i = 0; i < 40; i++) {
//			for (int j = 0; j < 30; j++) {
//				Element n = new Element("node"+i+" "+j) {
//				};
//				n.setBounds(new BoxBound(1,1));
//				n.setPhysicsNode(new PhysicsNode());
//				n.setCurrentLocation(new Position(n.getId(), i, j, 0));
//				this.addNode(n);
//			};
//		}
//		Element e = new Element("ball");
//		e.setPhysicsNode(new PhysicsNode());
//		e.setBounds(new SphereBound(1));
//		e.setCurrentLocation(new Position(e.getId(), 12,14, 0));
//		this.addNode(e);
		this.addNode(new ClipElement("leftClipNode", Clip.LEFT));
		this.addNode(new ClipElement("bottomLeftClipNode", Clip.BOTTOM_LEFT));
		this.addNode(new ClipElement("bottomRightClipNode", Clip.BOTTOM_RIGHT));
		this.addNode(new ClipElement("rightClipNode", Clip.RIGHT));
		this.addNode(new ClipElement("topRightClipNode", Clip.TOP_RIGHT));
		this.addNode(new ClipElement("topLeftClipNode", Clip.TOP_LEFT));
		this.cueBall = new CueBallElement(CUE_BALL);
		this.addNode(cueBall);
		this.addNode(new PocketElement("bottomLeftHole", PocketAlignment.BOTTOM_LEFT));
		this.addNode(new PocketElement("bottomMiddleHole", PocketAlignment.BOTTOM_MIDLE));
		this.addNode(new PocketElement("bottomRightHole", PocketAlignment.BOTTOM_RIGHT));
		this.addNode(new PocketElement("topLeftHole", PocketAlignment.TOP_LEFT));
		this.addNode(new PocketElement("topMiddleHole", PocketAlignment.TOP_MIDLE));
		this.addNode(new PocketElement("topRightHole", PocketAlignment.TOP_RIGHT));
		
		//this.addNode(new ScoreDisplayNode("scoreDisplay"));
		
		Iterator<PoolBallElement> it = PoolBallElement.rack();
		
		while(it.hasNext()){
			PoolBallElement ball = it.next();
			this.addNode(ball);
		}
	}
	
	public CueBallElement getCueBall(){
		return this.cueBall;
	}
	
	public void restoreCueBall(){
		this.addNode(getCueBall());
	}
}
