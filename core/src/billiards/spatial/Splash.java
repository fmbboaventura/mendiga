package billiards.spatial;

import billiards.spatial.node.SplashNode;
import MEnDiGa.spatial.Spatial;

public class Splash extends Spatial{

	public Splash(String id) {
		super(id);
		this.addNode(new SplashNode());
	}

}
