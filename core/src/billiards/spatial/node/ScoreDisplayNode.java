package billiards.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Node;
import MEnDiGa.spatial.nodes.TextNode;

public class ScoreDisplayNode extends Node{

	public ScoreDisplayNode(String id) {
		super(id);
		TextNode text = new TextNode(this.getName(), "Score: ");
		this.addGraphicNode(text);
		
		Position p = new Position(this.getName(), 0, 0, 200);
		this.setCurrentLocation(p);
	}

}
