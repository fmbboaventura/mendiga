package billiards.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.PhysicsNode;
import MEnDiGa.spatial.nodes.SphereBound;

public class PocketElement extends Element{
	
	public static enum PocketAlignment{
		BOTTOM_LEFT,
		BOTTOM_MIDLE,
		BOTTOM_RIGHT,
		TOP_LEFT,
		TOP_MIDLE,
		TOP_RIGHT
	}

	public PocketElement(String id, PocketAlignment alignment) {
		super(id);
		this.setGroupId("pocket");
		SphereBound bounds = new SphereBound(1);
		this.setBounds(bounds);
		this.physicsNode = new PhysicsNode();
		this.physicsNode.setSolid(false);
		this.setPosition(alignment);
	}
	
	private void setPosition(PocketAlignment alignment){
		Position p = new Position(this.getName() + "_Position", 0, 0, 0);
		float x = 0;
		float y = 0;
		
		switch(alignment){
		case BOTTOM_LEFT:
			x = 4.5f;
			y = 4.5f;
			break;
		case BOTTOM_MIDLE:
			x = 20;
			y = 4;
			break;
		case BOTTOM_RIGHT:
			x = 35.5f;
			y = 4.5f;
			break;
		case TOP_LEFT:
			x = 4.5f;
			y = 23.5f;
			break;
		case TOP_MIDLE:
			x = 20;
			y = 24;
			break;
		case TOP_RIGHT:
			x = 35.5f;
			y = 23.5f;
			break;
		}
		p.set(x, y);
		this.setCurrentLocation(p);
	}

}
