package billiards.spatial.node;

import java.util.ArrayList;
import java.util.Iterator;

import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Velocity;


public class PoolBallElement extends CueBallElement {

	private static int lastNumber = 1;
	private int ballNumber;

	public PoolBallElement() {
		super("Ball_" + String.format("%02d", lastNumber));
		this.setGroupId("poolBall");
		ballNumber = lastNumber;
		lastNumber++;
		this.getPosition().setX(13);
		this.addProperty(new Velocity());
		((SpriteNode) this.getGraphicNode(name)).setCurrentFrame(getNumber());
		this.getPosition().setZ(getNumber() + 1);
	}

	public static Iterator<PoolBallElement> rack() {
		ArrayList<PoolBallElement> set = new ArrayList<>();
		PoolBallElement firstBall = null;
		float radius = 1;
		float diameter = 2 * radius;
		
		for(int i = 0; i < 4; i++){
			for(int j = 0; j <= i; j++){
				PoolBallElement ball = new PoolBallElement();
				if(j == 0){
					if(firstBall != null){
						ball.getPosition().set(firstBall.getPosition().getX() - diameter,
								firstBall.getPosition().getY() + radius);
					}
					firstBall = ball;
				}
				else {
					PoolBallElement lastBall = set.get(set.size()-1);
					ball.getPosition().set(lastBall.getPosition().getX(), 
							lastBall.getPosition().getY() - diameter);
				}
				
				set.add(ball);
			}
		}
		return set.iterator();
	}

	public int getNumber() {
		return ballNumber;
	}

}
