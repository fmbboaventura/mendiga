package billiards.spatial.node;

import billiards.spatial.PoolTable;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class BackgroundNode extends Node {

	public BackgroundNode(String id) {
		super(id);
		
		SpriteNode graphicNode = new SpriteNode(id, "res/billiards/table2.png");
		graphicNode.setScale(PoolTable.pixelsToWorld);
		this.addGraphicNode(graphicNode);
		
		this.currentLocation = new Position(id, 2, 2, 0);
		
	}

}
