package billiards.spatial.node.property;

import MEnDiGa.spatial.nodes.Property;

public class Foul extends Property{
	private int count = 0;
	
	public void increment(){
		setChanged(true);
		count++;
	}
	
	public int getCount(){
		setChanged(false);
		return count;
	}
}
