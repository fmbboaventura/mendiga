package billiards.spatial.node.property;

import MEnDiGa.spatial.nodes.Property;

public class Flags extends Property{
	public boolean shooting;
	private boolean shootingAllowed = true;
	
	public void setShootingAllowed(boolean shootingAllowed){
		this.shootingAllowed = shootingAllowed;
	}
	
	public boolean getShootingAllowed(){
		return this.shootingAllowed;
	}
}
