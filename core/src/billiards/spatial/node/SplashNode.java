package billiards.spatial.node;

import billiards.spatial.PoolTable;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class SplashNode extends Node{

	public SplashNode() {
		super("splash");
		SpriteNode graphicNode = new SpriteNode(name, "res/billiards/logo.png");
		graphicNode.setScale(2*PoolTable.pixelsToWorld);
		this.addGraphicNode(graphicNode);
		this.currentLocation = new Position(name, 0, 0, 0);
	}

}
