package billiards.spatial.node;

import billiards.spatial.PoolTable;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.PhysicsNode;
import MEnDiGa.spatial.nodes.SphereBound;
import MEnDiGa.spatial.nodes.Velocity;

public class CueBallElement extends Element {
	
	public static final Position STARTING_POSITION = new Position("start", 
			28, 14, 1);

	public CueBallElement(String id) {
		super(id);
		this.setGroupId("cueBall");

		SphereBound bounds = new SphereBound(1);
		this.setBounds(bounds);
		
		this.physicsNode = new PhysicsNode();
		this.physicsNode.setDensity(1);
		this.physicsNode.setRestituition(0.9f);
		this.physicsNode.setFriction(0.3f);
		this.physicsNode.setLinearDamping(0.5f);
		this.physicsNode.setAngularDamping(0.5f);
		this.physicsNode.setRotationAllowed(true);
		
		Velocity v = new Velocity();
		this.addProperty(v);
		
		Position p = new Position(this.getName() + "_Position", 28, 
				14, 1);
		this.setCurrentLocation(p);
		
		SpriteNode graphicNode = new SpriteNode(this.name, "res/billiards/balls.png", 
				32, 32);
		graphicNode.setCurrentFrame(0);
		graphicNode.setScale(2*PoolTable.pixelsToWorld);
		graphicNode.setOriginX(16);
		graphicNode.setOriginY(16);
		graphicNode.setOffset(-16f, -16f);
		this.addGraphicNode(graphicNode);
	}

	public Position getPosition() {
		return (Position) this.getCurrentLocation();
	}
}