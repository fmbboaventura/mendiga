package billiards.spatial.node;

import billiards.spatial.node.property.Flags;
import billiards.spatial.node.property.Foul;
import billiards.spatial.node.property.ShootingForce;
import MEnDiGa.spatial.nodes.Player;
import MEnDiGa.spatial.nodes.Score;

public class BilliardsPlayer extends Player{

	public BilliardsPlayer() {
		super("BilliardsPlayer");
		this.addProperty(new Score());
		this.addProperty(new Flags());
		this.addProperty(new ShootingForce());
		this.addProperty(new Foul());
	}

}
