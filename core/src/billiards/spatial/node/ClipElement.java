package billiards.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.PhysicsNode;
import MEnDiGa.spatial.nodes.PolygonBound;

public class ClipElement extends Element {
	
	public static enum Clip{
		LEFT,
		TOP_LEFT,
		TOP_RIGHT,
		RIGHT,
		BOTTOM_RIGHT,
		BOTTOM_LEFT;
	}

	public ClipElement(String id, Clip clip) {
		super(id);
		this.physicsNode = new PhysicsNode();
		this.physicsNode = new PhysicsNode();
		this.physicsNode.setDensity(0.8f);
		this.physicsNode.setRestituition(0.3f);
		this.physicsNode.setFriction(0.6f);
		
		PolygonBound bounds = new PolygonBound();
		bounds.vertices = createVertices(clip);
		this.setBounds(bounds);
		setPosition(clip);
	}
	
	private float[] createVertices(Clip clip) {
		float[] vertices = null;
		float[] left = { 0, 0, 2, 0, 3, 1, 3, 15, 2, 16, 0, 16 };
		float[] bottomLeft = { 0, 0, 0, 2, 1, 3, 12, 3, 12.5f, 2, 12.5f, 0};
		float[] topLeft = {0, 3, 0, 1, 1, 0, 12, 0, 12.5f, 1, 12.5f, 3};

		switch (clip) {
		case LEFT:
			vertices = left;
			break;
		case BOTTOM_LEFT:
			vertices = bottomLeft;
			break;
		case BOTTOM_RIGHT:
			vertices = flipPolygon(bottomLeft);
			break;
		case RIGHT:
			vertices = flipPolygon(left);
			break;
		case TOP_RIGHT:
			vertices = flipPolygon(topLeft);
			break;
		case TOP_LEFT:
			vertices = topLeft;
			break;
		}

		return vertices;
	}

	private float[] flipPolygon(float[] vertices) {
		float[] max = max(vertices);
		float maxX = max[0];
		
		for (int i = 0; i < vertices.length; i++) {
			if(i % 2 == 0 ){
				vertices[i] = maxX - vertices[i];
			}
		}
		
		return vertices;
	}
	
	private float[] max(float[] values) {
		float maxX = 0;
		float maxY = 0;

		for (int i = 0; i < values.length; i++) {
			if (i % 2 == 0)
				if (values[i] > maxX)
					maxX = values[i];
			
			if (values[i] > maxY)
				maxY = values[i];
		}

		return new float[]{maxX, maxY};
	}
	
	private void setPosition(Clip clip){
		Position position = new Position(name + "_position", 0, 0, 0);
		float x = 0;
		float y = 0;
		
		switch (clip) {
		case LEFT:
			x = 2;
			y = 6;
			break;
		case BOTTOM_LEFT:
			x = 6;
			y = 2;
			break;
		case BOTTOM_RIGHT:
			// largura da ca�apa da esquerda + largura do clip node + largura da cacapa do meio
			x = 21.5f;
			y = 2;
			break;
		case RIGHT:
			x = 35;
			y = 6;
			break;
		case TOP_RIGHT:
			x = 21.5f;
			y = 23;
			break;
		case TOP_LEFT:
			x = 6;
			y = 23;
			break;
		}
		
		position.set(x, y);
		this.setCurrentLocation(position);
	}

}
