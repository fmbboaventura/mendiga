package billiards;

import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.observers.InputObserver;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.nodes.Player;
import billiards.behavior.BeginShot;
import billiards.behavior.ClearBall;
import billiards.behavior.ResetCueBall;
import billiards.behavior.ShootBehavior;
import billiards.behavior.UpdateShootStrenght;
import billiards.observer.BallVelocityObsever;
import billiards.observer.CueBallPocketCollisionbserver;
import billiards.observer.FoulObserver;
import billiards.observer.PoolBallPocketCollisionObserver;
import billiards.spatial.PoolTable;
import billiards.spatial.Splash;
import billiards.spatial.node.BilliardsPlayer;

public class BilliardsGame extends GameSession {
	
	public BilliardsGame(){
		Splash splash = new Splash("splash");
		PoolTable poolTable = new PoolTable("poolRoom");
		this.setCurrentSpatial(poolTable);
		this.addSpatial(splash.getId(), splash);
		this.addSpatial(poolTable.getId(), poolTable);
		
		Player player = new BilliardsPlayer();
		//poolTable.addNode(player);
		
		Behavior beginShoot = new BeginShot(player, poolTable.getCueBall());
		Behavior updateShootingStrenght = new UpdateShootStrenght(player, poolTable.getCueBall());
		Behavior shoot = new ShootBehavior(player, poolTable.getCueBall());
		Behavior clearBall = new ClearBall(poolTable);
		Behavior resetCueBall = new ResetCueBall(poolTable, player);
		this.addBehavior("beginShoot", beginShoot);
		this.addBehavior("updateStrenght", updateShootingStrenght);
		this.addBehavior("shoot", shoot);
		this.addBehavior("clearBall", clearBall);
		this.addBehavior("resetCueBall", resetCueBall);
		
		player.setPlayerBehavior(Player.TOUCH_DOWN, beginShoot);
		player.setPlayerBehavior(Player.TOUCH_DRAG, updateShootingStrenght);
		player.setPlayerBehavior(Player.TOUCH_UP, shoot);
		
		Observer poolBallPocket = new PoolBallPocketCollisionObserver();
		poolBallPocket.addBehavior(clearBall);
		
		Observer cueBallPocket = new CueBallPocketCollisionbserver();
		cueBallPocket.addBehavior(resetCueBall);
		
		Observer ballSpeed = new BallVelocityObsever(poolTable, player);
		
		Observer foulObserver = new FoulObserver(player);
		foulObserver.addBehavior(resetCueBall);
		
		this.addObserver("input", new InputObserver("input", player));
		this.addObserver(poolBallPocket);
		this.addObserver(cueBallPocket);
		this.addObserver(ballSpeed);
		this.addObserver(foulObserver);
	}
	
	public void update(){
		this.getObserver("velocityObserver").fire();
		this.getObserver("foulObserver").fire("reset");
	}
}
