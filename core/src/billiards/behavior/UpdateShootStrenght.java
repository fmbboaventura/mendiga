package billiards.behavior;

import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.Player;
import billiards.spatial.node.CueBallElement;
import billiards.spatial.node.property.Flags;
import billiards.spatial.node.property.ShootingForce;

public class UpdateShootStrenght extends Behavior {
	private CueBallElement cueBall;
	private Player player;

	public UpdateShootStrenght(Player player, CueBallElement cueBall) {
		super();
		this.cueBall = cueBall;
		this.player = player;
	}

	@Override
	public void execute(Object... params) {
		if (params != null) {
			float x = ((Float) params[1]).floatValue();
			float y = ((Float) params[2]).floatValue();

			if(player.getProperty(Flags.class).shooting){
				ShootingForce sf = player.getProperty(ShootingForce.class);
				sf.x = cueBall.getPosition().getX() - x;
				sf.y = cueBall.getPosition().getY() - y;
			}
		}
	}

}
