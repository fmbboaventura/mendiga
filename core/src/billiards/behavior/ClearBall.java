package billiards.behavior;

import MEnDiGa.behaviors.Behavior;
import billiards.spatial.PoolTable;
import billiards.spatial.node.PoolBallElement;

public class ClearBall extends Behavior {
	
	private PoolTable table;

	public ClearBall(PoolTable table){
		this.table = table;
	}

	@Override
	public void execute(Object... params) {
		if(params != null){
			PoolBallElement ball = null;
			if(params[1] instanceof PoolBallElement){
				ball = (PoolBallElement) params[1];
			}
			else ball = (PoolBallElement) params[2];
			
			table.removeNode(ball);
		}
	}

}
