package billiards.behavior;

import billiards.spatial.PoolTable;
import billiards.spatial.node.CueBallElement;
import billiards.spatial.node.property.Foul;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.Player;
import MEnDiGa.spatial.nodes.Velocity;

public class ResetCueBall extends Behavior{
	private PoolTable table;
	private Player player;

	public ResetCueBall(PoolTable table, Player player) {
		this.table = table;
		this.player = player;
	}

	@Override
	public void execute(Object... params) {
		if(params != null){
			
			if(params[0].equals("Contact Begin")){
				table.removeNode(table.getCueBall());
				this.player.getProperty(Foul.class).increment();
			}
			else if(params[0].equals("reset")){
				CueBallElement cueBall = table.getCueBall();
				cueBall.getProperty(Velocity.class).zero();
				cueBall.setCurrentLocation(CueBallElement.STARTING_POSITION);
				table.restoreCueBall();
				this.player.getProperty(Foul.class).setChanged(false);
			}
		}
	}

}
