package billiards.behavior;

import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.Player;
import billiards.spatial.node.CueBallElement;
import billiards.spatial.node.property.Flags;

public class BeginShot extends Behavior{
	private CueBallElement cueBall;
	private Player player;

	public BeginShot(Player player, CueBallElement cueBall) {
		this.player = player;
		this.cueBall = cueBall;
	}

	@Override
	public void execute(Object... params) {
		if (params != null) {
			float x = ((Float) params[1]).floatValue();
			float y = ((Float) params[2]).floatValue();
			
			float dx = cueBall.getPosition().getX() - x;
			float dy = cueBall.getPosition().getY() - y;
			float radius = 1;
			
			boolean ballClicked = dx * dx + dy * dy <= radius * radius;
			
			Flags flags = player.getProperty(Flags.class);

			if (ballClicked && flags.getShootingAllowed()) {
				flags.shooting = true;
			}
		}
	}
}
