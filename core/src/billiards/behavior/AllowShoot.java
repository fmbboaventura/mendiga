package billiards.behavior;

import billiards.spatial.node.property.Flags;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.Player;

public class AllowShoot extends Behavior {
	private Player player;
	private Flags flags;
	
	public AllowShoot(Player player) {
		this.player = player;
		this.flags = player.getProperty(Flags.class);
	}

	@Override
	public void execute(Object... params) {
		flags.setShootingAllowed(true);
	}

}
