package billiards.behavior;

import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.Player;
import MEnDiGa.spatial.nodes.Velocity;
import billiards.spatial.node.CueBallElement;
import billiards.spatial.node.property.Flags;
import billiards.spatial.node.property.ShootingForce;

public class ShootBehavior extends Behavior {
	private CueBallElement cueBall;
	private Player player;

	public ShootBehavior(Player player, CueBallElement cueBall) {
		super();
		this.player = player;
		this.cueBall = cueBall;
	}

	@Override
	public void execute(Object... params) {
		Flags flags = player.getProperty(Flags.class);
		if(flags.shooting){
			Velocity v = cueBall.getProperty(Velocity.class);
			ShootingForce sf = player.getProperty(ShootingForce.class);
			v.setX(sf.x * 10);
			v.setY(sf.y * 10);
			flags.shooting = false;
			flags.setShootingAllowed(false);
		}
	}

}
