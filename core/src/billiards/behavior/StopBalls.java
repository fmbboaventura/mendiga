package billiards.behavior;

import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.Velocity;

public class StopBalls extends Behavior {

	@Override
	public void execute(Object... params) {
		for(Object o : params){
			if (!(o instanceof Element)) continue;
			Element e = (Element) o;
			Velocity v = e.getProperty(Velocity.class);
			if(v != null) v.zero();
		}
	}

}
