package billiards.observer;

import java.util.LinkedList;

import billiards.behavior.AllowShoot;
import billiards.behavior.StopBalls;
import billiards.spatial.PoolTable;
import billiards.spatial.node.CueBallElement;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.Node;
import MEnDiGa.spatial.nodes.Player;
import MEnDiGa.spatial.nodes.Velocity;

public class BallVelocityObsever extends Observer {

	private PoolTable table;
	float minX = 0.5f;
	float minY = 0.5f;
	private Player player;

	public BallVelocityObsever(PoolTable table, Player player) {
		super("velocityObserver");
		this.table = table;
		this.player = player;
	}

	@Override
	public boolean evaluate(Object... params) {
		LinkedList<Element> ballsToStop = new LinkedList<>();
		LinkedList<Element> stopedBalls = new LinkedList<>();
		int ballCount = 0;
		for (Node n : table.getNodes()) {
			if (!(n instanceof CueBallElement))
				continue;

			CueBallElement e = (CueBallElement) n;
			Velocity v = e.getProperty(Velocity.class);
			
			if (v != null) {
				ballCount++;
				if (!v.isZero() && Math.abs(v.getX()) <= this.minX
						&& Math.abs(v.getY()) <= this.minY) {
					ballsToStop.add(e);
				} else if (v.isZero()) {
					stopedBalls.add(e);
				}
			}
		}
		if(stopedBalls.size() == ballCount){
			new AllowShoot(player).execute();
		}
		else{
			new StopBalls().execute(ballsToStop.toArray());
		}
		return false;
	}
}
