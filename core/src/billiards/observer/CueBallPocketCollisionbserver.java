package billiards.observer;

import MEnDiGa.observers.CollisionGroupObserver;

public class CueBallPocketCollisionbserver extends CollisionGroupObserver {
	
	public CueBallPocketCollisionbserver() {
		super("cueBallPocket", "cueBall", "pocket");
	}

	@Override
	public boolean evaluate(Object... params) {
		if(params != null){
			String eventType = (String) params[0];
			
			if(eventType.endsWith("Begin")){
				return true;
			}
		}
		return false;
	}

}
