package billiards.observer;

import MEnDiGa.observers.CollisionGroupObserver;

public class PoolBallPocketCollisionObserver extends CollisionGroupObserver{

	public PoolBallPocketCollisionObserver() {
		super("poolBallPocket", "poolBall", "pocket");
	}

	@Override
	public boolean evaluate(Object... params) {
		if(params != null){
			String eventType = (String) params[0];
			
			if(eventType.endsWith("Begin")){
				return true;
			}
		}
		return false;
	}

}
