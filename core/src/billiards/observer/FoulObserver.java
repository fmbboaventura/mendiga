package billiards.observer;

import billiards.spatial.PoolTable;
import billiards.spatial.node.property.Flags;
import billiards.spatial.node.property.Foul;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.nodes.Player;

public class FoulObserver extends Observer {

	private Player player;

	public FoulObserver(Player player) {
		super("foulObserver");
		this.player = player;
	}

	@Override
	public boolean evaluate(Object... params) {
		Flags flags = player.getProperty(Flags.class);
		Foul foulCounter = player.getProperty(Foul.class);
		
		if(flags.getShootingAllowed() && foulCounter.hasChanged()){
			return true;
		}
		return false;
	}

}
