package MEnDiGa.observers;

import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.Player;

public class InputObserver extends Observer {

	Player player;
	
	public InputObserver(String id, Player player) {
		super(id);
		this.player = player; 
	}

	@Override
	public boolean evaluate(Object... params) {
		if (params != null && params[0] != null){			
			if(!player.getCurrentCommand().equals(Player.BUTTON1_PRESS))
				player.setCurrentCommand((String) params[0]);
			
			Behavior behavior = player.getPlayerBehavior(player.getCurrentCommand());//(String) params[0]);
			if (behavior != null) behavior.execute(params);
		}
		
		return false;
	}
}
