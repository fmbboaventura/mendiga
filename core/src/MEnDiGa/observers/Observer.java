package MEnDiGa.observers;
import MEnDiGa.behaviors.Behavior;

import java.util.Iterator;
import java.util.LinkedList;

public abstract class Observer {
	protected String id;
	protected LinkedList<Behavior> behaviorsToPerform = new LinkedList<Behavior>();
	
	public Observer(String id) {
		this.id = id;
	}
	
	public String getId() {
		return id;
	}

	public void addBehavior(Behavior behavior){
		this.behaviorsToPerform.add(behavior);
	}
	
	public Behavior removeBehavior(Behavior behavior){
		Behavior result = null;
		
		if (behavior != null){
			Iterator<Behavior> iter = this.behaviorsToPerform.iterator();
			while (iter.hasNext() && result == null){
				Behavior temp = iter.next();
				if (behavior.equals(temp)){
					result = temp;
					this.behaviorsToPerform.remove(temp);
				}
			}
		}
		
		return result;
	}
		
	public void fire(Object... params){
		if (evaluate(params)){
			Iterator<Behavior> iter = this.behaviorsToPerform.iterator();
			while (iter.hasNext()){
				iter.next().execute(params);
			}
		}
	}
	
	public abstract boolean evaluate(Object... params);
	
}
