package MEnDiGa.observers;

public abstract class CollisionGroupObserver extends Observer{
	protected String collidedGroup1, collidedGroup2;

	public CollisionGroupObserver(String id, String collidedGroup1, String collidedGroup2) {
		super(id);
		this.collidedGroup1 = collidedGroup1;
		this.collidedGroup2 = collidedGroup2;
	}
	
	public String getCollidedGroup1(){
		return collidedGroup1;
	}
	
	public String getCollidedGroup2(){
		return collidedGroup2;
	}
}
