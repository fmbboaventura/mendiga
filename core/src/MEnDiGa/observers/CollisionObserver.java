package MEnDiGa.observers;

import MEnDiGa.spatial.nodes.Node;

public abstract class CollisionObserver extends Observer{
	Node colidedNode1, colidedNode2;

	public CollisionObserver(String id, Node colidedNode1, Node colidedNode2) {
		super(id);
		this.colidedNode1 = colidedNode1;
		this.colidedNode2 = colidedNode2;
	}
	
}
