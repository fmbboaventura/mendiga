package MEnDiGa.adapters.libgdx;

import MEnDiGa.spatial.nodes.AudioNode;

public abstract class AudioAdapter {

	protected AudioNode node;

	public AudioAdapter(AudioNode node) {
		this.node = node;
	}

	public abstract void stop();

	public abstract void pause();

	public abstract void update();

}