package MEnDiGa.adapters.libgdx.phisics;

import MEnDiGa.observers.CollisionGroupObserver;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.nodes.Node;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;

import java.util.HashMap;
import java.util.Iterator;

/**
 * <p>
 * Adapter responsible for notifying the registered {@link Observer} instances
 * about the ocurrency of collision related events.
 * </p>
 * 
 * @author Filipe Boaventura
 *
 */
public class CollisionAdapter implements ContactListener{

	public static final String CONTACT_BEGIN = "Contact Begin";
	public static final String CONTACT_END = "Contact End";
	private HashMap<String, Observer> observers = new HashMap<String, Observer>();
	private HashMap<String, Observer> beginContactObservers = new HashMap<String, Observer>();
	private HashMap<String, Observer> endContactObservers = new HashMap<String, Observer>();
	//private HashMap<String, CollisionObserver> collisionObservers = new HashMap<String, CollisionObserver>();
	private HashMap<String, CollisionGroupObserver> collisionGroupObservers = new HashMap<String, CollisionGroupObserver>();

	/**
	 * <p>
	 * Register a observer that will be notified about every collision related event.
	 * </p> 
	 * @param observer - The {@link Observer} to add.
	 */
	public void addObserver(Observer observer){
		if(observer != null) this.observers.put(observer.getId(), observer);
	}

	public void addObserver(Observer observer, String eventType){
		switch(eventType){
		case CONTACT_BEGIN:
			this.beginContactObservers.put(observer.getId(), observer);
			break;
		case CONTACT_END:
			this.endContactObservers.put(observer.getId(), observer);
			break;
		default:
			throw new IllegalArgumentException("Invalid eventType");
		}

	}
	
	public void addCollisionGroupObserver(CollisionGroupObserver observer){
		if(observer != null){
			String key = observer.getCollidedGroup1() + observer.getCollidedGroup2();
			collisionGroupObservers.put(key, observer);
		}
	}

	@Override
	public void beginContact(Contact contact) {
		notifyObservers(CONTACT_BEGIN, contact);
	}

	@Override
	public void endContact(Contact contact) {
		notifyObservers(CONTACT_END, contact);
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse contactImpluse) {
		// TODO Auto-generated method stub

	}

	@Override
	public void preSolve(Contact contact, Manifold mainfold) {
		// TODO Auto-generated method stub

	}

	private Object[] createParams(String eventType, Contact contact){
		Fixture fixtureA = contact.getFixtureA();
		Fixture fixtureB = contact.getFixtureB();
		Node colidedNode1 = null, colidedNode2 = null;
		
		if(fixtureA != null) colidedNode1 = (Node) fixtureA.getBody().getUserData();
		if(fixtureB != null) colidedNode2 = (Node) fixtureB.getBody().getUserData();

		Object[] params = new Object[3];
		params[0] = eventType;

		if(colidedNode1 != null) params[1] = colidedNode1;
		if(colidedNode2 != null) params[2] = colidedNode2;

		return params;
	}

	private void notifyObservers(String eventType, Contact contact){
		Iterator<Observer> generalObservers = this.observers.values().iterator();
		Iterator<Observer> specificEventObservers;
		Object[] params = createParams(eventType, contact);

		if(eventType.equals(CONTACT_BEGIN)){
			specificEventObservers = this.beginContactObservers.values().iterator();
		}
		// No futuro, os outros eventos ser�o tratados
		else /*if(eventType.equals(CONTACT_END))*/{
			specificEventObservers = this.endContactObservers.values().iterator();
		}
		
		// Notificando Collision Group Observers
		if(params[1] != null && params[2] != null){
			String key = ((Node) params[1]).getGroupId() + ((Node) params[2]).getGroupId();
			String reverseKey = ((Node) params[2]).getGroupId() + ((Node) params[1]).getGroupId();
			
			if(collisionGroupObservers.get(key) != null) {
				collisionGroupObservers.get(key).fire(params);
			}
			else if(collisionGroupObservers.get(reverseKey) != null) {
				collisionGroupObservers.get(reverseKey).fire(params);
			}
		}

		// REVER
		while(generalObservers.hasNext() || specificEventObservers.hasNext()){
			if(generalObservers.hasNext()) generalObservers.next().fire(params);
			if(specificEventObservers.hasNext()) specificEventObservers.next().fire(params);
		}
	}

}
