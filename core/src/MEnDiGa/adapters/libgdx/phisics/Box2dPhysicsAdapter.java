/**
 * 
 */
package MEnDiGa.adapters.libgdx.phisics;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;

import MEnDiGa.observers.CollisionGroupObserver;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoundingVolume;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.GraphicNode;
import MEnDiGa.spatial.nodes.Node;
import MEnDiGa.spatial.nodes.PhysicsNode;
import MEnDiGa.spatial.nodes.PolygonBound;
import MEnDiGa.spatial.nodes.SphereBound;
import MEnDiGa.spatial.nodes.Velocity;

import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.PolygonMapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;

/**
 * <p>
 * Provides methods for managing physics simulation and collision related
 * events. This class translates {@link Element} and .tmx Tiled Map Objects
 * instances into Box2D Bodies and also offers methods to physically manipulate
 * them (apply forces, velocity and so on). It also notify registered observer
 * instances about collision events.
 * </p>
 * 
 * @author Filipe Boaventura
 *
 */
public class Box2dPhysicsAdapter {
	
	private class BodyArgBundle{
		Object userData;
		BodyDef bDef;
		FixtureDef fDef;
		
		private BodyArgBundle(Object userData, BodyDef bDef, FixtureDef fDef) {
			this.userData = userData;
			this.bDef = bDef;
			this.fDef = fDef;
		}
	}
	
	private class TransformArgBundle{
		float x, y, angle;
		Body body;

		private TransformArgBundle(Body body, float x, float y, float angle) {
			super();
			this.body = body;
			this.x = x;
			this.y = y;
			this.angle = angle;
		}
	}
	
	/** <p>
	 * The object layer containing the obstacles must be 
	 * called CollisionLayer. This layer is retrieved from
	 * the {@link TileMap} instance and its objects are used
	 * to create static Box2D bodies to be used as obstacles.
	 * </p>
	 */
	public static final String COLLISION_LAYER = "CollisionLayer";
	
	public static final String ELEMENT_LAYER = "ElementLayer";
	
	public static final String GROUP_ID = "groupId";
	
	/** <p>
	 * Identifier constant of Box2D body types. Use it as the
	 * key in the {@link Element.setProperty} method. Use one 
	 * of the following constants for the property value:
	 * {@link Box2dPhysicsAdapter.STATIC_BODY}; 
	 * {@link Box2dPhysicsAdapter.DYNAMIC_BODY} or 
	 * {@link Box2dPhysicsAdapter.KINEMATIC_BODY}</p>
	 */
	public static final String BODY_TYPE = "BodyType";
	
	public static final String STATIC_BODY = "StaticBody";
	
	public static final String DYNAMIC_BODY = "DynamicBody";
	
	public static final String KINEMATIC_BODY = "KinematicBody";
	
	public static final String BOUNDING_BOX = "box";
	
	public static final String BOUNDING_POLYGON = "Polygon";
	
	public static final String BOUNDING_CIRCLE = "circle";
	
	public static final String IS_SENSOR = "isSensor";
	
	private World world;
	private Box2DDebugRenderer debugRenderer;
	private float timeStep;
	private int velocityIterations, positionIterations;
	private HashMap<String, Body> elementBodies;
	private Queue<Body> toDestroy;
	private Queue<BodyArgBundle> toCreate;
	private Queue<TransformArgBundle> toTransform;
	private CollisionAdapter collisionAdapter;
	private float unitScale = 1;

	private Spatial spatial;
	
	public Box2dPhysicsAdapter(Spatial s, float gravityX, float gravityY) {
		toDestroy = new LinkedList<Body>();
		toCreate = new LinkedList<BodyArgBundle>();
		toTransform = new LinkedList<>();
		world = new World(new Vector2(gravityX, gravityY), true);
		debugRenderer = new Box2DDebugRenderer();
		timeStep = 1/60f;
		velocityIterations = 6;
		positionIterations = 2;
		elementBodies = new HashMap<String, Body>();
		collisionAdapter = new CollisionAdapter();
		world.setContactListener(collisionAdapter);
		this.spatial = s;
	}
	
	public Box2dPhysicsAdapter(Spatial s, float gravityX, float gravityY, float unitScale){
		this(s, gravityX, gravityY);
		this.unitScale = unitScale;
	}
	
	/**<p>
	 * Creates Box2D static bodies for each map object inside a
	 * certain object layer of the given tmx TileMap instance 
	 * (Circle, Line and Ellipse map objects are not yet supported).
	 * The layer containing the obstacles must be named after 
	 * the {@link Box2dPhysicsAdapter} constant. Also keep in mind
	 * that Box2D can only create convex polygons.
	 * </p>
	 * 
	 * @param map
	 */
	public void createTmxMapObstacles(TiledMap map) {
		MapObjects mapObjects = map.getLayers()
				.get(Box2dPhysicsAdapter.COLLISION_LAYER).getObjects();
		
		for(MapObject obstacle : mapObjects) {
			
			float x = 0, y = 0;
			Shape shape = null;
			
			if(obstacle instanceof RectangleMapObject){
				RectangleMapObject block = (RectangleMapObject) obstacle;
			
				// Pega a posi��o do RectangleMapObjecct e converte
				// para a escala de unidade em uso
				x = block.getRectangle().x*unitScale;
				y = block.getRectangle().y*unitScale;
			
				float halfWidht = (block.getRectangle().width/2)*unitScale;
				float halfHeight = (block.getRectangle().height/2)*unitScale;
			    
				// Configura o shape do b2dBody para um retangulo
				// usando os dados do mapObject desenhado no tiled
				shape = new PolygonShape();
				
				/* Par�metros:
				 * Metade da altura e largura
				 * A posi��o centro da caixa
				 * A rota��o
				 */
				((PolygonShape) shape).setAsBox(halfWidht, halfHeight, 
						new Vector2(halfWidht, halfHeight), 0);
			}
			else if(obstacle instanceof PolygonMapObject){
				// Configura o shape do b2dBody como um pol�gono customizado
				PolygonMapObject polygonBlock = (PolygonMapObject) obstacle;
				
				x = polygonBlock.getPolygon().getX()*unitScale;
				y = polygonBlock.getPolygon().getY()*unitScale;
				
				shape = new PolygonShape();
				
				// M�todo para aplicar a unitScale nos vertices
				float[] vertices = this.calculateVertices(
						polygonBlock.getPolygon().getVertices());
				
				((PolygonShape) shape).set(vertices);
			}
			
			FixtureDef fDef = new FixtureDef();
			fDef.shape = shape;
			
			BodyDef bDef = new BodyDef();
			bDef.type = BodyDef.BodyType.StaticBody;
			bDef.position.set(x, y);
			
			Element mapElement = new Element(obstacle.getName() + obstacle.hashCode());
			Iterator<String> propertyKeys = obstacle.getProperties().getKeys();
			
			BodyArgBundle args = new BodyArgBundle(mapElement, bDef, fDef);
			
			// TODO: o que fazer com isso??
			while(propertyKeys.hasNext()){
				String key = propertyKeys.next();
				Object value = obstacle.getProperties().get(key);
				mapElement.setProperty(key, value);
			}
			
			mapElement.setGroupId((String) obstacle.getProperties().get(GROUP_ID));
			
			if(obstacle.getProperties().get(IS_SENSOR) != null){
				fDef.isSensor = Boolean.parseBoolean((String) 
						obstacle.getProperties().get(IS_SENSOR));
			}
			
			
			if(world.isLocked()){
				toCreate.add(args);
			}
			else{
				createBody(args);
			}
		}
	}
	
	private float[] calculateVertices(float[] vertices) {
		float[] result = new float[vertices.length];
		
		for (int i = 0; i < vertices.length; i++) {
			result[i] = vertices[i] * unitScale;
		}
		return result;
	}

	/**
	 * <p>
	 * Creates a Box2D using the informations contained in the 
	 * {@link Element}'s properties and {@link BoundingVolume}.
	 * The given element instance is stored in the {@link Body}
	 * using {@link Body.setUserData()}.
	 * </p>
	 * 
	 * @param element - A {@link Element} instance to create its body.
	 */
	public void createElementBody(Element element) {
		if(element.getPhysicsNode() == null) return;
		BodyDef bDef = new BodyDef();
		if(!element.hasProperty(Velocity.class) && 
				element.getPhysicsNode() != null){
			bDef.type = BodyType.StaticBody;
		} else {
			bDef.type = BodyType.DynamicBody;
		}
		
		Position p = (Position) element.getCurrentLocation();
		bDef.position.set(p.getX(), p.getY());
		
		FixtureDef fDef = new FixtureDef();
		BoundingVolume bounds = element.getBounds();
		Shape elementShape = null;
		
		if(bounds instanceof BoxBound){
			BoxBound box = (BoxBound) bounds;
			elementShape = new PolygonShape();
			((PolygonShape)elementShape).setAsBox(
					box.width/2, box.height/2,
					new Vector2(box.width/2, box.height/2), 0);
		}
		else if(bounds instanceof SphereBound){
			float radius = ((SphereBound)bounds).radius;
			elementShape = new CircleShape();
			((CircleShape)elementShape).setRadius(radius);
		}
		else if(bounds instanceof PolygonBound){
			elementShape = new PolygonShape();
			PolygonBound polygon = (PolygonBound) bounds;
			((PolygonShape)elementShape).set(polygon.vertices);
		}
//		switch(bounds.type) {
//			case BOUNDING_BOX:
//				elementShape = new PolygonShape();
//				((PolygonShape)elementShape).setAsBox(
//						bounds.width/2, bounds.height/2,
//						new Vector2(bounds.width/2, bounds.height/2), 0);
//				break;
//			case BOUNDING_POLYGON:
//				elementShape = new PolygonShape();
//				((PolygonShape)elementShape).set(bounds.vertices);
//				break;
//			case BOUNDING_CIRCLE:
//				elementShape = new CircleShape();
//				((CircleShape)elementShape).setRadius(bounds.radius);
//				fDef.restitution = 0.5f;
//				fDef.friction = 0.3f;
//				if(element.getId().equals("PoolBall")){
//					fDef.density = 101.356358f;
//				}else{
//					fDef.density = 58.474822f;
//				}
//				System.out.println(element.getId() + " " +bounds.radius);
//				break;
//		}
		PhysicsNode physicsNode = element.getPhysicsNode();
		fDef.shape = elementShape;
		fDef.isSensor = !physicsNode.isSolid();
		fDef.friction = physicsNode.getFriction();
		fDef.density = physicsNode.getDensity();
		fDef.restitution = physicsNode.getRestituition();
		bDef.gravityScale = physicsNode.getGravityScale();
		bDef.angularDamping = physicsNode.getAngularDamping();
		bDef.linearDamping = physicsNode.getLinearDamping();
		
		BodyArgBundle bodyArgs = new BodyArgBundle(element, bDef, fDef);
		if(world.isLocked()){
			toCreate.add(bodyArgs);
		}
		else {
			this.createBody(bodyArgs);
		}
	}
	
	public void debugRender(Matrix4 projMatrix){
		this.debugRenderer.render(world, projMatrix);
	}
	
	public void step(){
		// Aplica as alteracoes agendadas
		while(!toDestroy.isEmpty()){
			world.destroyBody(toDestroy.remove());
		}
		while(!toCreate.isEmpty()){
			this.createBody(toCreate.remove());
		}
		this.applyTransform();
		
		// Da step na simulacao
		world.step(timeStep, velocityIterations, positionIterations);

		//Atualiza a posicao dos nodes conforme a posicao dos Box2d Bodies 
		for(Node n : spatial.getNodes()){
			if(this.getElementBody(n.getName()) == null
					&& n instanceof Element){
				this.createElementBody((Element) n);
				continue;
			}
			Position p = (Position) n.getCurrentLocation();
			if(p == null) continue;
			if(p.getId().equals("respawn")){
				n.setCurrentLocation(
						new Position("currentPosition", 
						p.getX(), p.getY(), p.getZ()));
				this.setElementBodyPosition(n.getName());
				return;
			}
			
			if (this.getElementBody(n.getName()) != null) {

				float[] pos = this.getElementPosition(n.getName());
				n.setCurrentLocation(new Position("currentPosition", pos[0],
						pos[1], p.getZ()));

				/*
				 * Verifica se exite velociade a ser aplicada no physics node.
				 * Como quem manipula a simulacao fisica e o box2d, velocity
				 * precisa ser nulo as vezes para que o motor de fisica possa
				 * computar a posicao levando em conta a simulacao fisica.
				 * Essencial para dar realismo a queda.
				 */
				if(n instanceof Element && ((Element)n).hasProperty(Velocity.class)){
					Velocity v  = ((Element)n).getProperty(Velocity.class);
					if(v.hasChanged()){
						this.applyVelocityToElement(n.getName(), v.getX(), v.getY());
					}
					else{
						float[] velocity = this.getElementVelocity(n.getName());
						v.setX(velocity[0]);
						v.setY(velocity[1]);
						v.setChanged(false);
					}
				}
				
				if(!n.getGraphicNodes().isEmpty()){
					for(GraphicNode gNode : n.getGraphicNodes()){
						if(!(gNode instanceof SpriteNode)) continue;
						((SpriteNode) gNode).setRotation(
								this.getElementBody(n.getName()).getAngle() 
								* MathUtils.radiansToDegrees);
					}
				}
			}
			
			/* Quando algum node � removido do spatial, seu box2d body
			 * deve ser removido do physicsAdapter. O m�todo last remo-
			 * vedNodeId retorna uma fila com os ids dos nodes que foram
			 * removidos.
			 */
			Queue<String> removedNodeId = this.spatial.lastRemovedNodeId();
			while(!removedNodeId.isEmpty()){
				this.destroyElementBody((removedNodeId.remove()));
			}
		}
	}
	
	public void renderAndStep(Matrix4 projMatrix) {
		this.debugRender(projMatrix);
		this.step();
	}
	
	private void applyVelocityToElement(String elementId, float speedX, float speedY){
		Body b = this.getElementBody(elementId);
		
		float deltaVx = speedX - b.getLinearVelocity().x;
		float deltaVy = speedY - b.getLinearVelocity().y;
		float impulseX = b.getMass() * deltaVx;
		float impulseY = b.getMass() * deltaVy;
		
		b.applyLinearImpulse(impulseX, impulseY,b.getWorldCenter().x, b.getWorldCenter().y, true);
	}
	
	/**
	 * <p>
	 * Returns an array containing the x and y coordinates of the
	 * box2D Body of the {@link Element} containing the given name.
	 * </p>
	 * @param elementId - The identifier of the desired element.
	 * @return An array containing the box2D body coordinates of
	 *         the element or null, if there is no element with 
	 *         the given name.
	 */
	public float[] getElementPosition(String elementId){
		Body b = this.getElementBody(elementId);
		
		if(b != null){
			return new float[]{b.getPosition().x, b.getPosition().y};
		}
		return null;
	}
	
	/**
	 * <p>
	 * Returns an array containing the linear velocity of the
	 * box2D Body of the {@link Element} containing the given name.
	 * </p>
	 * @param elementId - The identifier of the desired element.
	 * @return An array representing the velocity vector of the
	 *         element or null, if there is no element with the 
	 *         given name.
	 */
	public float[] getElementVelocity(String elementId){
		Body b = this.getElementBody(elementId);
		if(b != null) return new float[]{b.getLinearVelocity().x, b.getLinearVelocity().y};
		return null;
	}
	
	private void createBody(BodyArgBundle args){
		BodyDef bDef = args.bDef;
		FixtureDef fDef = args.fDef;
		Element element = (Element) args.userData;
		
		Body elementBody = world.createBody(bDef);
		elementBody.setUserData(element);
		elementBody.createFixture(fDef);
		fDef.shape.dispose();
		elementBodies.put(element.getName(), elementBody);
	}
	
	/**
	 * <p>
	 * Destroys the box2D Body of the {@link Element} that contains
	 * the given name. Keep in mind that the Body will not be destroyed
	 * immediately, since the box2D World might be locked. Instead, it
	 * will be added to a Queue and it will be destroyed before the next
	 * step() call.
	 * </p>
	 * 
	 * @param elementId - The identifier of the Element whose Body will be destroyed.
	 * @return - The element that possesses the given Id.
	 */
	public Element destroyElementBody(String elementId){
		Body destroyed = this.getElementBody(elementId);
		Element destroyedElement = null;
		if(destroyed != null) {
			//System.out.println(elementId);
			destroyedElement = (Element) destroyed.getUserData();
			elementBodies.remove(elementId);
			toDestroy.add(destroyed);
		}
		return destroyedElement;
	}
	
	/**
	 * <p>
	 * Destroys all box2D Bodies of conteined in the world. Keep in mind 
	 * that the Bodies will not be destroyed immediately, since the box2D
	 * World might be locked. Instead, they will be added to a Queue and 
	 * they will be destroyed before the next step() call.
	 * </p>
	 * 
	 * @return - A Collection containing the {@link Element}s associated 
	 * with the destroyed bodies;
	 */
	public Collection<Element> destroyAllBodies(){
		Array<Body> a = new Array<Body>(world.getBodyCount());
		Collection<Element> destroyedElements = new LinkedList<Element>();
		world.getBodies(a);
		
		for(Body b : a){
			toDestroy.add(b);
			destroyedElements.add((Element) b.getUserData());
		}
		
		return destroyedElements;
	}
	
	private Body getElementBody(String elementId){
		return elementBodies.get(elementId);
	}
	
	public void addObserver(Observer observer){
		collisionAdapter.addObserver(observer);
	}
	
	public void setUnitScale(float unitScale){
		this.unitScale = unitScale;
	}
	
	public void addCollisionGroupObserver(CollisionGroupObserver observer){
		collisionAdapter.addCollisionGroupObserver(observer);
	}

	public void setElementBodyTransform(String elementId, float angle) {
		// TODO Rever
		Body b = this.getElementBody(elementId);
		if(b != null){
			Element e = (Element) b.getUserData();
			float x = ((Position) e.getCurrentLocation()).getX();
			float y =  ((Position) e.getCurrentLocation()).getY();
			//b.setTransform(x, y, b.getAngle());
			toTransform.add(new TransformArgBundle(b, x, y, angle));
		}
	}
	
	public void setElementBodyPosition(String elementId){
		// TODO Rever
		Body b = this.getElementBody(elementId);
		if(b != null){
			Element e = (Element) b.getUserData();
			float x = ((Position) e.getCurrentLocation()).getX();
			float y =  ((Position) e.getCurrentLocation()).getY();
			//b.setTransform(x, y, b.getAngle());
			toTransform.add(new TransformArgBundle(b, x, y, b.getAngle()));
		}
	}
	
	public void setGravityScale(String elementId, float scale){
		this.getElementBody(elementId).setGravityScale(scale);
	}
	
	private void applyTransform(){
		while(!toTransform.isEmpty()){
			TransformArgBundle bundle = toTransform.remove();
			bundle.body.setTransform(bundle.x, bundle.y, bundle.angle);
		}
	}

	public int getElementBodyCount() {
		return elementBodies.size();
	}
}
