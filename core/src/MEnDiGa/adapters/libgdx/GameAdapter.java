package MEnDiGa.adapters.libgdx;

import java.util.Collection;
import java.util.HashMap;

import MEnDiGa.GameSession;
import MEnDiGa.adapters.libgdx.graphics.SpatialRenderer;
import MEnDiGa.adapters.libgdx.phisics.Box2dPhysicsAdapter;
import MEnDiGa.observers.CollisionGroupObserver;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.locations.Map;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.AudioNode;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.Node;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.maps.tiled.TiledMap;

public abstract class GameAdapter extends Game {
	protected float viewportWidth;
	protected float viewportHeight;
	protected float gravityX;
	protected float gravityY;
	protected String lastDrawedSpatial;
	protected InputAdapter input;
	protected HashMap<String, SpatialRenderer> renders = new HashMap<String, SpatialRenderer>();
	protected HashMap<String, Box2dPhysicsAdapter> physicsAdapter = new HashMap<String, Box2dPhysicsAdapter>();
	protected HashMap<String, AudioAdapter> audioAdapters = new HashMap<>();

	protected GameSession game;

	public void createAdapter(Spatial s){
		SpatialRenderer spatialRenderer = new SpatialRenderer(s, viewportWidth, viewportHeight);//20, 14);//2.5f, 1.875f);
		Box2dPhysicsAdapter physicsAdapter = new Box2dPhysicsAdapter(s, gravityX, gravityY, Map.worldUnitPerPixels);
		
		Collection<Node> nodes = s.getNodes();
		for(Node n : nodes){
			spatialRenderer.createGraphicAdapter(n);
			if(n instanceof Element)
			physicsAdapter.createElementBody((Element) n);
			
			
		}
		
		TiledMap createdMap = spatialRenderer.createTmxTileMap();
		if(createdMap != null)
		physicsAdapter.createTmxMapObstacles(createdMap);
		
		for(Observer o : game.getObservers()){
			if(o instanceof CollisionGroupObserver){
				physicsAdapter.addCollisionGroupObserver((CollisionGroupObserver) o);
			}
		}
		
		this.renders.put(s.getId(), spatialRenderer);
		this.physicsAdapter.put(s.getId(), physicsAdapter);
	}
	
	@Override
	public void render() {
		super.render();
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		// Pega o renderer e o phisics adapter do spatial atual
		String spatialToDraw = game.getCurrentSpatial().getId();
		SpatialRenderer currentRender = this.renders.get(spatialToDraw);
		Box2dPhysicsAdapter currentWorld = this.physicsAdapter.get(spatialToDraw);

		/*
		 * Se for nulo, significa que o spatial atual ainda nao teve seus
		 * recursos instanciados com objetos do libGdx. Isso acontece quando ha
		 * uma troca de spatial.
		 */
		if (currentRender == null && currentWorld == null) {
			this.createAdapter(game.getCurrentSpatial());
		} else {

			/*
			 * Se o spatial que sera desenhado nessa chamada do render() for
			 * diferente do ultimo desenhado, significa que o pitfall foi para
			 * outra tela. Quando isso acontece, o LoadLevelBehavior seta a nova
			 * posicao do pitfall no novo spatial. O trecho de codigo abaixo
			 * informa ao physics adapter a posicao que os nodes devem tomar no
			 * spatial atual. Isso e necessario porque? Por exemplo, caso o
			 * pitfall saia pelo canto superior direito, a instancia do
			 * physicsAdapter para aquele spatial sera guardada no estado em que
			 * se encontra: com o corpo do pitfall no canto superior direito.
			 * Caso o player volte pelo canto inferior esquerdo do outro nivel,
			 * ele deve aparecer no canto inferior direito do anterior. Para que
			 * isso de fato ocorra, a instancia do physicsAdapter anterior tem
			 * que ser atualizada com a posi��o correta dos nodes.
			 */
			if (lastDrawedSpatial != null
					&& !lastDrawedSpatial.equals(spatialToDraw)) {
				this.updateNodesPosition(currentWorld);
			} else {
				currentRender.drawTmxTileMap();
				currentRender.beginRender();
				
				for (Node n : game.getCurrentSpatial().getNodes()) {
					this.drawNode(currentRender, n);
					this.updateAudio(n);
				}
				
				currentRender.endRender();
				currentWorld.debugRender(currentRender.getCamera().combined);
			}
			this.stepPhysicsSimulation(currentWorld);

			lastDrawedSpatial = spatialToDraw;
		}
	}

	public GameAdapter() {
		
	}
	
	protected void updateNodesPosition(Box2dPhysicsAdapter currentWorld){
		for (Node n : game.getCurrentSpatial().getNodes()) {
			currentWorld.setElementBodyPosition(n.getName());
		}
	}
	
	protected void stepPhysicsSimulation(Box2dPhysicsAdapter currentWorld){
		currentWorld.step();
	}
	
	protected void drawNode(SpatialRenderer currentRender, Node n){
		Position p = (Position) n.getCurrentLocation();
		
		// desenha na posicao atual
		currentRender.drawNode(n, p.getX(), p.getY());
	}
	
	protected void updateAudio(Node n){
		for(AudioNode audio : n.getAudioNodes()){
			AudioAdapter adapter = this.audioAdapters.get(audio);
			if(adapter != null) adapter.update();
		}
	}

}