package MEnDiGa.adapters.libgdx.graphics;

import java.util.Collection;
import java.util.HashMap;

import MEnDiGa.spatial.nodes.SpriteNode;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SpriteNodeRenderer implements GraphicNodeRenderer{
	private TextureRegion[] frames;
	private HashMap<String, Animation> animations;
	private Texture loadedImageFile;
	private SpriteNode graphicNode;
	
	public SpriteNodeRenderer(SpriteNode node){
		this.graphicNode = node;
		
		/* Carrega o arquivo de imagem com 
		 * as informacoes  do node
		 */
		loadedImageFile = new Texture(Gdx.files.internal(this.graphicNode.getResourcePath()));
		
		/* Cria uma regiao da textura representado a
		 * area da imagem que sera utilizada  
		 */
		if(node.getRegionWidth() == 0) node.setRegion(loadedImageFile.getWidth(), node.getRegionHeight());
		if(node.getRegionHeight() == 0) node.setRegion(node.getRegionWidth(), loadedImageFile.getHeight());
		
		TextureRegion spriteSheet = new TextureRegion(loadedImageFile, node.getRegionCoordineteX(), 
				node.getRegionCoordinateY(), node.getRegionWidth(), node.getRegionHeight());
		
		if(node.getFrameWidth() != 0 && node.getFrameHeight() != 0){
			/* O metodo split retorna uma matriz com frames com a
			 * largura e altura especificados. Agora, a altura e a
			 * largura dos frames � dadda pelo programador ao configurar
			 * o node Eu preciso transformar essa matrix num array 
			 * simples para que que eu possa acessar os frames da 
			 * forma definida.
			 */
			frames = matrixToArray(spriteSheet.split(node.getFrameWidth(), node.getFrameHeight()));
		}
		else{
			frames = new TextureRegion[]{spriteSheet};
		}
		
		animations = new HashMap<String, Animation>();
		
		HashMap<String, int[]> definedAnimations = node.getAnimations();
		Collection<String> animationKeys = definedAnimations.keySet();
		
		for(String key : animationKeys){
			int[] framesIndex = definedAnimations.get(key);
			
			/* Esse array recebera os frames definidos previamente
			 * ao definir as animacoes.
			 */
			TextureRegion[] animationFrames = new TextureRegion[framesIndex.length];
			
			/* Preenchendo o array com os frames. A cada iteracao
			 * do laco abaixo, o frame corespondente ao index e
			 * adcionado ao array. A variavel i garante que o frame
			 * sempre seja armazenado na proxima posicao do array e
			 * nao numa posiao ja ocupada.
			 */
			int i = 0;
			for(int index : framesIndex){
				animationFrames[i] = frames[index];
				i++;
			}
			
			/* Cria uma animacao como array de frames
			 * construido e usando as configuracoes de
			 * duracao de frame definidas. 
			 */
			float frameDuration = node.getFrameDuration(key);
			PlayMode playmode;
			switch(graphicNode.getPlayMode(key)){
			case NORMAL: playmode = PlayMode.NORMAL; break;
			case REVERSED: playmode = PlayMode.REVERSED; break;
			case LOOP_NORMAL: playmode = PlayMode.LOOP; break;
			case LOOP_REVERSED: playmode = PlayMode.LOOP_REVERSED; break;
			case LOOP_UP_DOWN: playmode = PlayMode.LOOP_PINGPONG; break;
			default: playmode = PlayMode.NORMAL; break;
			}
			Animation animation = new Animation (frameDuration, animationFrames);
			animation.setPlayMode(playmode);
			animations.put(key, animation);
		}
	}
	
	public void draw(SpriteBatch batch, float x, float y){
		TextureRegion currentFrame;
		
		// Se nao existe animacao definida...
		if(graphicNode.getCurrentAnimation() == null){
			// ... o frame atual foi definido pelo usuario.
			currentFrame = frames[graphicNode.getCurrentFrame()];
		}
		else{
			// Caso contrario, o frame atual esta contido na animacao atual.
			float stateTime = graphicNode.getAnimationStateTime();
			currentFrame = animations.get(graphicNode.getCurrentAnimation()).getKeyFrame(stateTime);
			graphicNode.addAnimationStateTime(Gdx.graphics.getDeltaTime());
		}
		
		// Verificando se o frame deve ser invertido de alguma forma
		currentFrame.flip(graphicNode.isXFliped() ^ currentFrame.isFlipX(), graphicNode.isYFliped() ^ currentFrame.isFlipY());
				
		
		// Cria um sprite com o frame atual
		Sprite sprite = new Sprite(currentFrame);
		
		// Seta a posicao do eixo de rotacao do sprite
		/* Nota: Tive que setar pra 0 para que o sprite pudesse
		 * ser escalado a partir do ponto inferior esquerdo.
		 * O problema com os sprites sendo desenhados fora da
		 * tela estava aqui, n�o na camera. 
		 */
		// TODO: rever, no futuro.
		sprite.setOrigin(graphicNode.getOriginX(), graphicNode.getOriginY());
		
		// Seta os fatores de escala
		// TODO: No futuro, levar em consideracao a escala individual de cada frame
		sprite.setScale(graphicNode.getScaleX(), graphicNode.getScaleY());
		
		// Seta a posi��o e aplica o deslocamento
		sprite.setPosition(x + graphicNode.getOffsetX(), y + graphicNode.getOffsetY());
		
		// Aplica a rotacao e desenha o sprite
		sprite.setRotation(graphicNode.getRotation());
		sprite.draw(batch);
	}
	
	protected TextureRegion[] matrixToArray(TextureRegion[][] matrix){
		int frameRows = graphicNode.getRegionHeight()/graphicNode.getFrameHeight();
		int frameColums = graphicNode.getRegionWidth()/graphicNode.getFrameWidth();
		
		//Cria um array pra comportar os elementos da matriz
		TextureRegion[] array = new TextureRegion[frameRows * frameColums];
		int elementsCopied = 0;
		for(int i = 0; i < frameRows; i++){
			/* Copia os elementos de cada linha da matriz para o
			 * novo array. Os parametros do metodo arraycopy sao:
			 * o array de origem (neste caso as linhas da matriz);
			 * o indice do primeiro elemento a ser copiado do array
			 * de origem; o array de destino, a posicao do array de
			 * destino na qual o primeiro elemento da origem seria 
			 * inserido e o numero de elementos a serem copiados.
			 * A variavel elementsCopied funciona como a variavel i
			 * utilizada na criacao das animacoes.
			 */
			System.arraycopy(matrix[i], 0, array, elementsCopied, frameColums);
			elementsCopied += frameColums;
		}
		return array;
	}
}
