package MEnDiGa.adapters.libgdx.graphics;

import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.GraphicNode;
import MEnDiGa.spatial.nodes.TextNode;

public class RendererFactory {
	
	public static GraphicNodeRenderer createGraphicAdapter(GraphicNode node){
		GraphicNodeRenderer adapter = null;
		if(node instanceof SpriteNode){
			adapter = new SpriteNodeRenderer((SpriteNode) node);
		} else if(node instanceof TextNode){
			adapter = (GraphicNodeRenderer) new TextNodeRenderer((TextNode) node);
		}
		return adapter;
	}

}
