package MEnDiGa.adapters.libgdx.graphics;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public interface GraphicNodeRenderer {
	public void draw(SpriteBatch batch, float x, float y);
}
