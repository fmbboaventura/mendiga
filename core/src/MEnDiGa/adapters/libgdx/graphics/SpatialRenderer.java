package MEnDiGa.adapters.libgdx.graphics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import MEnDiGa.spatial.Environment;
import MEnDiGa.spatial.Location;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.locations.Map;
import MEnDiGa.spatial.nodes.Camera;
import MEnDiGa.spatial.nodes.GraphicNode;
import MEnDiGa.spatial.nodes.Node;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

public class SpatialRenderer {
	private Spatial spatial;
	private TiledMap currentMap;
	private OrthogonalTiledMapRenderer mapRenderer;
	private OrthographicCamera camera;
	private SpriteBatch batch;
	private HashMap<Node, Collection<GraphicNodeRenderer>> graphicNodes;
	private Camera cameraNode;
	
	private SpatialRenderer(Spatial spatial){
		this.spatial = spatial;
		this.graphicNodes = new HashMap<>();
		this.batch = new SpriteBatch();
		this.camera = new OrthographicCamera();
	}
	
	public SpatialRenderer(Spatial spatial, float viewportWidth, float viewportHeight){
		this(spatial);
		this.camera.setToOrtho(false, viewportWidth, viewportHeight);// 640, 480);//160, 120);
		this.camera.update();
		
		this.cameraNode = new Camera("camera", viewportWidth, viewportHeight);
	}
	
	public SpatialRenderer(Spatial spatial, Camera camera){
		this(spatial);
		
		this.cameraNode = camera;
		this.camera.setToOrtho(false, cameraNode.getViewportWidth(), 
				cameraNode.getViewportHeight());
		this.camera.update();
	}
	
	public OrthographicCamera getCamera(){
		return this.camera;
	}
	
//	public void draw(){
//		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
//		
//		// Renderiza o mapa
//		if (this.mapRenderer != null)
//			this.mapRenderer.render();
//		
//		TreeMap<Float, Node> zMap = new TreeMap<Float, Node>();
//		for (Node n : this.currentEnviroment.getNodes()){
//			Position position = (Position) n.getCurrentLocation();
//			zMap.put(position.getZ(), n);
//		}
//		
//		// Renderiza os nodos graficos
//		this.batch.begin();
//		
//		for (Float i : zMap.keySet()){
//			Node n = zMap.get(i);
//			float[] position = physicsAdapter.getElementPosition(n.getId());
//			this.graphicNodes.get(n).draw(this.batch, position[0], position[1]);//position.getX(), position.getY());
//		}
//				
//		this.batch.end();
//		physicsAdapter.debugRender(camera.combined);//renderAndStep(camera.combined);
//		physicsAdapter.step();
//	}
	
	public void createGraphicAdapter(Node n){
		if(n != null){
			Collection<GraphicNode> graphicNodes = n.getGraphicNodes();
			
			if (!graphicNodes.isEmpty()) {
				Collection<GraphicNodeRenderer> adapters = new ArrayList<>(
						graphicNodes.size());

				for (GraphicNode gNode : graphicNodes) {
					adapters.add(RendererFactory.createGraphicAdapter(gNode));
				}
				this.graphicNodes.put(n, adapters);
			}
		}
	}
	
	public TiledMap createTmxTileMap(Map m){
		if(m != null)
		this.currentMap = new TmxMapLoader().load(((Map) m).getMapFile());
		this.mapRenderer = new OrthogonalTiledMapRenderer(this.currentMap, Map.worldUnitPerPixels, this.batch);
		this.mapRenderer.setView(this.camera);
		return this.currentMap;
	}
	
	public TiledMap createTmxTileMap() {
		if (this.spatial instanceof Environment) {
			Environment e = (Environment) this.spatial;
			Iterator<Location> locations = e.getLocations().iterator();
			while (locations.hasNext()) {
				Location temp = locations.next();
				if (temp instanceof Map) {
					return this.createTmxTileMap((Map) temp);
				}
			}
		}
		return null;
	}
	
	public void drawTmxTileMap(){
		if (this.mapRenderer != null)
			this.mapRenderer.render();
	}
	
//	public void depthSortNodes(){
//		TreeMap<Float, Node> zMap = new TreeMap<Float, Node>();
//		for (Node n : this.currentEnviroment.getNodes()){
//			Position position = (Position) n.getCurrentLocation();
//			zMap.put(position.getZ(), n);
//		}
//	}
	
	public void drawNode(Node n, float x, float y){
		Collection<GraphicNodeRenderer> adapters = this.graphicNodes.get(n); 
		if(adapters != null){
			for(GraphicNodeRenderer a : adapters)
				a.draw(this.batch, x, y);
		} else this.createGraphicAdapter(n);
	}
	
	public void beginRender(){
		this.camera.update();
		this.batch.setProjectionMatrix(camera.combined);
		this.batch.begin();
	}
	
	public void beginRender(boolean updateCamera){
		if(updateCamera){
			this.camera.setToOrtho(false, this.cameraNode.getViewportWidth(), 
					this.cameraNode.getViewportHeight());
			this.camera.position.set(this.cameraNode.getX(), this.cameraNode.getY(), 0);
		}
		this.beginRender();
	}
	
	public void endRender(){
		this.batch.end();
	}

	public SpriteBatch getBatch() {
		return this.batch;
	}
}
