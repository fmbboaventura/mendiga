package MEnDiGa.adapters.libgdx.graphics;

import MEnDiGa.spatial.nodes.TextNode;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.BitmapFont.HAlignment;

public class TextNodeRenderer implements GraphicNodeRenderer {
	private BitmapFont font;
	private TextNode textNode;
	float n;

	public TextNodeRenderer(TextNode textNode) {
		this.textNode = textNode;
		if (textNode.getFontFile() != null) {
			TextureRegion region = null;
			if (textNode.getFontTexture() != null) {
				Texture texture = new Texture(Gdx.files.internal(textNode
						.getFontTexture()));
				region = new TextureRegion(texture);
			}
			font = new BitmapFont(Gdx.files.internal(textNode.getFontFile()),
					region);
		} else {
			font = new BitmapFont();
		}
	}

	public void draw(SpriteBatch batch, float x, float y) {
		// font.setColor(Color.WHITE);

		font.setScale(textNode.getScaleX(), textNode.getScaleY());
		font.drawMultiLine(batch, textNode.getText(), x, y,
				textNode.getAlignmentWidth(),
				HAlignment.valueOf(textNode.getAlignment().toString()));
	}
}
