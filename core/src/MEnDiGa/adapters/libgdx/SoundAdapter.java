package MEnDiGa.adapters.libgdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import MEnDiGa.spatial.nodes.AudioNode;
import MEnDiGa.spatial.nodes.AudioNode.State;

public class SoundAdapter extends AudioAdapter{
	Sound sound;
	public SoundAdapter(AudioNode node){
		super(node);
		sound = Gdx.audio.newSound(Gdx.files.internal(node.getFilePath()));
	}

	@Override
	public void update() {
		switch (node.getState()) {
		case PAUSED:
			sound.pause();
			break;
		case PLAYING:
			if (node.isLooping())
				sound.loop(node.getVolume());
			else {
				sound.play(node.getVolume());
				node.setState(State.STOPED);
				return;
			}
			break;
		case STOPED:
			//sound.stop();
			break;
		}
	}
	
	@Override
	public void pause(){
		sound.pause();
	}
	
	@Override
	public void stop(){
		sound.stop();
	}
}
