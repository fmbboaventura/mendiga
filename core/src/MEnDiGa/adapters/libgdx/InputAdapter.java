package MEnDiGa.adapters.libgdx;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.nodes.Player;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class InputAdapter implements InputProcessor {
	
	protected HashMap<Integer, String> adaptedControlMap = new HashMap<Integer,String>();
	protected HashMap<String, Boolean> commandState = new HashMap<String, Boolean>();
	protected ArrayList<Observer> observersToPerform = new ArrayList<Observer>();
	private OrthographicCamera camera;
	
	public InputAdapter() {
		this.adaptedControlMap.put(Keys.LEFT, Player.MOVE_LEFT);
		this.adaptedControlMap.put(Keys.RIGHT, Player.MOVE_RIGHT);
		this.adaptedControlMap.put(Keys.UP, Player.MOVE_UP);
		this.adaptedControlMap.put(Keys.DOWN, Player.MOVE_DOWN);
		this.adaptedControlMap.put(Keys.SPACE, Player.BUTTON1_PRESS);
	}
	
	public InputAdapter(OrthographicCamera camera) {
		this();
		this.camera = camera;
	}

	public void addObserver(Observer observer){
		if(observer == null) return;
		this.observersToPerform.add(observer);
	}
	
	public Observer removeObserver(Observer observer){
		Observer result = null;
		
		if (observer != null){
			Iterator<Observer> iter = this.observersToPerform.iterator();
			while (iter.hasNext() && result == null){
				Observer temp = iter.next();
				if (observer.equals(temp)){
					result = temp;
					this.observersToPerform.remove(temp);
				}
			}
		}
		
		return result;
	}
		
	public void fire(Object... params){
		Iterator<Observer> iter = this.observersToPerform.iterator();
		while (iter.hasNext()){
			iter.next().fire(params);
		}	
	}
	
	public void fireKeyDown(int keycode){
		if(adaptedControlMap.containsKey(keycode)){
			this.commandState.put(adaptedControlMap.get(keycode), true);
			this.fire(adaptedControlMap.get(keycode));
		}
	}
	
	public void fireKeyUp(int keycode){
		if(adaptedControlMap.containsKey(keycode)){
			this.commandState.put(adaptedControlMap.get(keycode), false);
			
			for(String key : commandState.keySet()){
				if(commandState.get(key)) return;
			}
			this.fire(Player.NONE_MOVE);
		}
	}
	
	public void addCommandMapping(String command, int keyCode){
		this.adaptedControlMap.put(keyCode, command);
	}
	
	public void update(){
		for(String key : commandState.keySet()){
			if(commandState.get(key)) this.fire(key);
		}
	}

	@Override
	public boolean keyDown(int keycode) {
		this.fireKeyDown(keycode);
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		this.fireKeyUp(keycode);
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public boolean touchDown(int arg0, int arg1, int arg2, int arg3) {
		return this.touchEvent(Player.TOUCH_DOWN);
	}

	@Override
	public boolean touchDragged(int arg0, int arg1, int arg2) {
		return this.touchEvent(Player.TOUCH_DRAG);
	}

	@Override
	public boolean touchUp(int arg0, int arg1, int arg2, int arg3) {
		return this.touchEvent(Player.TOUCH_UP);
	}
	
	private boolean touchEvent(String eventType){
		Vector3 touchPos = new Vector3();
        touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
        camera.unproject(touchPos);
        
        this.fire(eventType, touchPos.x, touchPos.y);
		
		return true;
	}

}
