package MEnDiGa.adapters.libgdx;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;

import MEnDiGa.spatial.nodes.AudioNode;

public class MusicAdapter extends AudioAdapter{

	protected Music music;
	
	public MusicAdapter(AudioNode node) {
		super(node);
		music = Gdx.audio.newMusic(Gdx.files.internal(node.getFilePath()));
		music.setVolume(this.node.getVolume());
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update() {
		music.setLooping(node.isLooping());
		switch(node.getState()){
		case PAUSED:
			if(music.isPlaying()) music.pause();
			break;
		case PLAYING:
			if(!music.isPlaying()) music.play();
			break;
		case STOPED:
			music.stop();
			break;
		default:
			break;
		
		}
	}

}
