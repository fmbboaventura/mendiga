package MEnDiGa.behaviors;

import java.util.Comparator;

import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.nodes.Node;

public class SortNodes extends Behavior {
	
	private Comparator<Node> comparator;

	public SortNodes(Comparator<Node> comparator) {
		super();
		this.comparator = comparator;
	}

	@Override
	public void execute(Object... params) {
		if(params != null){
			Spatial s = (Spatial) params[0];
			s.sortNodes(comparator);
		}
	}

}
