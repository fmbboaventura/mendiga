package MEnDiGa.behaviors;

import java.util.Comparator;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Node;

public class DepthComparator implements Comparator<Node> {

	@Override
	public int compare(Node o1, Node o2) {
		Position p1 = (Position) o1.getCurrentLocation();
		Position p2 = (Position) o2.getCurrentLocation();
		
		if(p1 == null && p2 == null) return 0;
		if(p1 == null && p2 != null) return 1;
		if(p1 != null && p2 == null) return -1;
		if(p1.getZ() > p2.getZ()) return 1;
		if(p1.getZ() < p2.getZ()) return -1;
		return 0;
	}

}
