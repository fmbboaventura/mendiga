package MEnDiGa.behaviors;

public abstract class Behavior {
	//protected Object[] params;
	public abstract void execute(Object... params);
}
