package MEnDiGa;

import java.util.Collection;
import java.util.HashMap;

import MEnDiGa.behaviors.Behavior;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;

public abstract class GameSession {
	public HashMap<String, Spatial> spatials = new HashMap<String, Spatial>();
	public HashMap<String, Behavior> behaviors = new HashMap<String, Behavior>();
	public HashMap<String, Observer> observers = new HashMap<String, Observer>();
	// memoryData, configData
	
	private Spatial currentSpatial;
	private Spatial previousSpatial;
	
	// Spatial methods
	public void addSpatial(String key, Spatial spatial){
		spatials.put(key, spatial);
		if(spatials.size() == 1) this.setCurrentSpatial(spatial);
	}
	
	public void removeSpatial(String key){
		spatials.remove(key);
	}
	
	public Spatial getSpatial(String key){
		return spatials.get(key);
	}
	
	
	public Spatial getCurrentSpatial() {
		return currentSpatial;
	}

	public void setCurrentSpatial(Spatial currentSpatial) {
		this.previousSpatial = this.currentSpatial;
		this.currentSpatial = currentSpatial;
		if(!spatials.containsValue(currentSpatial)) 
			this.addSpatial(currentSpatial.getId(), currentSpatial);
	}
	
	public Spatial getPreviousSpatial(){
		return previousSpatial;
	}
	
	public Collection<Spatial> getSpatials(){
		return this.spatials.values();
	}

	// Observer methods	
	public void addObserver(String key, Observer observer){
		observers.put(key, observer);
	}
	
	public void addObserver(Observer observer){
		this.addObserver(observer.getId(), observer);
	}
	
	public void removeObserver(String key){
		observers.remove(key);
	}
	
	public Observer getObserver(String key){
		return observers.get(key);
	}
	
	public Collection<Observer> getObservers(){
		return this.observers.values();
	}
	
	
	// Behavior methods
	public void addBehavior(String key, Behavior behavior){
		behaviors.put(key, behavior);
	}
	
	public void removeBehavior(String key){
		behaviors.remove(key);
	}
	
	public Behavior getBehavior(String key){
		return behaviors.get(key);
	}
	
	//public abstract void initialize(Object... params);
	
	//public abstract void update(Object... params);
	
	//public abstract void endGame(Object... params);
}
