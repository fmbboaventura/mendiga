package MEnDiGa.spatial.nodes;

public class GraphicNode {

	protected String id;
	private float[] frameOffset;
	float scaleX;
	float scaleY;

	public GraphicNode() {
		super();
		scaleX = 1;
		scaleY = 1;
	}

	public String getId() {
		return id;
	}

	public float getScaleX() {
		return scaleX;
	}

	public float getScaleY() {
		return scaleY;
	}

	public void setScale(float scaleX, float scaleY) {
		this.scaleX = scaleX;
		this.scaleY = scaleY;
	}

	public void setScale(float scale) {
		this.scaleX = scale;
		this.scaleY = scale;
	}

	/**
	 * <p>
	 * Define offset values to be added to the drawing coordinates.
	 * </p>
	 * 
	 * @param offsetX - value to be added to the x-coordinate of the sprite
	 * @param offsetY - value to be added to the y-coordinate of the sprite
	 */
	public void setOffset(float offsetX, float offsetY) {
		frameOffset = new float[]{offsetX, offsetY};
	}

	public float getOffsetX() {
		float x = 0;
		if(frameOffset!=null) x = frameOffset[0];
		return x;
	}

	public float getOffsetY() {
		float y = 0;
		if(frameOffset!=null) y = frameOffset[1];
		return y;
	}

}