package MEnDiGa.spatial.nodes;

import java.util.HashMap;

/** <p>
 * A GraphicNode represents the graphical information of a {@link Node}.
 * It holds informations about a sprite sheet, such as the source of the
 * image file and the size of its frames (sprites). These informations are
 * used by {@link SpriteNodeRenderer} instances in order to load, prepare and
 * render the graphic resources described in the GraphicNode on its target
 * engine.
 * </p>
 * 
 * @author Filipe Boaventura
 *
 */
public class SpriteNode extends GraphicNode {
	
	public static enum PlayMode{
		NORMAL,
		REVERSED,
		LOOP_NORMAL,
		LOOP_REVERSED,
		LOOP_UP_DOWN;
	}
	
	private int frameRows, frameColumns, regionWidth, regionHeight;
	
	private int currentFrame;
	
	private String resourcePath;
	
	int frameWidth;

	private int frameHeight;
	
	// Indicam se o frame atual deve ser invertido
	private boolean flipX, flipY;
	
	/* As informaoes sobre as animaoees ficam
	 * armazenadas nestes hash maps*/
	private HashMap<String, int[]> animations = new HashMap<String, int[]>();
	
	private HashMap<String, Float> frameDuration = new HashMap<String, Float>();
	
	private HashMap<String, PlayMode> animationPlaymode = new HashMap<String, PlayMode>();
	
	private float originX = 0, originY = 0;
	
	// Chave da animacao atual
	private String currentAnimation;
	private float rotation;

	private int x;

	private int y;

	private HashMap<String, Float> stateTimeMap = new HashMap<String, Float>();

	public SpriteNode(String id, String resourcePath){
		this.id = id;
		this.resourcePath = resourcePath;
		scaleX = 1;
		scaleY = 1;
		currentFrame = 0;
	}
	
	/**<p>
	 * Constructs a new GraphicNode containing the source 
	 * of a sprite sheet with fixed frame sizes.
	 * </p>
	 * @param resourcePath - The source of a image file
	 * @param frameWidth - The width of single frame within the sheet
	 * @param frameHeight - The height of single frame within the sheet
	 */
	public SpriteNode(String id, String resourcePath, int frameWidth, int frameHeight){
		this(id, resourcePath);
		this.frameWidth = frameWidth;
		this.frameHeight = frameHeight;
	}
	
	/**<p>
	 * Constructs a new GraphicNode containing the source 
	 * of a sprite sheet, its widht, height and the number
	 * of rows and collums its frames are arranged.
	 * <p/>
	 * 
	 * @param resourcePath - The source of a image file
	 * @param resourceWidht - The widht of the desired area of the specified image file
	 * @param resourceHeight - The height of the desired area of the specified image file
	 * @param rows
	 * @param colums
	 * @deprecated
	 */
	public SpriteNode(String resourcePath, int resourceWidht, int resourceHeight, int rows, int colums) {
		this.resourcePath = resourcePath;
		this.regionWidth = resourceWidht;
		this.regionHeight = resourceHeight;
		this.frameRows = rows;
		this.frameColumns = colums;
		this.frameWidth = this.regionWidth/this.frameColumns;
		this.frameHeight = this.regionHeight/this.frameRows;
		this.rotation = 0;
	}

	/**<p>
	 * Construct a new GraphicNode and resizes its frames
	 * to match the widht and height especified in the parameters.
	 * Note that the libGDX {@link SpriteNodeRenderer} use those values
	 * for drawing and the real frame size is always the same.
	 * <p/>
	 * 
	 * @param frameWidth
	 * @param frameHeight
	 * @deprecated
	 */
	public SpriteNode(String resourcePath, int resourceWidht, int resourceHeight, int rows, int colums, int frameWidth, int frameHeight) {
		this(resourcePath, resourceWidht, resourceHeight, rows, colums);
		this.frameWidth = frameWidth;
		this.frameHeight = frameHeight;
	}
	
	/**<p>
	 * Set the current frame to rotate to the given angle, before drawing.
	 * <\p>
	 * @param angle
	 */
	public void setRotation(float angle) {
		this.rotation = angle;
	}

	public boolean isXFliped() {
		return flipX;
	}

	public boolean isYFliped() {
		return flipY;
	}
	
	public void flip(boolean flipX, boolean flipY){
		this.flipX = flipX;
		this.flipY = flipY;
	}
		
	public String getResourcePath() {
		return resourcePath;
	}
	
	public void setRegion(int regionWidth, int regionHeight) {
		this.regionWidth = regionWidth;
		this.regionHeight = regionHeight;
	}
	
	public void setRegion(int x, int y, int regionWidth, int regionHeight){
		//texture coordinates
		this.x = x;
		this.y = y;
		this.regionWidth = regionWidth;
		this.regionHeight = regionHeight;
	}
	
	public int getRegionCoordineteX(){
		return x;
	}
	
	public int getRegionCoordinateY(){
		return y;
	}

	@Deprecated
	public int getFrameRows() {
		return frameRows;
	}

	@Deprecated
	public int getFrameColums() {
		return frameColumns;
	}

	public int getRegionWidth() {
		return regionWidth;
	}

	public int getRegionHeight() {
		return regionHeight;
	}

	public float getFrameDuration(String animationName) {
		return frameDuration.get(animationName).floatValue();
	}
	
	public PlayMode getPlayMode(String animationName) {
		return animationPlaymode.get(animationName);
	}

	public int getCurrentFrame() {
		return currentFrame;
	}
	
	public void setCurrentFrame(int frameIndex){
		currentFrame = frameIndex;
		setCurrentAnimation(null);
	}
	
	
	public String getCurrentAnimation(){
		return currentAnimation;
	}
	
	public void setCurrentAnimation(String animationName){
		currentAnimation = animationName;
	}
	
	public void defineAnimation(String name, int[] frames, float frameDelay, PlayMode playmode){
		animations.put(name, frames);
		frameDuration.put(name, frameDelay);
		animationPlaymode.put(name, playmode);
		stateTimeMap.put(name, 0f);
	}

	public HashMap<String, int[]> getAnimations() {
		return animations;
	}
	
	public float getAnimationStateTime(String animationName){
		return this.stateTimeMap.get(animationName).floatValue();
	}
	
	public void setAnimationStateTime(String animationName, float stateTime){
		this.stateTimeMap.put(animationName, stateTime);
	}
	
	public void addAnimationStateTime(String animationName, float deltaTime){
		float stateTime = this.stateTimeMap.get(animationName).floatValue() + deltaTime;
		this.stateTimeMap.put(animationName, stateTime);
	}
	
	public float getAnimationStateTime(){
		return this.getAnimationStateTime(this.getCurrentAnimation());
	}
	
	public void setAnimationStateTime(float stateTime){
		this.setAnimationStateTime(this.getCurrentAnimation(), stateTime);
	}
	
	public void addAnimationStateTime(float deltaTime){
		this.addAnimationStateTime(this.getCurrentAnimation(), deltaTime);
	}
	
	public int getFrameWidth() {
		return frameWidth;
	}

	public int getFrameHeight() {
		return frameHeight;
	}

	public float getRotation() {
		return rotation;
	}
	
	public void setOffset(int frameIndex, float offsetX, float offsetY){
		throw new UnsupportedOperationException("not yet implemented");
	}
	
	public float getOffsetX(int frameIndex){
		throw new UnsupportedOperationException("not yet implemented");
	}
	
	public float getOffsetY(int frameIndex){
		throw new UnsupportedOperationException("not yet implemented");
	}

	public float getOriginY() {
		return originY;
	}

	public void setOriginY(float originY) {
		this.originY = originY;
	}

	public float getOriginX() {
		return originX;
	}

	public void setOriginX(float originX) {
		this.originX = originX;
	}

}
