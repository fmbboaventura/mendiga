package MEnDiGa.spatial.nodes;

import java.util.HashMap;

public class Element extends Node {

	private String type;
	HashMap<String, Object> properties = new HashMap<String, Object>();
	HashMap<String, Property> propertyMap = new HashMap<String, Property>();;
	
	public Element(String id) {
		super(id);
	}	
	
	public Element(String id, String type) {
		super(id);
		this.type = type;
	}	
	
	public Element(String id, String groupId, Node parentNode, String type) {
		super(id, groupId, parentNode);
		this.type = type;
	}

	@Deprecated
	public void setProperty(String key, Object value){
		properties.put(key, value);
	}
	
	public void addProperty(Property p){
		this.propertyMap.put(p.getClass().getSimpleName(), p);
	}
	
	@SuppressWarnings("unchecked")
	public <T extends Property> T getProperty(Class<T> propertyClass){
		return (T) this.propertyMap.get(propertyClass.getSimpleName());
	}
	
	public <T extends Property> boolean hasProperty(Class<T> propertyClass){
		return propertyMap.containsKey(propertyClass.getSimpleName());
	}
	
	@Deprecated
	public Object getProperty(String key){
		return properties.get(key);
	}

	public String getType() {
		return type;
	}
}
