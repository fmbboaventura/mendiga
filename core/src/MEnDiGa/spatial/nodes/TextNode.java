package MEnDiGa.spatial.nodes;

public class TextNode extends GraphicNode {
	protected String fontFile;
	protected String fontTexture;
	protected String text;
	
	public static enum Alignment {
		LEFT, CENTER, RIGHT
	}
	
	protected Alignment alignment;
	protected float alignmentWidth;
	
	public TextNode(String id){
		this.id = id;
		this.alignment = Alignment.LEFT;
	}

	public TextNode(String id, String text){
		this(id);
		this.text = text;
	}

	public TextNode(String id, String fontFile, String fontTexture) {
		this(id);
		this.fontFile = fontFile;
		this.fontTexture = fontTexture;
	}

	public float getAlignmentWidth() {
		return alignmentWidth;
	}

	public void setAlignmentWidth(float alignmentWidth) {
		this.alignmentWidth = alignmentWidth;
	}

	public Alignment getAlignment() {
		return alignment;
	}

	public void setAlignment(Alignment alignment) {
		this.alignment = alignment;
	}

	public String getFontFile() {
		return fontFile;
	}

	public String getFontTexture() {
		return fontTexture;
	}
	
	public String getText(){
		return text;
	}
	
	public void setText(String text){
		this.text = text;
	}
}
