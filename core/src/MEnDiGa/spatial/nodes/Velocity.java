package MEnDiGa.spatial.nodes;

public class Velocity extends Property {
	private float x;
	private float y;
	
	public Velocity(){
		this.x = 0;
		this.y = 0;
	}
	
	public Velocity(float x, float y){
		this.x = x;
		this.y = y;
	}

	public void zero() {
		x = 0;
		y = 0;
		this.changed = true;
	}
	
	public boolean isZero(){
		return x == 0 && y == 0;
	}
	
	public void set(float x, float y){
		this.x = x;
		this.y = y;
		this.changed = true;
	}

	/**
	 * @return the x
	 */
	public float getX() {
		this.changed = false;
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(float x) {
		this.changed = true;
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public float getY() {
		this.changed = false;
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(float y) {
		this.changed = true;
		this.y = y;
	}
}
