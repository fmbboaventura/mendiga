package MEnDiGa.spatial.nodes;

public abstract class Property {
	protected boolean changed = false;

	public boolean hasChanged() {
		return changed;
	}
	
	public void setChanged(boolean hasChanged) {
		this.changed = hasChanged;
	}
}
