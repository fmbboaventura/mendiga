package MEnDiGa.spatial.nodes;

/**
 * <p>
 * Contains information about the physical properties of a {@link Node}
 * such as its {@link BoundingVolume}, resulting force and torque to be applied
 * to it.
 * </p> 
 * @author Filipe Boaventura
 *
 */
public class PhysicsNode {
	
	
	/**
	 * <p>
	 * Defines the amount of gravity force that acts on the {@link Node}.
	 * Setting this to 0 means that the {@link Node} will ignore gravity. 
	 * </p>
	 */
	protected float gravityScale = 1;
	
	/**
	 * <p>
	 * Can this {@link Node} rotate? 
	 * </p>
	 */
	protected boolean rotationAllowed = true;
	
	protected boolean isSolid = true;
	
	protected float density;
	
	protected float restituition;
	
	protected float friction = 0.2f;

	protected float mass;

	private float angularDamping;

	private float linearDamping;

	/**
	 * @return the rotationAllowed
	 */
	public boolean isRotationAllowed() {
		return rotationAllowed;
	}

	/**
	 * @param rotationAllowed the rotationAllowed to set
	 */
	public void setRotationAllowed(boolean rotationAllowed) {
		this.rotationAllowed = rotationAllowed;
	}

	/**
	 * @return the density
	 */
	public float getDensity() {
		return density;
	}

	/**
	 * @param density the density to set
	 */
	public void setDensity(float density) {
		this.density = density;
	}

	/**
	 * @return the restituition
	 */
	public float getRestituition() {
		return restituition;
	}

	/**
	 * @param restituition the restituition to set
	 */
	public void setRestituition(float restituition) {
		this.restituition = restituition;
	}

	/**
	 * @return the friction
	 */
	public float getFriction() {
		return friction;
	}

	/**
	 * @param friction the friction to set
	 */
	public void setFriction(float friction) {
		this.friction = friction;
	}

	/**
	 * @return the mass
	 */
	public float getMass() {
		return mass;
	}

	public PhysicsNode(){
		
	}

	/**
	 * @return the gravityScale
	 */
	public float getGravityScale() {
		return gravityScale;
	}

	/**
	 * @param gravityScale the gravityScale to set
	 */
	public void setGravityScale(float gravityScale) {
		this.gravityScale = gravityScale;
	}

	/**
	 * @return the isSolid
	 */
	public boolean isSolid() {
		return isSolid;
	}

	/**
	 * @param isSolid the isSolid to set
	 */
	public void setSolid(boolean isSolid) {
		this.isSolid = isSolid;
	}

	public float getAngularDamping() {
		// TODO Auto-generated method stub
		return this.angularDamping;
	}
	
	public void setAngularDamping(float angularDamping){
		this.angularDamping = angularDamping;
	}
	
	public void setLinearDamping(float linearDamping){
		this.linearDamping = linearDamping;
	}
	
	public float getLinearDamping(){
		return this.linearDamping;
	}
}
