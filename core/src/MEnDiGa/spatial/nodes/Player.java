package MEnDiGa.spatial.nodes;

import java.util.HashMap;

import MEnDiGa.behaviors.Behavior;

public class Player extends Element {
	public static final String TOUCH_DOWN = "touchDown";
	public static final String TOUCH_UP = "touchUp";
	public static final String TOUCH_DRAG = "touchDrag";
	public static final String NONE_MOVE = "none";
	public static final String MOVE_LEFT = "left";
	public static final String MOVE_RIGHT = "right";
	public static final String MOVE_UP = "up";
	public static final String MOVE_DOWN = "down";
	public static final String BUTTON1_PRESS = "space";
	
	protected HashMap<String, Behavior> controlMap = new HashMap<String, Behavior>();
	protected String currentCommand;
	protected String previousCommand;
	
	public Player(String id) {
		super(id, "Player");
		this.setCurrentCommand(NONE_MOVE);
	}
	
	public Player(String id, String groupId, Node parentNode, String type) {
		super(id, groupId, parentNode, "Player");
	}
	

	public void setPlayerBehavior(String command, Behavior behavior){
		this.controlMap.put(command, behavior);
	}
	
	public Behavior getPlayerBehavior(String command){
		return this.controlMap.get(command);
	}
	
	public String getCurrentCommand() {
		return currentCommand;
	}

	public void setCurrentCommand(String currentCommand) {
		this.previousCommand = this.currentCommand;
		this.currentCommand = currentCommand;
	}
	
	public String getPreviousCommand(){
		return previousCommand;
	}
}
