package MEnDiGa.spatial.nodes;

public class Camera extends GraphicNode {
	
	private float viewportWidth;
	private float viewportHeight;
	private float x;
	private float y;
	
	public Camera(String id, float viewportWidth, float viewportHeight){
		this.id = id;
		this.setViewportHeight(viewportHeight);
		this.setViewportWidth(viewportWidth);
		this.x = this.viewportWidth/2;
		this.y = this.viewportHeight/2;
	}

	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getViewportWidth() {
		return viewportWidth;
	}

	public void setViewportWidth(float viewportWidth) {
		this.viewportWidth = viewportWidth;
	}

	public float getViewportHeight() {
		return viewportHeight;
	}

	public void setViewportHeight(float viewportHeight) {
		this.viewportHeight = viewportHeight;
	}

}
