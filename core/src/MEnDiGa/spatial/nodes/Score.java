package MEnDiGa.spatial.nodes;

public class Score extends Property {
	int value;
	
	public Score(){
		value = 0;
	}
	
	public void add(int value){
		this.value += value;
	}
	
	public int getValue(){
		return this.value;
	}

	public void clear() {
		this.value = 0;
	}
}
