/**
 * 
 */
package MEnDiGa.spatial.nodes;

/**
 * Contains information about the bounds of a {@link Node}.
 * 
 * @author Filipe Boaventura
 *
 */
public abstract class BoundingVolume {
}
