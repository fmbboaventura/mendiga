package MEnDiGa.spatial.nodes;

public class AudioNode {
	
	public static enum State{
		PLAYING,
		PAUSED,
		STOPED
	}
	//bufferData, state
	private String id;
	private boolean isLooping = false;
	private float volume;
	private String filePath;
	private State state;
	
	public AudioNode(String id, String filePath) {
		this.id = id;
		this.filePath = filePath;
		this.state = State.STOPED;
		this.volume = 1;
	}
	
	public String getId() {
		return id;
	}
	
	public boolean isLooping() {
		return isLooping;
	}
	
	public void setLooping(boolean isLooping) {
		this.isLooping = isLooping;
	}
	
	public String getFilePath() {
		return filePath;
	}
	
	public void setState(State state){
		this.state = state;
	}
	
	public State getState(){
		return state;
	}

	public float getVolume() {
		return volume;
	}

	public void setVolume(float volume) {
		this.volume = volume;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof AudioNode)) {
			return false;
		}
		AudioNode other = (AudioNode) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
}
