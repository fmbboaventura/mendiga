package MEnDiGa.spatial.nodes;

public class PolygonBound extends BoundingVolume{
	public float[] vertices;
	
	public PolygonBound(){
	}
	
	public PolygonBound(float[] vertices){
		this.vertices = vertices;
	}
}
