package MEnDiGa.spatial.nodes;

public class BoxBound extends BoundingVolume{
	public float width;
	public float height;

	public BoxBound() {
	}

	public BoxBound(float width, float height) {
		this.height = height;
		this.width = width;
	}
}