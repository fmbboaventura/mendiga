package MEnDiGa.spatial.nodes;

import java.util.Collection;
import java.util.HashMap;

import MEnDiGa.spatial.Location;


public abstract class Node {
	protected static int nextId = 0;
	protected int id;
	protected String name;
	protected String groupId;
	protected PhysicsNode physicsNode;
	protected Node parentNode; 
	protected Location previousLocation, currentLocation;
	protected HashMap<String, AudioNode> audioNode = new HashMap<>();
	protected HashMap<String, GraphicNode> graphicNode = new HashMap<>();
	protected BoundingVolume bounds;
		
	
	public Node(String name) {
		super();
		id = nextId;
		nextId++;
		this.name = name;
	}

	public Node(String id, String groupId, Node parentNode) {
		super();
		this.name = id;
		this.groupId = groupId;
		this.parentNode = parentNode;
	}

	public String getName(){
		return name;
	}
	
	public String getGroupId(){
		return groupId;
	}
	
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	public PhysicsNode getPhysicsNode() {
		return physicsNode;
	}

	public void setPhysicsNode(PhysicsNode physicsNode) {
		this.physicsNode = physicsNode;
	}

	public Node getParentNode() {
		return parentNode;
	}

	public void setParentNode(Node parentNode) {
		this.parentNode = parentNode;
	}
	
	public Location getCurrentLocation() {
		return currentLocation;
	}
	
	public void setCurrentLocation(Location currentLocation){
		this.previousLocation = this.currentLocation;
		this.currentLocation = currentLocation;
	}
	
	public Location getPreviousLocation() {
		return previousLocation;
	}

	
	public AudioNode getAudioNode(String id) {
		return audioNode.get(id);
	}

	public void addAudioNode(AudioNode audioNode) {
		this.audioNode.put(audioNode.getId(), audioNode);
	}
	
	public Collection<AudioNode> getAudioNodes(){
		return audioNode.values();
	}
	
	
	public GraphicNode getGraphicNode(String id){
		return this.graphicNode.get(id);
	}
	
	public void addGraphicNode(GraphicNode graphicNode) {
		this.graphicNode.put(graphicNode.getId(), graphicNode);
	}
	
	public Collection<GraphicNode> getGraphicNodes(){
		return this.graphicNode.values();
	}

	
	@Override
	public boolean equals(Object obj) {
		if(obj != null){
			if(obj instanceof Node){
				Node other = (Node) obj;
				return this.id == other.id;//this.name.equals(other.name);
			}
		}
		return false;
	}

	/**
	 * @return the bounds
	 */
	public BoundingVolume getBounds() {
		return bounds;
	}

	/**
	 * @param bounds the bounds to set
	 */
	public void setBounds(BoundingVolume bounds) {
		this.bounds = bounds;
	}
	
}
