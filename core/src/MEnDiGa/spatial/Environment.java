package MEnDiGa.spatial;

import java.util.*;

public class Environment extends Spatial {
	private HashMap<String, Location> locations = new HashMap<String, Location>();
	
	public Environment(String id) {
		super(id);
	}
	
	public void addLocation(String key, Location location){
		locations.put(key, location);
	}
	
	public Location getLocation(String key){
		return locations.get(key);
	}

	public Collection<Location> getLocations() {
		return locations.values();
	}
	
}
