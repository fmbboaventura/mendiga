package MEnDiGa.spatial.locations;

import MEnDiGa.spatial.Location;

public class Position extends Location {
	private float x;
	private float y;
	private float z;

	public Position(String id, String type, Location parentLocation, float x, float y, float z) {
		super(id, type, parentLocation);
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Position(String id, String type, float x, float y, float z) {
		super(id, type);
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public Position(String id, float x, float y, float z) {
		super(id);
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}	
	
	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	@Override
	/* Gerado pelo eclipse!!
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Position))
			return false;
		Position other = (Position) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		if (z != other.z)
			return false;
		return true;
	}

	public void set(float x, float y) {
		this.setX(x);
		this.setY(y);
	}
	
	public void set(float x, float y, float z) {
		this.set(x, y);
		this.setZ(z);
	}
}
