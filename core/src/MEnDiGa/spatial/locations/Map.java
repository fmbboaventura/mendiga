package MEnDiGa.spatial.locations;

import MEnDiGa.spatial.Location;

public class Map extends Location {
	
	public static float worldUnitPerPixels;
	
	private String mapFile;	

	public Map(String id, String type, Location parentLocation, String mapFile) {
		super(id, type, parentLocation);		
		this.mapFile = mapFile;
	}

	public Map(String id, String type, String mapFile) {
		super(id, type);
		this.mapFile = mapFile;
	}

	public Map(String id, String mapFile) {
		super(id);
		this.mapFile = mapFile;
	}
	
	public String getMapFile() {
		return this.mapFile;
	}

	public void setMapFile(String mapFile) {
		this.mapFile = mapFile;
	}
	
	public static float pixelsToWorld(float pixels){
		return Map.worldUnitPerPixels * pixels;
	}
	
	public static float worldToPixels(float meters){
		return Map.worldUnitPerPixels/meters;
	}
}
