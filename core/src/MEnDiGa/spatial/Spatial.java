package MEnDiGa.spatial;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import MEnDiGa.spatial.nodes.Node;


public class Spatial {
	protected String id;
	protected List<Node> nodes = new LinkedList<Node>();
	protected Queue<String> lastRemovedNodeId = new LinkedList<String>();
	
	public Spatial(String id){
		this.id = id; 
	}
	
	public String getId() {
		return id;
	}
	
	public void addNode(Node node){
		nodes.add(node);
	}
	
	public void addNodeGroup(String key, Collection<Node> nodes){
		//TODO: rever addNodeGroup
		if (nodes != null){
			Iterator<Node> iter = nodes.iterator();
			
			while (iter.hasNext()){
				this.nodes.add(iter.next());
			}
		}
	}
	
	public Node getNodeByName(String name){
		Iterator<Node> it = nodes.iterator();
		
		while(it.hasNext()) {
			Node n = it.next();
			if(n.getName().equals(name)) return n;
		}
		return null;
	}
	
	public Collection<Node> getNodes(){
		return nodes;
	}
	
	/**
	 * Returns a list containing the nodes of the given groupName.
	 * 
	 */
	public List<Node> getNodeGroup(String groupName){
		LinkedList<Node> nodeGroup = new LinkedList<Node>();
		for(Node n : nodes){
			if(n.getGroupId() != null &&
					n.getGroupId().equals(groupName))
				nodeGroup.add(n);
		}
		return nodeGroup;
	}
	
	public boolean removeNode(Node node){
		this.lastRemovedNodeId.add(node.getName());
		return nodes.remove(node);
	}
	
	public Queue<String> lastRemovedNodeId(){
		return lastRemovedNodeId;
	}

	public void sortNodes(Comparator<Node> comparator) {
		Collections.sort(nodes, comparator);
	}
	
	
}
