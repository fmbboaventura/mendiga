package MEnDiGa.spatial;

public abstract class Location {
	private String id;
	private String type;
	private Location parentLocation;
	
	public Location(String id){
		this.id = id;
	}
		
	public Location(String id, String type){
		this.id = id;
		this.type = type;
	}
	
	public Location(String id, String type, Location parentLocation){
		this.id = id;
		this.type = type;
		this.parentLocation = parentLocation;
	}

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public Location getParentLocation() {
		return parentLocation;
	}	
	
}
