package superjumper.behavior;

import superjumper.spatial.World;
import superjumper.spatial.node.Squirrel;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Velocity;

public class UpdateSquirel extends Behavior{

	@Override
	public void execute(Object... params) {
		if(params != null){
			Squirrel squirrel = (Squirrel) params[0];
			Float deltaTime = (Float) params[1];
			
			Velocity v = squirrel.getProperty(Velocity.class);
			Position p = (Position) squirrel.getCurrentLocation();
			
			p.setX(p.getX() + v.getX() * deltaTime);
			
			SpriteNode graphicNode = 
					(SpriteNode) squirrel.getGraphicNode(squirrel.getName());
			if(p.getX() < 0){
				p.setX(0);
				v.setX(-v.getX());
				graphicNode.flip(false, false);
			} else if(p.getX() > World.WORLD_WIDTH - Squirrel.SQUIRREL_WIDTH){
				p.setX(World.WORLD_WIDTH - Squirrel.SQUIRREL_WIDTH);
				v.setX(-v.getX());
				graphicNode.flip(true, false);
			}
		}
	}

}
