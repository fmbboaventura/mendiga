package superjumper.behavior;

import superjumper.SuperJumperGame;

public class PlayJumpSound extends PlayAudioNode {

	public PlayJumpSound(SuperJumperGame game) {
		super(game);
	}

	@Override
	public void execute(Object... params) {
		super.execute(this.game.getAudioNode(SuperJumperGame.JUMP_SOUND));
	}
}
