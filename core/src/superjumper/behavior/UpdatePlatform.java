package superjumper.behavior;

import java.util.List;

import superjumper.spatial.World;
import superjumper.spatial.node.Platform;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Node;
import MEnDiGa.spatial.nodes.Velocity;

public class UpdatePlatform extends Behavior {
	
	private List<Node> platforms;

	public UpdatePlatform(List<Node> platforms) {
		this.platforms = platforms;
	}

	@Override
	public void execute(Object... params) {
		if(params != null){
			Platform platform =  (Platform) params[0];
			Float deltaTime = (Float) params[1];
			
			if(platform.hasProperty(Velocity.class)){
				Velocity v = platform.getProperty(Velocity.class);
				
				Position p = (Position) platform.getCurrentLocation();
				p.setX(p.getX() + v.getX() * deltaTime);
				if(p.getX() < 0){
					p.setX(0);
					v.setX(-v.getX());
				} else if(p.getX() > World.WORLD_WIDTH - Platform.PLATFORM_WIDTH){
					p.setX(World.WORLD_WIDTH - Platform.PLATFORM_WIDTH);
					v.setX(-v.getX());
				}
			}
			platform.stateTime += deltaTime;
			
			if(platform.pulverizing && platform.stateTime > Platform.PLATFORM_PULVERIZE_TIME){
				// Remove a plataforma da lista do observer
				platforms.remove(platform);
				
				//Autoriza a remo��o da plataforma do spatial
				params[2] = true;
			}
			
		}
	}

}
