package superjumper.behavior;

import superjumper.SuperJumperGame;
import superjumper.SuperJumperGame.GameState;
import superjumper.observer.ObserverGroup;
import superjumper.spatial.HudNodes;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;

public class PauseGame extends Behavior {

	private GameSession game;
	
	public PauseGame(GameSession game) {
		super();
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		Spatial hud = this.game.getSpatial("hud");
		hud.addNode(HudNodes.RESUME);
		hud.addNode(HudNodes.QUIT);
		
		ObserverGroup root = (ObserverGroup) this.game.getObserver("group");
		Observer pauseGroup = this.game.getObserver("gamePausedGroup");
		
		root.clearObservers();
		root.addObserver(pauseGroup);
		((SuperJumperGame)game).setGameState(GameState.PAUSED);
	}

}
