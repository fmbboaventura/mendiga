package superjumper.behavior;

import superjumper.observer.ObserverGroup;
import superjumper.spatial.node.Bob;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Velocity;

public class ResolveBobSquirrelCollision extends Behavior {
	
	private GameSession game;

	public ResolveBobSquirrelCollision(GameSession game) {
		super();
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		Bob bob = Bob.getInstance();
		bob.getProperty(Velocity.class).zero();
		bob.hit = true;
		
		SpriteNode graphicNode = 
				(SpriteNode) bob.getGraphicNode(bob.getName());
		
		graphicNode.setCurrentFrame(Bob.FALL_FRAME);
		graphicNode.flip(false, true);
		
		// Limpa o grupo root para desabilitar o input
		ObserverGroup root = (ObserverGroup) this.game.getObserver("group");
		root.clearObservers();
	}

}
