package superjumper.behavior;

import superjumper.SuperJumperGame;

public class PlayHighJumpSound extends PlayAudioNode {

	public PlayHighJumpSound(SuperJumperGame game) {
		super(game);
	}

	@Override
	public void execute(Object... params) {
		super.execute(this.game.getAudioNode(SuperJumperGame.HI_JUMP_SOUND));
	}
}
