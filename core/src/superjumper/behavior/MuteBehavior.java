package superjumper.behavior;

import superjumper.SuperJumperGame;
import superjumper.spatial.node.MuteButton;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class MuteBehavior extends Behavior {
	
	Node muteButton;
	SuperJumperGame game;

	public MuteBehavior(SuperJumperGame game, Node muteButton) {
		super();
		this.muteButton = muteButton;
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		SpriteNode graphicNode = (SpriteNode) muteButton.getGraphicNode(muteButton.getName());
		switch(graphicNode.getCurrentFrame()){
		case MuteButton.MUTE: 
			graphicNode.setCurrentFrame(MuteButton.UNMUTE); 
			game.setSoundEnabled(true);
			break;
		case MuteButton.UNMUTE: 
			graphicNode.setCurrentFrame(MuteButton.MUTE); 
			game.setSoundEnabled(false);
			break;
		}
	}

}
