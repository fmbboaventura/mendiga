package superjumper.behavior;

import superjumper.spatial.node.MessageNode;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.Spatial;

public class ChangeMessage extends Behavior {
	private GameSession game;

	public ChangeMessage(GameSession game) {
		super();
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		Spatial end = this.game.getSpatial("end");
		MessageNode messageNode = (MessageNode) end.getNodeByName("msgNode");
		
		if(messageNode.getCurrentMessageIndex() == 6){
			this.game.getBehavior("changeToMenu").execute();
			messageNode.reset();
			return;
		}
		messageNode.nextMessage();
	}

}
