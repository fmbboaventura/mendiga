package superjumper.behavior;

import superjumper.SuperJumperGame;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.AudioNode;
import MEnDiGa.spatial.nodes.AudioNode.State;

public class PlayAudioNode extends Behavior {
	
	protected SuperJumperGame game;
	
	public PlayAudioNode(SuperJumperGame game){
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		if(params != null && this.game.isSoundEnabled()){
			AudioNode audio = (AudioNode) params[0];
			audio.setState(State.PLAYING);
		}
	}

}
