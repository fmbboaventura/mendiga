package superjumper.behavior;

import superjumper.SuperJumperGame;
import superjumper.SuperJumperGame.GameState;
import superjumper.observer.ObserverGroup;
import superjumper.spatial.HudNodes;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;

public class ResumeGame extends Behavior{

	private GameSession game;
	
	public ResumeGame(GameSession game) {
		super();
		this.game = game;
	}
	
	@Override
	public void execute(Object... params) {
		Spatial hud = this.game.getSpatial("hud");
		if (((SuperJumperGame) game).getGameState().equals(GameState.PAUSED)) {
			hud.removeNode(HudNodes.QUIT);
			hud.removeNode(HudNodes.RESUME);
		} else if (((SuperJumperGame) game).getGameState().equals(
				GameState.READY)) {
			hud.removeNode(HudNodes.READY_NODE);
		}
		
		ObserverGroup root = (ObserverGroup) this.game.getObserver("group");
		Observer runningGroup = this.game.getObserver("gameRunningGroup");
		
		root.clearObservers();
		root.addObserver(runningGroup);
		((SuperJumperGame)game).setGameState(GameState.RUNNING);
	}

}
