package superjumper.behavior;

import superjumper.SuperJumperGame;
import superjumper.observer.ClickNodeObserver;
import superjumper.observer.ObserverGroup;
import superjumper.spatial.node.BackgroundNode;
import superjumper.spatial.node.MainMenuNode;
import superjumper.spatial.node.MainMenuNode.MainMenuOption;
import superjumper.spatial.node.MuteButton;
import superjumper.spatial.node.TitleNode;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.nodes.Node;

public class ChangeToMainMenuScreen extends Behavior {

	private GameSession game;

	public ChangeToMainMenuScreen(GameSession game) {
		super();
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		// Tenta recuperar o menu
		Spatial menu = this.game.getSpatial("mainMenu");
		if (menu == null) {
			
			// cria o spatial, adciona o background e o t�tulo
			menu = new Spatial("mainMenu");
			menu.addNode(new BackgroundNode());
			menu.addNode(new TitleNode());

			// Observergroup que vai conter os clickNodeObservers do menu principal
			ObserverGroup mainMenuGroup = new ObserverGroup("mainMenuGroup");

			// Cria os nodes referentes as op��es do menu
			Node playMenuItem = new MainMenuNode(MainMenuOption.PLAY);
			menu.addNode(playMenuItem);

			Node hiScoresMenuItem = new MainMenuNode(MainMenuOption.HIGHSCORES);
			menu.addNode(hiScoresMenuItem);

			Node helpMenuItem = new MainMenuNode(MainMenuOption.HELP);
			menu.addNode(helpMenuItem);

			Node muteButton = new MuteButton();
			menu.addNode(muteButton);
			
			// Recupera o playClickSound do game
			Behavior playClickSound = this.game.getBehavior("playClickSound");
			
			/* Cria clickNodeObservers para as op��es do menu.
			 * Cada observer registra o playClickSound na sua
			 * lista de behaviors, assim como um ou mais beha-
			 * viors espec�ficos ao seu prop�sito.
			 */
			
			//Observer do bot�o play
			Observer clickedPlayObserver = new ClickNodeObserver(playMenuItem);
			// Behavior que prepara o jogo
			clickedPlayObserver.addBehavior(new ReadyGame(this.game));
			clickedPlayObserver.addBehavior(playClickSound);
			mainMenuGroup.addObserver(clickedPlayObserver);

			// Observer do bot�o highscores
			Observer clickedHighScoresObserver = new ClickNodeObserver(
					hiScoresMenuItem);
			clickedHighScoresObserver.addBehavior(playClickSound);
			// Behavior que prepara a tela dos highscores
			clickedHighScoresObserver.addBehavior(new ChangeToHighScoresScreen(this.game));
			mainMenuGroup.addObserver(clickedHighScoresObserver);

			// Observer do bot�o help
			Observer clickedHelpObserver = new ClickNodeObserver(helpMenuItem);
			// Behavior que prepara a tela de ajuda
			clickedHelpObserver.addBehavior(new ChangeToHelpScreen(this.game));
			clickedHelpObserver.addBehavior(playClickSound);
			mainMenuGroup.addObserver(clickedHelpObserver);

			//Observer do bot�o mute
			Observer clickedMuteObserver = new ClickNodeObserver(muteButton);
			// Behavior que chama o setSoundEnabled() do SuperJumperGame
			clickedMuteObserver.addBehavior(new MuteBehavior((SuperJumperGame) game, muteButton));
			mainMenuGroup.addObserver(clickedMuteObserver);
			
			// Registra no game o group do menu principal
			this.game.addObserver(mainMenuGroup);
		}
		
		// Pega o root group, que est� registrado no inputObserver
		ObserverGroup root = (ObserverGroup) this.game
				.getObserver("group");
		
		/* Remove os observers desse grupo para que observers que
		 * foram inseridos em outro contexto n�o sejam mais avaliados
		 */
		root.clearObservers();
		
		// Recupera o grupo criado e registra no root
		root.addObserver(this.game.getObserver("mainMenuGroup"));
		
		// Seta o menu como spatial atual
		this.game.setCurrentSpatial(menu);
	}

}
