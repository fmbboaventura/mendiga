package superjumper.behavior;

import superjumper.spatial.World;
import superjumper.spatial.node.Bob;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Player;
import MEnDiGa.spatial.nodes.Velocity;

public class UpdateBob extends Behavior {
	
	public UpdateBob() {
		
	}

	@Override
	public void execute(Object... params) {
		if (params == null)
			return;
		Bob bob = (Bob) params[0];
		String command = bob.getCurrentCommand();
		
		// Limitando o valor do deltaTime para evitar um timestep muito grande
		Float deltaTime = Math.min((Float) params[1], 1 / 30f);
		
		float accel = 0;
		Position position = (Position) bob.getCurrentLocation();

		SpriteNode graphicNode = (SpriteNode) bob
				.getGraphicNode(bob.getName());
		
		if (!bob.hit) {
			if (command.equals(Player.MOVE_LEFT)) {
				accel = -5;
				graphicNode.flip(true, false);
			} else if (command.equals(Player.MOVE_RIGHT)) {
				accel = 5;
				graphicNode.flip(false, false);
			}
		}

		Velocity v = bob.getProperty(Velocity.class);
		v.setX(accel / 10 * Bob.BOB_MOVE_VELOCITY);
		v.setY(v.getY() + (World.gravity * deltaTime));

		position.set(position.getX() + v.getX() * deltaTime, position.getY()
				+ v.getY() * deltaTime);

		bob.heightSoFar = Math.max(position.getY(), bob.heightSoFar);

		if (v.getY() < 0 && !bob.hit) {
			graphicNode.setCurrentAnimation(Bob.FALL_ANIMATION);
		}

		if (position.getX() < 0)
			position.setX(World.WORLD_WIDTH);
		if (position.getX() > World.WORLD_WIDTH)
			position.setX(0);

		if (position.getY() > World.WORLD_CAM.getY())
			World.WORLD_CAM.setY(position.getY());
	}

}
