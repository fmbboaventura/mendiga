package superjumper.behavior;

import superjumper.spatial.node.HelpNode;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.behaviors.DepthComparator;
import MEnDiGa.spatial.Spatial;

public class ChangeTip extends Behavior {

	private GameSession game;

	public ChangeTip(GameSession game) {
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		Spatial s = game.getSpatial("help");
		HelpNode helpNode = (HelpNode) s.getNodeGroup("help").get(0);
		
		s.removeNode(helpNode);
		
		if(helpNode.getPage() == 5){
			// Retorna para a primeira dica
			s.addNode(new HelpNode(1));
			s.sortNodes(new DepthComparator());
			// Retorna para o menu principal
			game.getBehavior("changeToMenu").execute();
			return;
		}
		
		// Exibe a pr�xima dica
		s.addNode(new HelpNode(helpNode.getPage() + 1));
		s.sortNodes(new DepthComparator());
	}

}
