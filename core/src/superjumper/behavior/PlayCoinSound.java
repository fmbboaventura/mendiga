package superjumper.behavior;

import superjumper.SuperJumperGame;

public class PlayCoinSound extends PlayAudioNode {

	public PlayCoinSound(SuperJumperGame game) {
		super(game);
	}

	@Override
	public void execute(Object... params) {
		super.execute(this.game.getAudioNode(SuperJumperGame.COIN_SOUND));
	}
}
