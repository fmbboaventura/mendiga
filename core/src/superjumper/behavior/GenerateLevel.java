package superjumper.behavior;

import java.util.Random;

import superjumper.spatial.World;
import superjumper.spatial.node.BackgroundNode;
import superjumper.spatial.node.Bob;
import superjumper.spatial.node.Castle;
import superjumper.spatial.node.Coin;
import superjumper.spatial.node.Platform;
import superjumper.spatial.node.Spring;
import superjumper.spatial.node.Squirrel;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.behaviors.DepthComparator;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Node;
import MEnDiGa.spatial.nodes.Velocity;

public class GenerateLevel extends Behavior {
	GameSession game;
	private Random rand;
	
	public GenerateLevel(GameSession game) {
		super();
		this.game = game;
		this.rand = new Random();
	}

	@Override
	public void execute(Object... params) {
		Node background = new BackgroundNode();
		background.getGraphicNode(background.getName()).setScale(World.UNITY_SCALE);
		
		World world = new World("world");
		world.addNode(background);
		
		float y = Platform.PLATFORM_HEIGHT / 2;
		float maxJumpHeight = Bob.BOB_JUMP_VELOCITY * Bob.BOB_JUMP_VELOCITY 
				/ (2 * -World.gravity);
		
		while (y < World.WORLD_HEIGHT - World.WORLD_WIDTH / 2){
			Platform platform = new Platform();
			
			float x = rand.nextFloat() * (World.WORLD_WIDTH - Platform.PLATFORM_WIDTH);
			
			Position p = (Position) platform.getCurrentLocation();
			p.set(x, y);
			
			// Decidindo se a plataforma � movel
			if(rand.nextFloat() > 0.8f){
				platform.addProperty(new Velocity(3, 0));
			}
			
			//Decidindo se a plataforma ter� uma mola
			if(rand.nextFloat() > 0.9f && !platform.hasProperty(Velocity.class)){
				Spring spring = new Spring();
				Position springPosition = new Position("springPosition", 
						p.getX() + Platform.PLATFORM_WIDTH/4,
						p.getY() + Platform.PLATFORM_HEIGHT/2, 0);
				spring.setCurrentLocation(springPosition);
				world.addNode(spring);
			}
			
			if (y > World.WORLD_HEIGHT / 3 && rand.nextFloat() > 0.8f){
				Squirrel squirrel = new Squirrel();
				Position squirrelPosition =  new Position("squirrelPosition",
						p.getX() + rand.nextFloat(),
						p.getY() + Squirrel.SQUIRREL_HEIGHT + rand.nextFloat() * 2, 0);
				squirrel.setCurrentLocation(squirrelPosition);
				world.addNode(squirrel);
			}
			
			if (rand.nextFloat() > 0.6f){
				Coin coin = new Coin();
				Position coinPosition = new Position("coinPosition", 
						p.getX() + rand.nextFloat(), 
						p.getY() + Coin.COIN_HEIGHT + rand.nextFloat() * 3, 0);
				coin.setCurrentLocation(coinPosition);
				world.addNode(coin);
			}
			
			y += (maxJumpHeight - 0.5f);
			y -= rand.nextFloat() * (maxJumpHeight / 3);
			world.addNode(platform);
		}
		Castle castle = new Castle();
		Position castlePosition = (Position) castle.getCurrentLocation();
		castlePosition.setY(y);
		world.addNode(castle);
		world.addNode(Bob.getInstance());
		world.sortNodes(new DepthComparator());
		this.game.setCurrentSpatial(world);
	}

}
