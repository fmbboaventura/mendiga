package superjumper.behavior;

import superjumper.spatial.World;
import superjumper.spatial.node.Bob;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Score;
import MEnDiGa.spatial.nodes.Velocity;

public class ResetBob extends Behavior {

	@Override
	public void execute(Object... params) {
		Position p = (Position) Bob.getInstance().getCurrentLocation();
		p.set(0, 0);
		Bob.getInstance().getProperty(Velocity.class)
				.set(0, Bob.BOB_JUMP_VELOCITY);
		Bob.getInstance().getProperty(Score.class).clear();
		Bob.getInstance().hit = false;
		Bob.getInstance().heightSoFar = 0;

		SpriteNode graphicNode = (SpriteNode) Bob.getInstance()
				.getGraphicNode(Bob.getInstance().getName());
		graphicNode.flip(false, false);
		graphicNode.setCurrentAnimation(Bob.JUMP_ANIMATION);
		graphicNode.setScale(World.UNITY_SCALE);
	}

}
