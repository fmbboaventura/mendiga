package superjumper.behavior;

import java.util.Random;

import superjumper.spatial.node.Bob;
import superjumper.spatial.node.Platform;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Velocity;

public class ResolveBobPlatformCollision extends Behavior {
	
	private Bob bob;
	private Random rand;
	
	public ResolveBobPlatformCollision(Bob bob) {
		this.bob = bob;
		rand = new Random();
	}

	@Override
	public void execute(Object... params) {
		// Aplica velocidade no eixo y e seta a anima��o do pulo como atual
		bob.getProperty(Velocity.class).setY(Bob.BOB_JUMP_VELOCITY);
		SpriteNode graphicNode = (SpriteNode) bob.getGraphicNode(bob.getName());
		graphicNode.setCurrentAnimation(Bob.JUMP_ANIMATION);
		
		// Decide se a plataforma deve ser destru�da
		if (rand.nextFloat() > 0.5f){
			Platform p = (Platform) params[1];
			p.pulverizing = true;
			
			// Contador do tempo. Incrementado com o deltaTime
			p.stateTime = 0;
			if(p.hasProperty(Velocity.class)) p.getProperty(Velocity.class).setX(0);
			
			SpriteNode platformGraphicNode = 
					(SpriteNode) p.getGraphicNode(p.getName());
			platformGraphicNode.setCurrentAnimation(Platform.PULVERIZE_ANIMATION);
		}
	}

}
