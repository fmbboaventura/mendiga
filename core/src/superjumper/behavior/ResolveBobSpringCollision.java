package superjumper.behavior;

import superjumper.spatial.node.Bob;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.nodes.Velocity;

public class ResolveBobSpringCollision extends Behavior {
	
	private Bob bob;

	public ResolveBobSpringCollision(Bob bob) {
		this.bob = bob;
	}

	@Override
	public void execute(Object... params) {
		Velocity v = bob.getProperty(Velocity.class);
		v.setY(Bob.BOB_JUMP_VELOCITY * 1.5f);
	}

}
