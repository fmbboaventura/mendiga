package superjumper.behavior;

import superjumper.SuperJumperGame;
import superjumper.spatial.HudNodes;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.locations.Position;

public class UpdateHighScores extends Behavior {

	private SuperJumperGame game;

	public UpdateHighScores(SuperJumperGame game) {
		super();
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		int score = (Integer) params[0];
		int[] highscores = this.game.getHighscores();

		for (int i = 0; i < 5; i++) {
			if (highscores[i] < score) {
				for (int j = 4; j > i; j--)
					highscores[j] = highscores[j - 1];
				highscores[i] = score;
				break;
			}
		}
		// Muda a mensagem do scoreNode e o recua para esquerda
		HudNodes.SCORE_NODE.setText("NEW HIGHSCORE: " + score);
		((Position) HudNodes.SCORE_NODE.getCurrentLocation()).setX(0);
	}

}
