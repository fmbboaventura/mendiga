package superjumper.behavior;

import java.util.Iterator;

import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.nodes.Node;

public abstract class UpdateNodes extends Behavior {
	
	protected Spatial spatial;
	
	public UpdateNodes(Spatial spatial) {
		this.spatial = spatial;
	}

	@Override
	public void execute(Object... params) {
		for(Iterator<Node> it = spatial.getNodes().iterator(); it.hasNext();) {
			boolean remove = processNode(it.next(), params);
			if(remove){
				it.remove();
			}
		}
	}

	protected abstract boolean processNode(Node node, Object... params);
}
