package superjumper.behavior;

import superjumper.SuperJumperGame;

public class PlayHitSound extends PlayAudioNode {

	public PlayHitSound(SuperJumperGame game) {
		super(game);
	}

	@Override
	public void execute(Object... params) {
		super.execute(this.game.getAudioNode(SuperJumperGame.HIT_SOUND));
	}
}
