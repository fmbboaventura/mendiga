package superjumper.behavior;

import superjumper.SuperJumperGame;
import superjumper.SuperJumperGame.GameState;
import superjumper.observer.ObserverGroup;
import superjumper.spatial.HudNodes;
import superjumper.spatial.node.Bob;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.nodes.Score;

public class EndGame extends Behavior {
	
	private GameSession game;

	public EndGame(GameSession game) {
		super();
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		Bob bob = Bob.getInstance();
		this.game.getObserver("checkHighScores").fire(
				bob.getProperty(Score.class).getValue());
		Spatial hud = this.game.getSpatial("hud");
		hud.addNode(HudNodes.GAME_OVER_NODE);
		hud.removeNode(HudNodes.PAUSE_BUTTON_NODE);
		((SuperJumperGame)this.game).setGameState(GameState.GAME_OVER);
		
		ObserverGroup root = (ObserverGroup) this.game.getObserver("group");
		Observer clickedGameOver = this.game.getObserver("clickedGameOver");
		root.clearObservers();
		root.addObserver(clickedGameOver);
	}

}
