package superjumper.behavior;

import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;

public class UpdateRunning extends Behavior {
	
	private GameSession game;

	public UpdateRunning(GameSession game) {
		super();
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		this.game.getBehavior("update").execute(params);
		this.game.getObserver("collisionObserver").fire(new Object[2]);
		this.game.getObserver("checkGameOver").fire();
	}

}
