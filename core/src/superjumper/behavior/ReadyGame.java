package superjumper.behavior;

import java.util.List;

import superjumper.SuperJumperGame;
import superjumper.SuperJumperGame.GameState;
import superjumper.observer.BobCoinCollisionObserver;
import superjumper.observer.BobPlatformCollisionObserver;
import superjumper.observer.BobSpringCollisionObserver;
import superjumper.observer.BobSquirrelCollisionObserver;
import superjumper.observer.ObserverGroup;
import superjumper.spatial.HudNodes;
import superjumper.spatial.World;
import superjumper.spatial.node.Bob;
import superjumper.spatial.node.Platform;
import superjumper.spatial.node.Squirrel;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Node;

public class ReadyGame extends Behavior {

	GameSession game;

	public ReadyGame(GameSession game) {
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		this.game.getBehavior("generateLevel").execute();
		this.game.getBehavior("resetBob").execute();
		World world = (World) this.game.getSpatial("world");

		// Recupera a lista de plataformas
		final List<Node> platforms = world.getNodeGroup("platform");

		/* UpdateNodes chama o processNode() para
		 * cada Node do spatial. Se o retorno for
		 * true, o node processado � removido do
		 * spatial.
		 */
		Behavior b = new UpdateNodes(world) {

			@Override
			protected boolean processNode(Node node, Object... params) {
				Behavior b = null;
				if (node instanceof Platform) {
					/* Behavior responsavel por mover as plataformas
					 * e e decidir se elas devem ser destru�das.
					 * Ele recebe a mesma lista de plataformas que o
					 * CollisionObserver recebe. Dessa forma, o
					 * observer n�o avalia uma plataforma destru�da.
					 */
					b = new UpdatePlatform(platforms);
				} else if (node instanceof Bob) {
					/* Behavior respons�vel por atualizar o estado
					 * do player.
					 */
					b = new UpdateBob();
				} else if (node instanceof Squirrel) {
					// Behavior respons�vel por mover os esquilos
					b = new UpdateSquirel();
				} else
					return false;

				// params[0] � o deltaTime. Ser� fornecido por outra classe
				Object[] args = new Object[] { node, params[0], false };
				b.execute(args);

				/* O behavior executado p�e na posi��o 2 do
				 * array fornecido, um boolean, aprovando ou
				 * n�o a remo��o do node processado
				 */
				return (Boolean) args[2];
			}
		};

		this.game.addBehavior("update", b);

		/* Behaviors para tratar eventos de colis�o do player com
		 * diversos nodes do jogo
		 */
		Behavior resolvePlatformCollision = new ResolveBobPlatformCollision(
				Bob.getInstance());
		Behavior resolveSpringCollision = new ResolveBobSpringCollision(
				Bob.getInstance());
		Behavior resolveCoinCollision = new ResolveBobCoinCollision(this.game);
		Behavior resolveSquirelCollision = new ResolveBobSquirrelCollision(
				this.game);

		/* Observers para avaliar se houve colis�o entre bob e
		 * os outros nodes do jogo
		 */
		Observer platformCollision = new BobPlatformCollisionObserver(
				"bobPlatformCollision", Bob.getInstance(), platforms);
		platformCollision.addBehavior(resolvePlatformCollision);
		platformCollision.addBehavior(new PlayJumpSound(
				(SuperJumperGame) this.game));

		Observer springCollision = new BobSpringCollisionObserver(
				"bobSpringCollision", Bob.getInstance(),
				world.getNodeGroup("spring"));
		springCollision.addBehavior(resolveSpringCollision);
		springCollision.addBehavior(new PlayHighJumpSound(
				(SuperJumperGame) this.game));

		Observer coinCoillision = new BobCoinCollisionObserver(
				"bobCoinCollision", Bob.getInstance(),
				world.getNodeGroup("coin"));
		coinCoillision.addBehavior(resolveCoinCollision);
		coinCoillision.addBehavior(new PlayCoinSound(
				(SuperJumperGame) this.game));

		Observer squirrelCollision = new BobSquirrelCollisionObserver(
				"bobSquirrelCollision", Bob.getInstance(),
				world.getNodeGroup("squirrel"));
		squirrelCollision.addBehavior(resolveSquirelCollision);
		squirrelCollision.addBehavior(new PlayHitSound(
				(SuperJumperGame) this.game));

		Observer castleCollision = new BobCoinCollisionObserver(
				"bobCastleCollision", Bob.getInstance(),
				world.getNodeGroup("castle"));
		castleCollision.addBehavior(new ChangeToEndingScreen(this.game));

		// Cria um ObserverGroup para conter os CollisionObservers
		ObserverGroup collisionObserver = new ObserverGroup("collisionObserver");
		collisionObserver.addObserver(springCollision);
		collisionObserver.addObserver(platformCollision);
		collisionObserver.addObserver(coinCoillision);
		collisionObserver.addObserver(squirrelCollision);
		collisionObserver.addObserver(castleCollision);

		this.game.addObserver(collisionObserver);

		// Seta a posi��o inicial da camera
		World.WORLD_CAM.setX(World.WORLD_CAM.getViewportWidth() / 2);
		World.WORLD_CAM.setY(World.WORLD_CAM.getViewportHeight() / 2);

		// Cria o spatial que cont�m os nodes do hud
		this.game.getBehavior("createHud").execute();

		// Pega o observer que t� registrado no inputAdapter e limpa
		ObserverGroup root = (ObserverGroup) this.game.getObserver("group");
		root.clearObservers();

		Observer clickedReady = this.game.getObserver("clickedReady");

		// Registra o observer do gameReady como o atual
		root.addObserver(clickedReady);

		// Exibe a mensagem "READY?"
		Spatial hud = this.game.getSpatial("hud");
		hud.addNode(HudNodes.READY_NODE);
		
		// Seta o texto do score display
		HudNodes.SCORE_NODE.setText("Score: " + 0);
		((Position) HudNodes.SCORE_NODE.getCurrentLocation()).setX(16);

		// Seta o estado do jogo como "pronto"
		((SuperJumperGame) this.game).setGameState(GameState.READY);
	}
}
