package superjumper.behavior;

import superjumper.observer.ClickNodeObserver;
import superjumper.observer.JustClickedObserver;
import superjumper.observer.ObserverGroup;
import superjumper.spatial.HudNodes;
import superjumper.spatial.node.Bob;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.observers.InputObserver;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;

public class CreateHud extends Behavior {
	
	private GameSession game;

	public CreateHud(GameSession game) {
		super();
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		Spatial hud = this.game.getSpatial("hud");

		if (hud == null) {
			// recupera o playClickSoundBehavior
			Behavior playClickSound = this.game.getBehavior("playClickSound");
			
			// cria o grupo de observers do gameplay
			ObserverGroup gameRunningGroup = new ObserverGroup("gameRunningGroup");
			
			Observer clickedPauseObserver = new ClickNodeObserver(HudNodes.PAUSE_BUTTON_NODE);
			clickedPauseObserver.addBehavior(new PauseGame(game));
			clickedPauseObserver.addBehavior(playClickSound);
			
			gameRunningGroup.addObserver(clickedPauseObserver);
			gameRunningGroup.addObserver(new InputObserver("input", Bob.getInstance()));
			
			// Cria o grupo de opserver do gamePaused
			ObserverGroup gamePausedGroup = new ObserverGroup("gamePausedGroup");
			
			Observer clickedResumeObserver = new ClickNodeObserver(HudNodes.RESUME);
			Behavior resumeGame = new ResumeGame(this.game);
			clickedResumeObserver.addBehavior(resumeGame);
			clickedResumeObserver.addBehavior(playClickSound);
			
			Observer clickedQuitObserver = new ClickNodeObserver(HudNodes.QUIT);
			clickedQuitObserver.addBehavior(this.game.getBehavior("changeToMenu"));
			clickedQuitObserver.addBehavior(playClickSound);
			
			gamePausedGroup.addObserver(clickedResumeObserver);
			gamePausedGroup.addObserver(clickedQuitObserver);
			
			// Cria o observer do gameReady
			Observer clickedReady = new JustClickedObserver("clickedReady");
			clickedReady.addBehavior(resumeGame);
			clickedReady.addBehavior(playClickSound);
			
			// Cria o observer do gameOver
			Observer clickedGameOver = new JustClickedObserver("clickedGameOver");
			clickedGameOver.addBehavior(new ChangeToMainMenuScreen(this.game));
			clickedGameOver.addBehavior(playClickSound);
			
			this.game.addObserver(gameRunningGroup);
			this.game.addObserver(gamePausedGroup);
			this.game.addObserver(clickedReady);
			this.game.addObserver(clickedGameOver);
			
		}
		hud = new Spatial("hud");
		hud.addNode(HudNodes.SCORE_NODE);
		hud.addNode(HudNodes.PAUSE_BUTTON_NODE);
		this.game.addSpatial(hud.getId(), hud);
	}

}
