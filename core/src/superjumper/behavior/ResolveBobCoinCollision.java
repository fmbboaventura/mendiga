package superjumper.behavior;

import superjumper.spatial.HudNodes;
import superjumper.spatial.node.Bob;
import superjumper.spatial.node.Coin;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.nodes.Score;

public class ResolveBobCoinCollision extends Behavior {
	
	GameSession game;

	public ResolveBobCoinCollision(GameSession game) {
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		// Remove a moeda
		Coin coin = (Coin) params[1];
		Spatial world = this.game.getSpatial("world");
		world.removeNode(coin);
		
		// Incrementa o score
		Score score = Bob.getInstance().getProperty(Score.class);
		score.add(10);
		
		// Atualiza o hud
		HudNodes.SCORE_NODE.setText("Score: " + score.getValue());
	}

}
