package superjumper.behavior;

import superjumper.SuperJumperGame;
import superjumper.observer.ClickNodeObserver;
import superjumper.observer.ObserverGroup;
import superjumper.spatial.node.ArrowNode;
import superjumper.spatial.node.BackgroundNode;
import superjumper.spatial.node.HighScoresLabel;
import superjumper.spatial.node.TextViewNode;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Node;

public class ChangeToHighScoresScreen extends Behavior {

	private GameSession game;

	public ChangeToHighScoresScreen(GameSession game) {
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		Spatial highScoresSpatial = new Spatial("highScores");
		highScoresSpatial.addNode(new BackgroundNode());
		highScoresSpatial.addNode(new HighScoresLabel());

		Node backButton = new ArrowNode();
		Position backPosition = (Position) backButton.getCurrentLocation();
		backPosition.set(0, 0);
		highScoresSpatial.addNode(backButton);

		Observer clickedBackObserver = new ClickNodeObserver(backButton);
		clickedBackObserver.addBehavior(this.game.getBehavior("changeToMenu"));

		int[] highScores = ((SuperJumperGame) this.game).getHighscores();

		for (int i = 0, y = 370; i < highScores.length; i++, y -= 28) {
			TextViewNode textViewNode = new TextViewNode("highScore" + i);
			textViewNode.setText(i + 1 + ". " + highScores[i]);
			
			Position p = (Position) textViewNode.getCurrentLocation();
			p.set(120, y);
			highScoresSpatial.addNode(textViewNode);
		}

		ObserverGroup root = (ObserverGroup) this.game.getObserver("group");
		root.clearObservers();
		root.addObserver(clickedBackObserver);
		this.game.setCurrentSpatial(highScoresSpatial);
	}
}
