package superjumper.behavior;

import superjumper.observer.JustClickedObserver;
import superjumper.observer.ObserverGroup;
import superjumper.spatial.node.BackgroundNode;
import superjumper.spatial.node.Bob;
import superjumper.spatial.node.Castle;
import superjumper.spatial.node.MessageNode;
import superjumper.spatial.node.Princess;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.behaviors.DepthComparator;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class ChangeToEndingScreen extends Behavior {
	
	private GameSession game;
	
	public ChangeToEndingScreen(GameSession game){
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		Spatial end = this.game.getSpatial("end");
		
		if(end == null){
			end = new Spatial("end");
			end.addNode(new BackgroundNode());
			
			Node castle = new Castle();
			Position castlePosition = (Position) castle.getCurrentLocation();
			castlePosition.set(60, 120);
			SpriteNode castleGraphicNode = (SpriteNode) castle
					.getGraphicNode(castle.getName());
			castleGraphicNode.setScale(200/64f);
			end.addNode(castle);
			
			end.addNode(new Princess());
			
			end.addNode(new MessageNode("msgNode"));
			
			
			end.sortNodes(new DepthComparator());
			
			Behavior playClickSound = this.game.getBehavior("playClickSound");
			
			// Observer que dispara evento quando o player clica
			Observer clickedNextMsg = new JustClickedObserver("clickedNextMsg");
			clickedNextMsg.addBehavior(playClickSound);
			
			// Behavior responsável por mudar a mensagem da tela final
			clickedNextMsg.addBehavior(new ChangeMessage(game));
			this.game.addObserver(clickedNextMsg);
		}
		
		Node bob = Bob.getInstance();
		Position bobPosition = (Position) bob.getCurrentLocation();
		bobPosition.set(120, 200);
		SpriteNode bobGraphicNode = (SpriteNode) bob
				.getGraphicNode(bob.getName());
		bobGraphicNode.setScale(1);
		bobGraphicNode.setCurrentFrame(2);
		bobGraphicNode.flip(false, false);
		end.addNode(bob);
		
		ObserverGroup group = (ObserverGroup) game.getObserver("group");
		group.clearObservers();
		group.addObserver(this.game.getObserver("clickedNextMsg"));
		this.game.setCurrentSpatial(end);
	}

}
