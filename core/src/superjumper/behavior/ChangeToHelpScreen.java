package superjumper.behavior;

import superjumper.observer.ClickNodeObserver;
import superjumper.observer.ObserverGroup;
import superjumper.spatial.node.HelpNode;
import superjumper.spatial.node.ArrowNode;
import MEnDiGa.GameSession;
import MEnDiGa.behaviors.Behavior;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.nodes.Node;

public class ChangeToHelpScreen extends Behavior {
	
	private GameSession game;
	
	public ChangeToHelpScreen(GameSession game) {
		this.game = game;
	}

	@Override
	public void execute(Object... params) {
		Spatial s = game.getSpatial("help");
		
		if(s != null) {
			game.setCurrentSpatial(s);
		} else {
			s = new Spatial("help");
			s.addNode(new HelpNode(1));
			
			Node next = new ArrowNode(true, false);
			s.addNode(next);
			
			Behavior playClickSound = this.game.getBehavior("playClickSound");
			
			Observer clickedNextTipObserver = new ClickNodeObserver(next);
			// Behavior responsável por mudar a dica da tela de ajuda
			clickedNextTipObserver.addBehavior(new ChangeTip(game));
			clickedNextTipObserver.addBehavior(playClickSound);
			
			game.addObserver(clickedNextTipObserver);
			
			game.addSpatial("help", s);
			game.setCurrentSpatial(s);
		}
		ObserverGroup group = (ObserverGroup) game.getObserver("group");
		group.clearObservers();
		group.addObserver(this.game.getObserver("nextClickObserver"));
		
	}

}
