package superjumper.behavior;

import superjumper.SuperJumperGame;

public class PlayClickSound extends PlayAudioNode {

	public PlayClickSound(SuperJumperGame game) {
		super(game);
	}

	@Override
	public void execute(Object... params) {
		super.execute(this.game.getAudioNode(SuperJumperGame.CLICK_SOUND));
	}

}
