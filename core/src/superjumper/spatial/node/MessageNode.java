package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.TextNode;
import MEnDiGa.spatial.nodes.TextNode.Alignment;

public class MessageNode extends TextViewNode {
	
	private String[] messages = {
			"Princess: Oh dear!\n What have you done?",
			"Bob: I came to \nrescue you!",
			"Princess: you are\n mistaken\nI need no rescueing",
			"Bob: So all this \nwork for nothing?",
			"Princess: I have \ncake and tea!\nWould you like some?",
			"Bob: I'd be my \npleasure!",
			"And they ate cake\nand drank tea\nhappily ever \nafter\n\n\n\n\n\n\nKära Emma!\nDu är fantastisk!\nDu blev ferdig\n med spelet!" };
	private int currentMessage = 0;

	public MessageNode(String name) {
		super(name);
		TextNode text = (TextNode) this.getGraphicNode(this.name);
		text.setText(messages[currentMessage]);
		text.setAlignmentWidth(320);
		
		((Position) this.getCurrentLocation()).set(0, 400);
	}

	public void nextMessage() {
		this.currentMessage++;
		
		TextNode text = (TextNode) this.getGraphicNode(this.name);
		text.setText(messages[currentMessage]);
		text.setAlignment(Alignment.CENTER);
	}
	
	public int getCurrentMessageIndex(){
		return currentMessage;
	}
	
	public void reset(){
		this.currentMessage = -1;
		this.nextMessage();
	}

}
