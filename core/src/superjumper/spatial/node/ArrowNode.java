package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class ArrowNode extends Node{

	public ArrowNode(boolean flipX, boolean flipY) {
		super("next");
		this.groupId = "hud";
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png");
		graphicNode.setRegion(0, 64, 64, 64);
		graphicNode.flip(flipX, flipY);
		this.addGraphicNode(graphicNode);
		
		BoxBound box = new BoxBound(64, 64);
		this.setBounds(box);
		
		Position p = new Position(this.getName(), 320 - 64, 0, 1);
		this.setCurrentLocation(p);
	}
	
	public ArrowNode(){
		this(false, false);
	}

}
