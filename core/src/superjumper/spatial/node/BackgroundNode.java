package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class BackgroundNode extends Node {

	public BackgroundNode() {
		super("background");
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/background.png");
		graphicNode.setRegion(320, 480);
		//graphicNode.setScale(World.UNITY_SCALE);
		this.addGraphicNode(graphicNode);
		
		this.currentLocation = new Position(name, 0, 0, 0);
	}

}
