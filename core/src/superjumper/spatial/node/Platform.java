package superjumper.spatial.node;

import superjumper.spatial.World;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.SpriteNode.PlayMode;

public class Platform extends Element {
	
	public static final float PLATFORM_WIDTH = 2;
	public static final float PLATFORM_HEIGHT = 0.5f;
	public static final float PLATFORM_PULVERIZE_TIME = 0.2f * 4;
	public static final String PULVERIZE_ANIMATION = "pulverize";
	public static int PLATFORM_NUMBER = 1;
	public float stateTime;
	public boolean pulverizing;
	

	public Platform() {
		super("platform_" + PLATFORM_NUMBER);
		this.groupId = "platform";
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png", 64, 16);
		graphicNode.setRegion(64, 160, 64, 64);
		graphicNode.setScale(World.UNITY_SCALE);
		graphicNode.defineAnimation(PULVERIZE_ANIMATION, new int[]{0, 1, 2, 3}, 0.2f, PlayMode.LOOP_NORMAL);
		this.addGraphicNode(graphicNode);
		
		BoxBound bound = new BoxBound(PLATFORM_WIDTH, PLATFORM_HEIGHT);
		this.setBounds(bound);
		
		this.currentLocation = new Position(name, 2, 2, PLATFORM_NUMBER);
		PLATFORM_NUMBER++;
		stateTime = 0;
		pulverizing = false;
	}

}
