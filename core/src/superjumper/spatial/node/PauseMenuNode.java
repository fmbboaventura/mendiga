package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class PauseMenuNode extends Node {
	
	public static enum PauseMenuItem{
		RESUME,
		QUIT
	}

	public PauseMenuNode(PauseMenuItem itemType) {
		super(itemType.toString());
		this.groupId = "hud";
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png", 192, 48);
		graphicNode.setRegion(224, 128, 192, 96);
		this.addGraphicNode(graphicNode);
		
		int currentFrame = 0;
		float y = 240 - 96 / 2;
		
		BoxBound box = new BoxBound(192, 48);
		this.setBounds(box);
		
		switch(itemType){
		case RESUME: currentFrame = 0; y += 48; break;
		case QUIT: currentFrame = 1; break;
		}
		
		graphicNode.setCurrentFrame(currentFrame);
		
		this.currentLocation = new Position(name, 160 - 192 / 2, y, 0);
	}

}
