package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class TitleNode extends Node {

	public TitleNode() {
		super("title");
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png");
		graphicNode.setRegion(0, 352, 274, 142);
		this.addGraphicNode(graphicNode);
		
		Position p = new Position(this.getName(), 160 - 274 / 2, 480 - 10 - 142, 0);
		this.setCurrentLocation(p);
	}

}
