package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class Princess extends Node {

	public Princess() {
		super("princess");
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png");
		graphicNode.setRegion(170, 127, 32, 32);
		graphicNode.flip(true, false);
		this.addGraphicNode(graphicNode);
		
		Position p = new Position(getName(), 173, 200, 0);
		this.setCurrentLocation(p);
	}

}
