package superjumper.spatial.node;

import superjumper.spatial.World;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class Castle extends Node {
	
	public static float CASTLE_WIDTH = 1.7f;
	public static float CASTLE_HEIGHT = 1.7f;

	public Castle() {
		super("castle");
		this.groupId = "castle";
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png", 64, 64);
		graphicNode.setScale(World.UNITY_SCALE);
		graphicNode.setRegion(128, 64, 64, 64);
		this.addGraphicNode(graphicNode);
		
		BoxBound box = new BoxBound(CASTLE_WIDTH, CASTLE_HEIGHT);
		this.setBounds(box);
		
		this.currentLocation = new Position(name, World.WORLD_WIDTH/2 - 1, 0, 0);
	}

}
