package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class MuteButton extends Node {
	
	public static final int MUTE = 0;
	public static final int UNMUTE = 1;

	public MuteButton() {
		super("mute");
		this.groupId = "hud";
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png", 64, 64);
		graphicNode.setRegion(0, 0, 128, 64);
		graphicNode.setCurrentFrame(UNMUTE);
		this.addGraphicNode(graphicNode);
		
		BoxBound box = new BoxBound(64, 64);
		this.setBounds(box);
		
		Position p = new Position(this.getName(), 0, 0, 0);
		this.setCurrentLocation(p);
	}
	
}
