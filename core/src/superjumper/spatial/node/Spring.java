package superjumper.spatial.node;

import superjumper.spatial.World;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class Spring extends Node {
	public static float SPRING_WIDTH = 0.3f;
	public static float SPRING_HEIGHT = 0.3f;

	public Spring() {
		super("spring");
		this.groupId = "spring";
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png", 32, 32);
		graphicNode.setRegion(128, 0, 32, 32);
		graphicNode.setScale(World.UNITY_SCALE);
		
		BoxBound box = new BoxBound(SPRING_WIDTH, SPRING_HEIGHT);
		this.setBounds(box);
		
		this.addGraphicNode(graphicNode);
	}

}
