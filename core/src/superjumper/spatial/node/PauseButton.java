package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class PauseButton extends Node {

	public PauseButton() {
		super("pause");
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png");
		this.groupId = "hud";
		graphicNode.setRegion(64, 64, 64, 64);
		this.addGraphicNode(graphicNode);
		
		BoxBound box = new BoxBound(64, 64);
		this.setBounds(box);
		
		this.currentLocation = new Position(name, 320 - 64, 480 - 64, 0);
	}

}
