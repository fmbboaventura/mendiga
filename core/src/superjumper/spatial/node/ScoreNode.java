package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;

public class ScoreNode extends TextViewNode {

	public ScoreNode(String name) {
		super(name);
		((Position) this.getCurrentLocation()).set(0, 480 - 20);
	}
}
