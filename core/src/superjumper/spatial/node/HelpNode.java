package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class HelpNode extends Node {

	private int page;

	public HelpNode(int helpPage) {
		super("helpNode" + helpPage);
		this.page = helpPage;
		this.groupId = "help";
		
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
		// Caminho da imagem correspondente � pagina
				"res/superjumper/help" + helpPage + ".png");
		graphicNode.setRegion(320, 480);
		this.addGraphicNode(graphicNode);
		
		this.currentLocation = new Position(name, 0, 0, 0);
	}
	
	public int getPage(){
		return page;
	}

}
