package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class MainMenuNode extends Node {
	
	public static enum MainMenuOption{
		PLAY,
		HIGHSCORES,
		HELP
	}

	public MainMenuNode(MainMenuOption option) {
		super(option.toString());
		this.groupId = "hud";
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png", 300, 36);
		graphicNode.setRegion(0, 224, 300, 110);
		this.addGraphicNode(graphicNode);
		
		int currentFrame = 0;
		float y = 200 - 110 / 2;
		
		BoxBound box = new BoxBound(300, 36);
		this.setBounds(box);
		
		switch(option){
		case PLAY: currentFrame = 0; y += 36 * 2; break;
		case HIGHSCORES: currentFrame = 1; y += 36; break;
		case HELP: currentFrame = 2; break;
		}
		
		graphicNode.setCurrentFrame(currentFrame);
		
		this.currentLocation = new Position(name, 10, y, 0);
	}

}
