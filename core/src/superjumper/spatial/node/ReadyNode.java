package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class ReadyNode extends Node{

	public ReadyNode() {
		super("ready");
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png");
		graphicNode.setRegion(320, 224, 192, 32);
		this.addGraphicNode(graphicNode);
		
		this.currentLocation = new Position(name, 160 - 192 / 2, 240 - 32 / 2, 0);
	}

}
