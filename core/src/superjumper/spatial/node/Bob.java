package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.SpriteNode.PlayMode;
import MEnDiGa.spatial.nodes.Player;
import MEnDiGa.spatial.nodes.Score;
import MEnDiGa.spatial.nodes.Velocity;

public class Bob extends Player {
	public static final int FALL_FRAME = 4;
	public static final float BOB_JUMP_VELOCITY = 11;
	public static final float BOB_MOVE_VELOCITY = 20;
	public static final float BOB_WIDTH = 0.8f;
	public static final float BOB_HEIGHT = 0.8f;
	public final static String JUMP_ANIMATION = "jump";
	public final static String FALL_ANIMATION = "fall";
	public boolean hit;
	public float heightSoFar;
	
	private static final Bob instance = new Bob();
	
	public static Bob getInstance(){
		return instance;
	}

	private Bob() {
		super("bob");
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png", 32, 32);
		graphicNode.setRegion(0, 128, 160, 32);
		graphicNode.defineAnimation(JUMP_ANIMATION, new int[]{0, 1}, 0.2f, PlayMode.LOOP_NORMAL);
		graphicNode.defineAnimation(FALL_ANIMATION, new int[]{2, 3}, 0.2f, PlayMode.LOOP_NORMAL);
		this.addGraphicNode(graphicNode);
		
		BoxBound box = new BoxBound(BOB_WIDTH, BOB_HEIGHT);
		this.setBounds(box);
		
		this.addProperty(new Velocity(0, BOB_JUMP_VELOCITY));
		this.addProperty(new Score());
		
		this.currentLocation = new Position(name, 0, 0, 1);
	}
}
