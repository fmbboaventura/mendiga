package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class GameOverNode extends Node {

	public GameOverNode() {
		super("gameOver");
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png");
		graphicNode.setRegion(352, 256, 160, 96);
		this.addGraphicNode(graphicNode);
		
		this.currentLocation = new Position(name, 160 - 160 / 2, 240 - 96 / 2, 0);
	}

}
