package superjumper.spatial.node;

import superjumper.spatial.World;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;
import MEnDiGa.spatial.nodes.SpriteNode.PlayMode;

public class Coin extends Node {
	
	public static final float COIN_WIDTH = 0.5f;
	public static final float COIN_HEIGHT = 0.8f;

	public Coin() {
		super("coin");
		this.groupId = "coin";
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png", 32, 32);
		graphicNode.setScale(World.UNITY_SCALE);
		graphicNode.setRegion(128, 32, 96, 32);
		graphicNode.defineAnimation("spin", new int[]{0, 1, 2}, 0.2f, PlayMode.LOOP_NORMAL);
		graphicNode.setCurrentAnimation("spin");
		this.addGraphicNode(graphicNode);
		
		BoxBound box = new BoxBound(COIN_WIDTH, COIN_HEIGHT);
		this.setBounds(box);
		
		this.currentLocation = new Position(name, 0, 0, 0);
	}

}
