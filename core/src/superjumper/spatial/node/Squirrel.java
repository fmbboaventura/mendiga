package superjumper.spatial.node;

import superjumper.spatial.World;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.Velocity;
import MEnDiGa.spatial.nodes.SpriteNode.PlayMode;

public class Squirrel extends Element{
	public static final float SQUIRREL_WIDTH = 1;
	public static final float SQUIRREL_HEIGHT = 0.6f;

	public Squirrel() {
		super("squirrel");
		this.groupId = "squirrel";
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png", 32, 32);
		graphicNode.setScale(World.UNITY_SCALE);
		graphicNode.setRegion(0, 160, 64, 32);
		graphicNode.defineAnimation("fly", new int[]{0, 1},  0.2f, PlayMode.LOOP_NORMAL);
		graphicNode.setCurrentAnimation("fly");
		this.addGraphicNode(graphicNode);
		
		BoxBound box = new BoxBound(SQUIRREL_WIDTH, SQUIRREL_HEIGHT);
		this.setBounds(box);
		
		this.addProperty(new Velocity(3, 0));
		this.currentLocation = new Position(name, 0, 0, 0);
	}

}
