package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Node;
import MEnDiGa.spatial.nodes.TextNode;

public class TextViewNode extends Node {

	public TextViewNode(String name) {
		super(name);
		TextNode text = new TextNode(name, "res/superjumper/font.fnt", "res/superjumper/font.png");
		this.addGraphicNode(text);
		
		Position p = new Position(name, 0, 0, 0);
		this.setCurrentLocation(p);
	}
	
	public void setText(String text){
		TextNode textNode = (TextNode) this.getGraphicNode(this.getName());
		textNode.setText(text);
	}

}