package superjumper.spatial.node;

import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.SpriteNode;
import MEnDiGa.spatial.nodes.Node;

public class HighScoresLabel extends Node {

	public HighScoresLabel() {
		super("highscores");
		SpriteNode graphicNode = new SpriteNode(this.getName(), 
				"res/superjumper/items.png");
		graphicNode.setRegion(0, 257, 300, 110 / 3);
		this.addGraphicNode(graphicNode);
		
		this.currentLocation = new Position(name, 10, 400, 0);
	}

}
