package superjumper.spatial;

import MEnDiGa.spatial.nodes.Node;
import superjumper.spatial.node.GameOverNode;
import superjumper.spatial.node.PauseButton;
import superjumper.spatial.node.PauseMenuNode;
import superjumper.spatial.node.ReadyNode;
import superjumper.spatial.node.ScoreNode;
import superjumper.spatial.node.PauseMenuNode.PauseMenuItem;

public abstract class HudNodes {
	
	public static final ScoreNode SCORE_NODE = new ScoreNode("score");
	public static final Node PAUSE_BUTTON_NODE = new PauseButton();
	public static final Node RESUME = new PauseMenuNode(PauseMenuItem.RESUME);
	public static final Node QUIT = new PauseMenuNode(PauseMenuItem.QUIT);
	public static final Node READY_NODE = new ReadyNode();
	public static final Node GAME_OVER_NODE = new GameOverNode();
}
