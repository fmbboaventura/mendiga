package superjumper.spatial;

import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.nodes.Camera;

public class World extends Spatial {
	
	public static final Camera WORLD_CAM = new Camera("worldCam", 10, 15);
	public static final float WORLD_WIDTH = 10;
	public static final float WORLD_HEIGHT = 15 * 20;
	public static final float UNITY_SCALE = 1/32f;
	public static float gravity = -12;

	public World(String id) {
		super(id);
	}
}
