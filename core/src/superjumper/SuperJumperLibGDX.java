package superjumper;

import java.util.Iterator;

import superjumper.spatial.World;
import MEnDiGa.adapters.libgdx.GameAdapter;
import MEnDiGa.adapters.libgdx.InputAdapter;
import MEnDiGa.adapters.libgdx.MusicAdapter;
import MEnDiGa.adapters.libgdx.SoundAdapter;
import MEnDiGa.adapters.libgdx.graphics.SpatialRenderer;
import MEnDiGa.spatial.Spatial;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.AudioNode;
import MEnDiGa.spatial.nodes.Camera;
import MEnDiGa.spatial.nodes.Node;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;

public class SuperJumperLibGDX extends GameAdapter {

	@Override
	public void create() {
		this.game = new SuperJumperGame();
		viewportWidth = SuperJumperGame.GUI_CAM.getViewportWidth();
		viewportHeight = SuperJumperGame.GUI_CAM.getViewportHeight();
		((SuperJumperGame)game).initialize();
		for(Spatial s : this.game.getSpatials()){
			this.createAdapter(s);
		}
		InputAdapter input = new InputAdapter(this.renders.get(game.getCurrentSpatial().getId()).getCamera());
		input.addObserver(this.game.getObserver("group"));
		input.addObserver(this.game.getObserver("input"));
		Gdx.input.setInputProcessor(input);
		this.createAudioAdapters();
	}

	@Override
	public void createAdapter(Spatial s) {
		Camera camera;
		if(s.getId().equals("world")){
			camera = World.WORLD_CAM;
			Spatial hud = this.game.getSpatial("hud");
			this.createAdapter(hud);
		}
		else{
			camera = SuperJumperGame.GUI_CAM;
		}
		SpatialRenderer renderer = new SpatialRenderer(s, camera);
		this.renders.put(s.getId(), renderer);
	}
	
	private void createAudioAdapters(){
		AudioNode[] nodes = ((SuperJumperGame)game).getAudioNodes();
		for(int i = 0; i < nodes.length - 1; i++){
			AudioNode audio = nodes[i];
			audioAdapters.put(audio.getId(), new SoundAdapter(audio));
		}
		AudioNode music = nodes[SuperJumperGame.MUSIC];
		audioAdapters.put(music.getId(), new MusicAdapter(music));
	}

	@Override
	public void render() {
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		Spatial current = this.game.getCurrentSpatial();
		SpatialRenderer renderer = this.renders.get(current.getId());
		if(renderer == null){
			this.createAdapter(current);
			renderer = this.renders.get(current.getId());
		}
		
		if(!current.getId().equals("world")){
			renderer.beginRender();
			
			for(Node n : current.getNodes()){
				this.drawNode(renderer, n);
			}
			
			renderer.endRender();
		}
		else{
			this.drawWorld(renderer);
			this.drawHud();
			((SuperJumperGame)game).update(Gdx.graphics.getDeltaTime());
		}
		this.updateAudio();
	}
	
	private void updateAudio(){
		for(AudioNode n : ((SuperJumperGame)game).getAudioNodes()){
			this.audioAdapters.get(n.getId()).update();
		}
	}
	
	private void drawWorld(SpatialRenderer currentRender) {
		World world = (World) this.game.getCurrentSpatial();
		currentRender.beginRender(true);

		Iterator<Node> it = world.getNodes().iterator();
		Node background = it.next();
		((Position) background.getCurrentLocation()).set(
				World.WORLD_CAM.getX()
						- World.WORLD_CAM.getViewportWidth() / 2,
				World.WORLD_CAM.getY()
						- World.WORLD_CAM.getViewportHeight() / 2);

		for (Node n : world.getNodes()) {
			this.drawNode(currentRender, n);
		}
		currentRender.endRender();
	}
	
	private void drawHud(){
		SpatialRenderer hudRenderer = this.renders.get("hud");
		Spatial hud = this.game.getSpatial("hud");
		
		hudRenderer.beginRender();
		for(Node n : hud.getNodes()){
			this.drawNode(hudRenderer, n);
		}
		hudRenderer.endRender();
	}
}
