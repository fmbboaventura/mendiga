package superjumper;

import superjumper.behavior.ChangeToMainMenuScreen;
import superjumper.behavior.CreateHud;
import superjumper.behavior.EndGame;
import superjumper.behavior.GenerateLevel;
import superjumper.behavior.PlayClickSound;
import superjumper.behavior.ResetBob;
import superjumper.behavior.UpdateRunning;
import superjumper.observer.CheckForNewHighScore;
import superjumper.observer.CheckGameOver;
import superjumper.observer.ObserverGroup;
import MEnDiGa.GameSession;
import MEnDiGa.spatial.nodes.AudioNode;
import MEnDiGa.spatial.nodes.AudioNode.State;
import MEnDiGa.spatial.nodes.Camera;

public class SuperJumperGame extends GameSession {

	// Camera para renderizar a gui
	public static final Camera GUI_CAM = new Camera("guiCam", 320, 480);

	public static enum GameState {
		RUNNING, PAUSED, READY, GAME_OVER
	}

	private GameState gameState;

	public static final int COIN_SOUND = 0;
	public static final int JUMP_SOUND = 1;
	public static final int HI_JUMP_SOUND = 2;
	public static final int HIT_SOUND = 3;
	public static final int CLICK_SOUND = 4;
	public static final int MUSIC = 5;

	private AudioNode[] audioNodes;

	private boolean soundEnabled;
	
	// Array com os highscores default
	private int[] highscores = new int[] {100, 80, 50, 30, 10};

	public SuperJumperGame() {

		// Cria os audioNodes referentes aos sons do jogo
		this.audioNodes = new AudioNode[] {
				new AudioNode("coinSound", "res/superjumper/coin.wav"),
				new AudioNode("jumpSound", "res/superjumper/jump.wav"),
				new AudioNode("hiJumpSound", "res/superjumper/highjump.wav"),
				new AudioNode("hitSound", "res/superjumper/hit.wav"),
				new AudioNode("click", "res/superjumper/click.wav"),
				new AudioNode("gameMusic", "res/superjumper/music.mp3") };

		// A musica deve ficar em looping
		this.getAudioNode(MUSIC).setLooping(true);
		this.getAudioNode(MUSIC).setVolume(0.5f);
		
		// O audio � habilitado por default
		setSoundEnabled(true);
		
		/* A seguir, s�o registrados os observer e behaviors iniciais do
		 * super jumper. Outros s�o instanciados conforme a necessidade.
		 * Para mais detalhes, ctrl+clique o nome dos behaviors/observers
		 * para ver sua implementa��o + coment�rios.
		 */

		// Adciona o observergroup principal, o que ser� registrado no inputAdapter
		this.addObserver(new ObserverGroup("group"));
		
		// Adciona o observer respons�vel por avaliar se o player atingiu algum highscore
		this.addObserver(new CheckForNewHighScore(this));
		
		// Adciona o observer responsavel por checar que o jogador perdeu
		this.addObserver(new CheckGameOver(this));
		
		/* Adciona o behavior que prepara o spatial do menu principal e 
		 * seta como spatial atual
		 */
		this.addBehavior("changeToMenu", new ChangeToMainMenuScreen(this));
		
		// Behavior respons�vel por gerar um level
		this.addBehavior("generateLevel", new GenerateLevel(this));
		
		/* Behavior respos�vel por criar o spatial que
		 * cont�m os nodes do hud
		 */
		this.addBehavior("createHud", new CreateHud(this));
		
		// Behavior respons�vel por retornar o player ao estado inicial
		this.addBehavior("resetBob", new ResetBob());
		
		/* Behavior respons�vel pela execu��o dos behaviors e observers
		 * respons�veis pela din�mica do jogo em execu��o
		 */
		this.addBehavior(GameState.RUNNING.toString(), new UpdateRunning(this));
		
		// Behavior respons�vel pela mensagem de gameover no hud
		this.addBehavior("endGame", new EndGame(this));
		
		// Behavior respons�vel por mudar o estado do clickSound para PLAYING
		this.addBehavior("playClickSound", new PlayClickSound(this));
	}

	public void initialize() {
		this.getBehavior("changeToMenu").execute();
		this.getAudioNode(MUSIC).setState(State.PLAYING);
	}

	public void update(float deltaTime) {
		// Atualiza e executa os behaviors da mecanica do jogo
		if (this.gameState.equals(GameState.RUNNING)) {
			this.getBehavior(this.gameState.toString()).execute(deltaTime);
		}
	}

	public int[] getHighscores() {
		return highscores;
	}

	public AudioNode[] getAudioNodes() {
		return audioNodes;
	}

	public AudioNode getAudioNode(int index) {
		return audioNodes[index];
	}

	public boolean isSoundEnabled() {
		return soundEnabled;
	}

	public void setSoundEnabled(boolean soundEnabled) {
		this.soundEnabled = soundEnabled;
		if (this.soundEnabled)
			this.getAudioNode(MUSIC).setState(State.PLAYING);
		else
			this.getAudioNode(MUSIC).setState(State.PAUSED);
	}

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}
}
