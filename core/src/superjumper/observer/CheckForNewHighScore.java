package superjumper.observer;

import superjumper.SuperJumperGame;
import superjumper.behavior.UpdateHighScores;
import MEnDiGa.observers.Observer;

public class CheckForNewHighScore extends Observer {

	private SuperJumperGame game;
	
	public CheckForNewHighScore(SuperJumperGame game) {
		super("checkHighScores");
		this.game = game;
		/* Caso a avalia��o seja positiva, esse behavior
		 * atualiza o array de highscores do SuperJumperGame
		 */
		this.addBehavior(new UpdateHighScores(this.game));
	}

	@Override
	public boolean evaluate(Object... params) {
		int score = (Integer) params[0];
		int[] highscores = this.game.getHighscores();
		
		return (score > highscores[4]);
	}

}
