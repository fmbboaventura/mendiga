package superjumper.observer;

import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.nodes.Player;

public class JustClickedObserver extends Observer {

	public JustClickedObserver(String id) {
		super(id);
	}

	@Override
	public boolean evaluate(Object... params) {
		if(params != null && params[0].equals(Player.TOUCH_DOWN)) return true;
		return false;
	}

}
