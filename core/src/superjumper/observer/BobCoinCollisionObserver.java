package superjumper.observer;

import java.util.List;

import superjumper.spatial.node.Bob;
import MEnDiGa.spatial.nodes.Node;

public class BobCoinCollisionObserver extends BobCollisionObserver {

	public BobCoinCollisionObserver(String id, Node node, List<Node> nodeGroup) {
		super(id, node, nodeGroup);
	}

	@Override
	public boolean evaluate(Object... params) {
		if(((Bob)this.node).hit) return false;
		boolean collided = super.evaluate(params);
		if(collided) this.nodeGroup.remove(params[1]);
		return collided;
	}
}
