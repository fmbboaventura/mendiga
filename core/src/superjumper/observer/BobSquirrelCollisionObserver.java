package superjumper.observer;

import java.util.List;

import superjumper.spatial.node.Bob;
import MEnDiGa.spatial.nodes.Node;

public class BobSquirrelCollisionObserver extends BobCollisionObserver {

	public BobSquirrelCollisionObserver(String id, Node node,
			List<Node> nodeGroup) {
		super(id, node, nodeGroup);
	}

	@Override
	public boolean evaluate(Object... params) {
		return super.evaluate(params) && !((Bob)this.node).hit;
	}

}
