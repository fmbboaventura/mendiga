package superjumper.observer;

import java.util.List;

import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.Node;

public class BobCollisionObserver extends Observer {
	
	Node node;
	List<Node> nodeGroup;

	public BobCollisionObserver(String id, Node node, List<Node> nodeGroup) {
		super(id);
		this.node = node;
		this.nodeGroup = nodeGroup;
	}

	@Override
	public boolean evaluate(Object... params) {
		Position bobPosition = (Position) node.getCurrentLocation();
		BoxBound bobBounds = (BoxBound) node.getBounds();

		for (Node groupMember : nodeGroup) {
			Position platformPosition = (Position) groupMember
					.getCurrentLocation();
			BoxBound platformBounds = (BoxBound) groupMember.getBounds();
			boolean overlap = bobPosition.getX() < platformPosition.getX()
					+ platformBounds.width
					&& bobPosition.getX() + bobBounds.width > platformPosition
							.getX()
					&& bobPosition.getY() < platformPosition.getY()
							+ platformBounds.height
					&& bobPosition.getY() + bobBounds.height > platformPosition
							.getY();
			if (overlap) {
				params[0] = this.node;
				params[1] = groupMember;
				return true;
			}
		}
		return false;
	}

}
