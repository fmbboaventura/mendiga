package superjumper.observer;

import java.util.List;

import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.Node;
import MEnDiGa.spatial.nodes.Velocity;

public class BobSpringCollisionObserver extends BobCollisionObserver {

	public BobSpringCollisionObserver(String id, Node node, List<Node> nodeGroup) {
		super(id, node, nodeGroup);
	}

	@Override
	public boolean evaluate(Object... params) {
		Velocity v = ((Element) node).getProperty(Velocity.class);
		if(v.getY() > 0) return false;
		
		return super.evaluate(params);
	}

}
