package superjumper.observer;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import MEnDiGa.observers.Observer;

/**
 * <p>
 * An {@link Observer} that delegates to a list of other
 * observers.
 * </p>
 * @author Filipe Boaventura
 *
 */
public class ObserverGroup extends Observer {

	protected List<Observer> observers;
	private Queue<Observer> toAdd;
	private Queue<Observer> toRemove;
	private boolean evaluating;
	
	public ObserverGroup(String id) {
		super(id);
		this.observers = new LinkedList<Observer>();
		this.toAdd = new LinkedList<Observer>();
		this.toRemove = new LinkedList<Observer>();
	}

	public ObserverGroup(String id, List<Observer> observers) {
		super(id);
		this.observers = observers;
	}
	
	public void addObserver(Observer o){
		if(evaluating) this.toAdd.add(o);
		else this.observers.add(o);
	}
	
	public void removeObserver(Observer o){
		if(evaluating) this.toRemove.add(o);
		else {
			boolean removed = this.observers.remove(o);
			if(!removed) this.toAdd.remove();
		}
	}
	
	public void clearObservers(){
		this.toRemove.addAll(observers);
		this.toAdd.clear();
	}
	
	/**
	 * @param observerName
	 * @return the first observer found with the given observerName
	 */
	public Observer getObserver(String observerName){
		for(Observer o : this.observers)
			if(o.getId() != null && o.getId().equals(observerName)) return o;
		return null;
		
	}
	
	@Override
	public boolean evaluate(Object... params) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void fire(Object... params) {
		this.evaluating = true;
		super.fire(params);
		
		for(Iterator<Observer> it = this.observers.iterator(); it.hasNext();){
			Observer o = it.next();
			o.fire(params);
		}
		this.evaluating = false;
		
		while(!this.toAdd.isEmpty()){
			this.observers.add(this.toAdd.remove());
		}
		
		while(!this.toRemove.isEmpty()){
			this.observers.remove(this.toRemove.remove());
		}
	}

}
