package superjumper.observer;

import java.util.List;

import superjumper.spatial.node.Bob;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.Node;
import MEnDiGa.spatial.nodes.Element;
import MEnDiGa.spatial.nodes.Velocity;

public class BobPlatformCollisionObserver extends BobCollisionObserver {

	public BobPlatformCollisionObserver(String id, Node bob, List<Node> platforms) {
		super(id, bob, platforms);
	}

	@Override
	public boolean evaluate(Object... params) {
		if(((Bob)this.node).hit) return false;
		boolean collided = super.evaluate(params);
		Velocity v = ((Element) node).getProperty(Velocity.class);
		Position bobPosition = (Position) node.getCurrentLocation();
		
		if(v.getY() > 0) return false;
		
		if(collided){
			Node platform = (Node) params[1];
			Position platformPosition = (Position) platform.getCurrentLocation();
			if(bobPosition.getY() > platformPosition.getY()) return true;
		}
		
		return false;
	}
	
	public void setNodeGroup(List<Node> group){
		this.nodeGroup = group;
	}

}
