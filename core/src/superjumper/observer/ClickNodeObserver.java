package superjumper.observer;

import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.locations.Position;
import MEnDiGa.spatial.nodes.BoxBound;
import MEnDiGa.spatial.nodes.Node;
import MEnDiGa.spatial.nodes.Player;

public class ClickNodeObserver extends Observer {
	
	private Node node;

	public ClickNodeObserver(Node node) {
		super(node.getName() + "ClickObserver");
		this.node = node;
	}

	@Override
	public boolean evaluate(Object... params) {
		if(params != null && params[0].equals(Player.TOUCH_DOWN)){
			float x = ((Float) params[1]).floatValue();
			float y = ((Float) params[2]).floatValue();
			
			Position position = (Position) node.getCurrentLocation();
			BoxBound box = (BoxBound) node.getBounds();
			
			boolean clicked = position.getX() <= x 
					&& position.getX() + box.width >= x
					&& position.getY() <= y
					&& position.getY() + box.height >= y;
			System.out.println(node.getName() + clicked);
			return clicked;
		}
		return false;
	}

}
