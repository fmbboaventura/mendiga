package superjumper.observer;

import superjumper.behavior.EndGame;
import superjumper.spatial.World;
import superjumper.spatial.node.Bob;
import MEnDiGa.GameSession;
import MEnDiGa.observers.Observer;
import MEnDiGa.spatial.locations.Position;

public class CheckGameOver extends Observer {

	public CheckGameOver(GameSession game) {
		super("checkGameOver");
		this.addBehavior(new EndGame(game));
	}

	@Override
	public boolean evaluate(Object... params) {
		Bob bob = Bob.getInstance();
		Position position = (Position) bob.getCurrentLocation();
		return (bob.heightSoFar - World.WORLD_CAM.getViewportHeight() / 2 > position
				.getY());
	}

}
