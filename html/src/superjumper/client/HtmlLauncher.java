package superjumper.client;

import superjumper.SuperJumperLibGDX;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;

public class HtmlLauncher extends GwtApplication {

        @Override
        public GwtApplicationConfiguration getConfig () {
                return new GwtApplicationConfiguration(339, 600);
        }

        @Override
        public ApplicationListener getApplicationListener () {
                return new SuperJumperLibGDX();
        }
}